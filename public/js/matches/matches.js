$(document).ready (function() {

    $("input[name='date']").inputmask("d.m.y", { "placeholder": "дд.мм.гггг" });

    $list_team = $("select[name='team_rival']").parent().clone();

    $("i.new_rival").click(function() {
        $block = "";
        $(this).parent().prev().remove();
        if($(this).hasClass("fa-plus")) {
            $block = "<div class='col-md-6 new_rival'>" +
                         "<div class='row'>" +
                             "<div class='col-md-12'>" +
                             "<input type='text' class='form-control' name='title_rival' placeholder='Название команды...' required>" +
                             "</div>" +
                         "</div>" +
                         "<div class='row margin-top-10px'>" +
                             "<div class='col-md-12'>" +
                             "<input type='file' name='logo_rival' required>" +
                             "</div>" +
                         "</div>" +
                     "</div>";
        } else if ($(this).hasClass("fa-minus")) {
            $block = $list_team;
        }

        $(this).parent().before(
            $block
        );
        // $(this).remove();
        $(this).toggleClass("fa-plus");
        $(this).toggleClass("fa-minus");
    });

    $("i#edit_match").click(function() {
        var id = $(this).attr('data-id');
        var div = $("div.edit_match");
        $.ajax({
            url: 'calendar/edit_match',
            type:'get',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(response) {
                console.log(response);
                div.find("select[name='type_match']").empty();
                div.find("select[name='type_event']").empty();
                div.find("select[name='team_rival']").empty();
                div.find("input[name='date']").val(response.match.date);
                div.find("input[name='stadium']").val(response.match.stadium);
                div.find("input[name='id']").val(response.match.id);
                response.match.type_match == 0 ?
                    div.find("select[name='type_match']").append(
                        "<option value='0'>Хозяин</option>" +
                        "<option value='1'>Гость</option>"
                    ) :
                    div.find("select[name='type_match']").append(
                        "<option value='1'>Гость</option>" +
                        "<option value='0'>Хозяин</option>"
                    );

                div.find("select[name='type_event']").append(
                    "<option value='"+response.match.event.id+"'>" + response.match.event.title + "</option>"
                );
                for(var i in response.list_data.events) {
                    div.find("select[name='type_event']").append(
                        "<option value='"+response.list_data.events[i].id+"'>" + response.list_data.events[i].title + "</option>"
                    );
                }

                div.find("select[name='team_rival']").append(
                    "<option value='"+response.match.team_rival.id+"'>" + response.match.team_rival.title + "</option>"
                );
                for(var i in response.list_data.team_rival) {
                    div.find("select[name='team_rival']").append(
                        "<option value='"+response.list_data.team_rival[i].id+"'>" + response.list_data.team_rival[i].title + "</option>"
                    );
                }

                $("div.edit_match").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

});

function add_match(e) {
    $("div.error").remove();

    var valid_date = $("input[name='date']").val().split('.');

    if(parseInt(valid_date[0]).toString() == "NaN" || parseInt(valid_date[1]).toString() == "NaN" || parseInt(valid_date[2]).toString() == "NaN"
        || parseInt(valid_date[2]).toString().length != 4) {

        $("div.error").remove();
        $("div.end").append("<div class='error'>" + "<label style='color:red'>Дата указана неверно</label>" + "</div>");
        return false;
    } else {
        return true;
    }
}