$(document).ready(function() {

    $('input[name="contact[][email]"], input[name="new_contact[][email]"]').inputmask({
        mask: "*{1,20}@*{1,20}[.*{2,6}]",
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        }
    });

    $("form[name='edit_contacts']").submit(function(event) {
        event.preventDefault();
        var form = $(this).serializeObject();
        //console.log(form);
        $.ajax({
            url: 'contacts/edit_contacts',
            type:'post',
            dataType: 'json',
            data: {
                form:form,
                _token: $("input[name='_token']").val()
            },
            success: function(response) {
                console.log(response);
                if(response == 1) {
                    window.location.replace("contacts");
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("button.new_contact_office").click(function(event) {
        event.preventDefault();
        var id = $(this).attr("data-id");
        $("table#office"+id+"").append(
            "<tr>"+
                "<input type='hidden' value='"+id+"' form='edit_contact' name='new_contact[][office_id]'>"+
                "<td><i class='fa fa-phone  ad-contacts-phone_icon'></i></td>"+
                "<td><p><input form='edit_contact' type='text' class='form-control' name='new_contact[][contact_type]' required></p>"+
                    "<div class='row'>"+
                        "<div class='col-md-5'>" +
                            "<input form='edit_contact' type='text' class='form-control' name='new_contact[][phone_number]' required>" +
                        "</div>" +
                        "<div class='col-md-1 ad-contacts-block__dash-labels'> — </div>"+
                        "<div class='col-md-5'>"+
                            "<input form='edit_contact' type='text' class='form-control' name='new_contact[][position]' required>"+
                            "<input form='edit_contact' type='text' class='form-control' name='new_contact[][contact_name]' required>"+
                            "<input form='edit_contact' type='text' class='form-control' name='new_contact[][email]' required>"+
                        "</div>"+
                        "<div class='col-md-1'><i class='fa fa-minus del_new_contact' style='cursor: pointer'></i></div>"+
                    "</div>"+
                "</td>"+
            "</tr>"
        );
        $('input[name="new_contact[][email]"]').inputmask({
            mask: "*{1,20}@*{1,20}[.*{2,6}]",
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    cardinality: 1,
                    casing: "lower"
                }
            }
        });
    });

    $("i.del_contact").click(function() {
        var input = $(this).parents("table.office_contact tbody tr").find("input[name='contact[][id]']");
        input.attr("name","delete_contact[]");
        $(this).parents("table.office_contact tbody tr").find("td").remove();
        //$(this).parent().parent().parent().parent().prev().attr("name","delete_contact[]");
        //$(this).parent().parent().parent().parent().remove();
    });

    $("div.ad-contacts-block").on("click","i.del_new_contact",function() {
        $(this).parent().parent().parent().parent().remove();
    });

    $("i.add_contact").click(function() {
        $("table#list_new_contact tr:first td").eq(1).append(
            "<div class='row'>"+
                "<div class='row margin-top-10px'>"+
                    "<div class='col-md-11'>"+
                        "<input form='add_office' type='text' class='form-control' name='new_contact[][contact_type]' placeholder='Тип контакта...' required>"+
                    "</div>"+
                "</div>"+
                "<div class='row margin-top-10px'>"+
                    "<div class='col-md-5'><input form='add_office' type='text' class='form-control' name='new_contact[][phone_number]' placeholder='Телефон...' required></div>"+
                    "<div class='col-md-6'>"+
                        "<input form='add_office' type='text' class='form-control' name='new_contact[][position]' placeholder='Должность...' required>"+
                        "<input form='add_office' type='text' class='form-control' name='new_contact[][contact_name]' placeholder='Имя...' required>"+
                        "<input form='add_office' type='text' class='form-control' name='new_contact[][email]' placeholder='Почта...' required>"+
                    "</div>"+
                    "<div class='col-md-1'><i class='fa fa-minus delete_contact' style='cursor: pointer'></i></div>"+
                "</div>"+
            "</div>"
        );
    });

    $("table#list_new_contact").on("click", "i.delete_contact", function() {
        $(this).parent().parent().parent().remove();
    });

    $("form#add_office").submit(function(event) {
        var form = $(this).serializeObject();
        event.preventDefault();
        $.ajax({
            url: 'contacts/add_contacts',
            type:'post',
            dataType: 'json',
            data: {
                form:form,
                _token: $("input[name='_token']").val()
            },
            success: function(response) {
                console.log(response);
                if(response == 1) {
                    window.location.replace("contacts");
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("a.delete_office").click(function() {
        $("div.delete_office").modal("show");
        $("div.delete_office a#delete_office").attr("href",$(this).attr("data-url"))
    });

    $("button.edit_contact_office").click(function() {
        var container = $(this).parents("div.container");
        var contact = [], new_contact = [], delete_contact = [];

        container.find("div.ad-contacts-block table.office_contact tbody tr").each(function (key, value) {
            contact.push({
                contact_id: $(value).find("input[name='contact[][id]']").val(),
                contact_type: $(value).find("input[name='contact[][contact_type]']").val(),
                contact_phone_number: $(value).find("input[name='contact[][phone_number]']").val(),
                contact_position: $(value).find("input[name='contact[][position]']").val(),
                contact_name: $(value).find("input[name='contact[][contact_name]']").val(),
                contact_email: $(value).find("input[name='contact[][email]']").val()
            });
            $(value).find("input[name='new_contact[][office_id]']").length ? new_contact.push({
                contact_type: $(value).find("input[name='new_contact[][contact_type]']").val(),
                contact_phone_number: $(value).find("input[name='new_contact[][phone_number]']").val(),
                contact_position: $(value).find("input[name='new_contact[][position]']").val(),
                contact_name: $(value).find("input[name='new_contact[][contact_name]']").val(),
                contact_email: $(value).find("input[name='new_contact[][email]']").val()
            }) : false;
            $(value).find("input[name='delete_contact[]']").length ? delete_contact.push($(value).find("input[name='delete_contact[]']").val()) : false;
        });

        var obj = {
            id: container.find("input[name='office[][id]']").val(),
            office_name: container.find("input[name='office[][offices_name]']").val(),
            legal_addr: container.find("input[name='office[][legal_addr]']").val(),
            actual_addr: container.find("input[name='office[][actual_addr]']").val(),
            email: container.find("input[name='office[][email]']").val(),
            contacts: contact,
            new_contacts: new_contact,
            delete_contacts: delete_contact
        };
        //console.log(delete_contact);
        $.ajax({
            url: 'contacts/update_office_contact',
            type:'post',
            dataType: 'json',
            data: {
                form:obj,
                _token: $("input[name='_token']").val()
            },
            success: function(response) {
                console.log(response);
                if(response == 1) {
                    window.location.replace("contacts");
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});