//=========================================================

//=========================================================
//                    HS
//=========================================================
//                     Содержание
//=========================================================
// jquery-ui
//	- Windows
//		- popup
//	- usability
//		- checkbox
//		- radio
//		- select
//		- slider
//		- datepicker
//		- tooltip
//		- spinner
//		- tabs
// slike
//	-carousel
// jquery-hs
//	- validate
//		- hs_valid_text
//		- hs_valid_num
//		- hs_valid_mail
//	- usability
//  		- hs_scrollbar
//  		- hs_tree
//  		- hs_resize_text
//  		- hs_up_button
//  		- hs_tofixed
//  		- hs_slide_menu
//  		- hs_timer
//  		- hs_gallery
//  		- hs_scroll_screen
//	- animation
// 		-zoom
// 		-float shadow
// 		-rotate
//	- function
// 		- hs_bounce
// 		- hs_transform_color
//=========================================================
//                    /HS
//=========================================================
var WS_TRIGGER = false;

jQuery(document).ready(function(){
    if (WS_TRIGGER) return;
    WS_TRIGGER = true;
    
    $.fn.ws_link_scripts = function (){
    //=========================================================
    // VARS
    //=========================================================
    $ = jQuery;
    var qSelector = $(this);
    
    //=========================================================
    // /VARS
    //=========================================================
    //=========================================================
    // CONF
    //=========================================================
    var Checkbox 		= qSelector.find('.hsj-checkbox');
    var Radio			= qSelector.find('.hsj-radio');
    var Carousel 		= qSelector.find('.hsj-carousel');
    var Tabs	 		= qSelector.find('.hsj-tabs');
    var Date			= qSelector.find('.hsj-date');
    var Select			= qSelector.find('.hsj-select');
    var Slider			= qSelector.find('.hsj-slider');
    var Spinner			= qSelector.find('.hsj-spinner');
    var Tooltip			= qSelector.find('.hsj-tooltip');
    var Popup			= qSelector.find('.hsj-popup-button');
    
    var Scrollbar		= qSelector.find('.hsj-scrollbar');
    var Tree 			= qSelector.find('.hsj-tree');
    var Resize_text		= qSelector.find('.hsj-resize-text');
    var Up_button		= qSelector.find('.hsj-up-button');
    var Tofixed			= qSelector.find('.hsj-tofixed');
    var Timer			= qSelector.find('.hsj-timer');
    var Gallery			= qSelector.find('.hsj-gallery');
    var Scroll_screen		= qSelector.find('.hsj-scroll-screen');
    
    var Anim_zoom		= qSelector.find('.hsj-anim-zoom');
    var Anim_float_shadow 	= qSelector.find('.hsj-anim-float-shadow');
    var Anim_rotate		= qSelector.find('.hsj-anim-roatate');
    //=========================================================
    // /CONF
    //=========================================================
    //=========================================================
    // FUNCTIONS
    //=========================================================
    function extend(args, default_args) {
	for(var item in args) if (args[item]===undefined) delete args[item];
	
	return $.extend (default_args, args);
    }
    //=========================================================
    // /FUNCTIONS
    //=========================================================
    
    //------------------------------------------------
    // Scroll_screen
    //------------------------------------------------
    (function() {
        if (Scroll_screen.lenght == 0) return;
	
        var default_args = {
	    AnimateStep    : 10,
            AnimateDellay  : 5
	};
        
        Scroll_screen.each(function(){
            var args = {
		AnimateStep    : $(this).data('animation-step'),
		AnimateDellay  : $(this).data('animation-dellay')
            };
            args = extend(args, default_args);
            
	    $(this).hs_scroll_screen(args);
        });
    })();
    //------------------------------------------------
    // /Scroll_screen
    //------------------------------------------------
    //------------------------------------------------
    // Gallery
    //------------------------------------------------
    (function() {
        if (Gallery.lenght == 0) return;
	
        var default_args = {
	    AnimationType : 'fade',// тип анимации
            AnimationTime : 300,// время анимации
            Popup : false,// по клаку на большую картинку показать попап с этой картинкой
            Buttons : false// кнопки управления
	};
        
        Gallery.each(function(){
            var args = {
		AnimationType : $(this).data('animation-type'),
		AnimationTime : $(this).data('animation-time'),
		Popup : $(this).data('popup'),
		Buttons : $(this).data('buttons')
            };
            args = extend(args, default_args);
            
	    $(this).hs_gallery(args);
        });
    })();
    //------------------------------------------------
    // /Gallery
    //------------------------------------------------
    //------------------------------------------------
    // Timer
    //------------------------------------------------
    (function() {
        if (Timer.lenght == 0) return;
	
        var default_args = {
	    EndNumber: 0,// конечное значение таймера
            TimeAnimation: 100,// время анимации
            TypeAnimation: 'slide'// тип анимации
	};
        
        Timer.each(function(){
            var args = {
		EndNumber: $(this).data('end-number'),
		TimeAnimation: $(this).data('time-animation'),
		TypeAnimation: $(this).data('type-animation')
            };
            args = extend(args, default_args);
            
	    $(this).hs_timer(args);
        });
    })();
    //------------------------------------------------
    // /Timer
    //------------------------------------------------
    //------------------------------------------------
    // Tofixed
    //------------------------------------------------
    (function() {
        if (Tofixed.lenght == 0) return;
	
        var default_args = {
	    top:    0,// положение окна
            right:  0,
            scrollAfter: 0,// значение после которого срабатывает скрипт
            position : undefined,
	};
        
        Tofixed.each(function(){
            var args = {
		top:    $(this).data('top'),
		right:  $(this).data('right'),
		bottom: $(this).data('bottom'),
		left: $(this).data('left'),
		scrollAfter: $(this).data('scroll-after'),
		position : $(this).data('position'),
            };
            args = extend(args, default_args);
            
	    $(this).hs_tofixed(args);
        });
    })();
    //------------------------------------------------
    // /Tofixed
    //------------------------------------------------
    //------------------------------------------------
    // Up_button
    //------------------------------------------------
    (function() {
        if (Up_button.lenght == 0) return;
	
        var default_args = {
	    AnimationTime: 300 //время появления/скрытия "кнопки"
	};
        
        Up_button.each(function(){
            var args = {
		AnimationTime: $(this).data('animation-time')
            };
            args = extend(args, default_args);
            
	    $(this).hs_up_button(args);
        });
    })();
    //------------------------------------------------
    // /Up_button
    //------------------------------------------------
    //------------------------------------------------
    // Resize_text
    //------------------------------------------------
    (function() {
        if (Resize_text.lenght == 0) return;
	
        var default_args = {
	    MaxHeight: 50,// высота в свернутом состоянии
            CountLine : 0,// количество строк в свернутом состоянии
            TypeButton: 1,// тип кноки
            WordOpen: 'Развернуть',
            WordClose: 'Свернуть',
            Icon: '<i class="fa fa-chevron-down"></i>',
            ToggleClasses: 'fa-chevron-up fa-chevron-down',
            CustomClass: ''
	};
        
        Resize_text.each(function(){
            var args = {
		MaxHeight: $(this).data('max-height'),
		CountLine : $(this).data('count-line'),
		TypeButton: $(this).data('type-button'),
		WordOpen: $(this).data('word-opne'),
		WordClose: $(this).data('word-close'),
		Icon: $(this).data('icon'),
		ToggleClasses: $(this).data('toggle-classes'),
		CustomClass: $(this).data('custom-class')
            };
            args = extend(args, default_args);
            
	    $(this).hs_resize_text(args);
        });
    })();
    //------------------------------------------------
    // /Resize_text
    //------------------------------------------------
    //------------------------------------------------
    // Tree
    //------------------------------------------------
    (function() {
        if (Tree.lenght == 0) return;
	
        var default_args = {
	    Сollapsed : true
	};
        
        Tree.each(function(){
            var args = {
		Сollapsed : $(this).data('collapsed')// скрыть по умолчанию
            };
            args = extend(args, default_args);
            
	    $(this).hs_tree(args);
        });
    })();
    //------------------------------------------------
    // /Tree
    //------------------------------------------------
    //------------------------------------------------
    // Scrollbar
    //------------------------------------------------
    (function() {
        if (Scrollbar.lenght == 0) return;
	
        var default_args = {
	    Height : 150
	};
        
        Scrollbar.each(function(){
            var args = {
		Height : $(this).data('hight')// высота блока
            };
            args = extend(args, default_args);
	    
            Scrollbar.hs_scrollbar(args);
        });
    })();
    //------------------------------------------------
    // /Scrollbar
    //------------------------------------------------
    //------------------------------------------------
    // Popup
    //------------------------------------------------
    (function() {
        if (Popup.lenght == 0) return;
	
        var default_args = {
	    popup : '#hs-popup-1',
	    animation: "fade",
	    animation_time: 1000
	};
        
	
        Popup.each(function(){
            var args = {
		popup: $(this).data('window'),// id окна
		animation: $(this).data('animation'),// анимация
		animation_time: $(this).data('time')// время анимации
            };
            args = extend(args, default_args);
	    
            $(this).click(function(){
		var hide = true;
		var close = $(args.popup).find('.hsj-popup__remove');
		var window = $(args.popup).find('.hsj-popup__window');
		
		function onHide() {
		    $(args.popup).hide(args.animation, args.animation_time);
		}
		
		$(args.popup).toggle(args.animation, args.animation_time);
		
		close.click(function(){
		    onHide();
		});
		$(args.popup).click(function(){
		    if (hide) onHide();
		});
		$(window).mouseenter(function(){
		    hide = false;
		});
		$(window).mouseleave(function(){
		    hide = true;
		});
		
	    });    
        });
    })();
    //------------------------------------------------
    // /Popup
    //------------------------------------------------
    //------------------------------------------------
    // Tooltip
    //------------------------------------------------
    (function() {
        if (Tooltip.lenght == 0) return;
        
        var default_args = {
        };
        
        Tooltip.each(function(){
            var args = {
		content: $(this).data('content'),// текст
		disabled: $(this).data('disabled'),// не активный
		hide: $(this).data('hide'),// скрыть
		tooltipClass: $(this).data('tooltipClass'),// дополнительный класс
		show: {// анимация появления
		    effect: $(this).data('show-effect'),
		    duration: $(this).data('show-duration')
		},
		position: {// позиция подсказки
		    my: $(this).data('position-my'),
		    at: $(this).data('position-at')
		}
		
            }
            
            args = extend(args, default_args);
            
            $(this).tooltip(args);
        });
    })();
    //------------------------------------------------
    // /Tooltip
    //------------------------------------------------
    //------------------------------------------------
    // Spinner
    //------------------------------------------------
    (function() {
        if (Spinner.lenght == 0) return;
        
        var default_args = {
        };
        
        Spinner.each(function(){
            var args = {
		disabled: $(this).data('disabled'),// не активный
		max: $(this).data('max'),// минимум
		min: $(this).data('min'),// максимум
		numberFormat: $(this).data('number-format'),// формат числа
		step: $(this).data('step'),// шаг
		icons: {
		    down: $(this).data('icons-down'),// иконка "Ввех"
		    up: $(this).data('icons-up')// иконка "Вниз"
		}
            };
            
            args = extend(args, default_args);
            
            $(this).spinner(args);
        });
    })();
    //------------------------------------------------
    // /Spinner
    //------------------------------------------------
    //------------------------------------------------
    // Slider
    //------------------------------------------------
    (function() {
        if (Slider.lenght == 0) return;
        
        var default_args = {
	    range: true,
	    animate: 'fast',
        };
        
        Slider.each(function(){
            var args = {
		range: $(this).data('range'),// диапазон
		animate: $(this).data('animate'),// анимация
		disabled: $(this).data('disabled'),// не активный
		max: $(this).data('max'),// максимально значение
		min: $(this).data('min'),// минимальное значение
		orientation: $(this).data('orientation'),// ориентация
		step: $(this).data('step'),// шаг 
		value: $(this).data('value'),// начальное значение
		values: $(this).data('values')// начальные значения
            };
            
            args = extend(args, default_args);
            
            $(this).slider(args);
        });
    })();
    //------------------------------------------------
    // /Slider
    //------------------------------------------------
    //------------------------------------------------
    // Select
    //------------------------------------------------
    (function() {
        if (Select.lenght == 0) return;
        
        var default_args = {
	    disabled: false
        };
        
        Select.each(function(){
            var args = {
		disabled: $(this).data('disabled'),// дизактивировать
		width: $(this).data('width'),// ширина списка
		icons: {
		    button: $(this).data('icons-button')// иконка кнопки
		},
		position: {
		    my: $(this).data('position-my'),
		    at: $(this).data('position-at'),
		    collision: $(this).data('position-collision')
		}
            };
            
            args = extend(args, default_args);
            
            $(this).selectmenu(args);
        });
    })();
    //------------------------------------------------
    // /Select
    //------------------------------------------------
    //------------------------------------------------
    // Date
    //------------------------------------------------
    (function() {
        if (Date.lenght == 0) return;
	
        var default_args = {
	    prevText: '&#x3C;Пред',
	    nextText: 'След&#x3E;',
	    currentText: 'Сегодня',
	    buttonText: 'Выбрать',
	    closeText: 'Закрыть',
	    monthNames: [ 'Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь' ],
	    monthNamesShort: [ 'Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек' ],
	    dayNames: [ 'воскресенье','понедельник','вторник','среда','четверг','пятница','суббота' ],
	    dayNamesShort: [ 'вск','пнд','втр','срд','чтв','птн','сбт' ],
	    dayNamesMin: [ 'Вс','Пн','Вт','Ср','Чт','Пт','Сб' ],
	    weekHeader: 'Нед',
	    dateFormat: 'dd . mm . yy',
	    firstDay: 1,
	    isRTL: false,
	    showMonthAfterYear: false,
	    yearSuffix: '',
	    formatDate: 'yy/mm/dd',
	    buttonImage: '/images/btn-date.png',
	    buttonImageOnly: false,
	    changeMonth: false,
	    changeYear: false,
	    
	    maxDate: '',
	    minDate: '',
	    showAnim: 'fade',
	    showButtonPanel: true,
	    showOn: 'both',
	    yearRange: '',
	    showOptions: {
		direction: "up"
	    }
	    
        };
        
        Date.each(function(){
            var args = {
		showAnim: $(this).data('show-anim'),
		dateFormat: $(this).data('date-format'),
		maxDate: $(this).data('max-date'),
		minDate: $(this).data('min-date'),
		showAnim: $(this).data('show-animation'),
		showButtonPanel: $(this).data('show-button-panel'),
		showOn: $(this).data('show-on'),
		yearRange: $(this).data('year-range'),
		showOptions: {
		    direction: $(this).data('option-direction')
		}
            };
            
            args = extend(args, default_args);
            
            $(this).datepicker(args);
        });
    })();
    //------------------------------------------------
    // /Date
    //------------------------------------------------
    //------------------------------------------------
    // Tabs
    //------------------------------------------------
    
    (function() {
        if (Tabs.lenght == 0) return;
        
        var default_args = {
	    active : 0,
	    collapsible : false,
	    disabled : [],
	    event : 'click',
	    heightStyle : 'auto',
	    hide : {
		effect: 'explode',
		duration: 1000
	    },
	    show : {
		effect: 'explode',
		duration: 1000
	    }
        };
        
        Tabs.each(function(){
            var args = {
		active : $(this).data('active'),// активная влкдка
		collapsible : $(this).data('collapsible'),// сворачивать активную вкладку
		disabled : $(this).data('disabled'),// не активные вкладки (массив [0,1,2,3])
		event : $(this).data('click'),// событие переключения
		heightStyle : $(this).data('height-style'),// тип высоты вкладок
		hide : {// анимация скрытия
		    effect: $(this).data('hide-effect'),
		    duration: $(this).data('hide-duration')
		},
		show : {// анимация появление
		    effect: $(this).data('show-effect'),
		    duration: $(this).data('show-duration')
		}
	    };
            
            args = extend(args, default_args);
            $(this).tabs(args);
        });
    })();
    //------------------------------------------------
    // /Tabs
    //------------------------------------------------
    //------------------------------------------------
    // Carousel
    //------------------------------------------------
    (function() {
        if (Carousel.length == 0) return;
        
        Carousel.each(function(){
	    var default_args = {
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    prevArrow : '<div class="hs-carousel__prev"><i class="fa fa-angle-left"></i></div>',
	    nextArrow : '<div class="hs-carousel__next"><i class="fa fa-angle-right"></i></div>',
	    dots : false,
	    autoplay : true,
	    centerMode: false,
	    autoplaySpeed : 8000,
	    responsive: [
		{
		    breakpoint: 1199,
		    settings: {
			slidesToShow: 2,
			slidesToScroll: 2,
		    }
		},
		{
		    breakpoint: 767,
		    settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		    }
		}
	    ]
        };
	
            var args = {
		slidesToShow: $(this).data('slides-to-show'), // отображаемые слайды
		slidesToScroll: $(this).data('slides-to-scroll'),// шаг
		prevArrow : $(this).data('prev-arrow'),// кнопка "Назад"
		nextArrow : $(this).data('next-arrow'),// копка "Вперед"
		dots : $(this).data('dots'),// конпки управления
		autoplay : $(this).data('autoplay'),// авто прокрутка
		centerMode: $(this).data('center-mode'),
		autoplaySpeed : $(this).data('autoplay-speed'),// скорость прокрутки
		
		responsive: [// аттрибуты в зависимости от ширины экрана
		    {
			breakpoint: $(this).data('breakpoint-md'),
			settings: {
			    slidesToShow: $(this).data('slides-to-show-md'),
			    slidesToScroll: $(this).data('slides-to-scroll-md'),
			}
		    },
		    {
			breakpoint: $(this).data('breakpoint-sm'),
			settings: {
			    slidesToShow: $(this).data('slides-to-show-sm'),
			    slidesToScroll: $(this).data('slides-to-scroll-sm'),
			}
		    },
		    {
			breakpoint: $(this).data('breakpoint-xs'),
			settings: {
			    slidesToShow: $(this).data('slides-to-show-xs'),
			    slidesToScroll: $(this).data('slides-to-scroll-xs'),
			}
		    },
		    {
			breakpoint: $(this).data('breakpoint-xxs'),
			settings: {
			    slidesToShow: $(this).data('slides-to-show-xxs'),
			    slidesToScroll: $(this).data('slides-to-scroll-xxs'),
			}
		    },
		]
            };
            
            args = extend(args, default_args);
            $(this).slick(args);
        });
    })();
    //------------------------------------------------
    // /Carousel
    //------------------------------------------------
    
    //------------------------------------------------
    // TMP
    //------------------------------------------------
    /*(function() {
        if (TMP.lenght == 0) return;
        
        var default_args = {
        };
        
        TMP.each(function(){
            var args = {
		slidesToShow: $(this).data(''),
            };
            
            args = extend(args, default_args);
            
            $(this).plugin(args);
        });
    })();*/
    //------------------------------------------------
    // /TMP
    //------------------------------------------------
    }
    
    
    //------------------------------------------------
    // Run
    //------------------------------------------------
    $('body').ws_link_scripts();
});
