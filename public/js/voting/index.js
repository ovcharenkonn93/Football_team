var voting_data = Array();
var vote;
//var barChart;

function show_voting(data){
    var included="";
    for(var key in data.questions){
        included = included + "<div class=\"block-input-radio\">" +
            "<input id='checkopt"+key+"' class='que_answ' name=\"que_answ\" type='radio' value='"+data.questions[key].id+"'/>" +
            "<label for='checkopt"+key+"'>"+data.questions[key].question+"</label></div>";
    }
    $("#answer_options").html(included);
    $("#resChart").css("display","none");
    $("#vote").css("display","block");
    $("#other_results").css("display","block");
    $(".que_answ").on('click',function(){
        vote = $(this).val();
    })
}

//function show_results(data){
//    $("#answer_options").html("");
//    $("#vote").css("display","none");
//    $("#other_results").css("display","block");
//    $("#resChart").css("display","block");
//    var que_titles = Array();
//    var que_colors = Array();
//    var que_results = Array();
//    for(var key in data.questions){
//        que_titles[key] = data.questions[key].question;
//        if(data.questions[key].id==data.vote){
//            que_colors[key] = 'rgba(255, 0, 0, 0.5)';
//            //data.questions_results[key] = data.questions_results[key]+1;
//        }
//        else{
//            que_colors[key] = 'rgba(0, 0, 255, 0.5)';
//        }
//
//        var sum_results = 0;
//        for(var in_key in data.questions_results){
//            sum_results = sum_results + data.questions_results[in_key];
//        }
//        que_results[key] = Number((data.questions_results[key]/sum_results*100).toFixed(2));
//    }
//    var data = {
//        labels: que_titles,
//        datasets: [
//            {
//                label: "Проголосовало (%)",
//                backgroundColor: que_colors,
//                data: que_results
//            }
//        ]
//    };
//
//    $("#resChart").val("");
//    var ctx = $("#resChart").get(0).getContext("2d");
//    if(barChart!=undefined){
//        barChart.destroy();
//    }
//    barChart = new Chart(ctx,{
//
//            type: 'horizontalBar',
//            data: data,
//            options: {
//                scales:{
//                    xAxes:[{
//                        ticks: {
//                            beginAtZero: true,
//                            max:100
//                        },
//                        gridLines:{
//                            drawOnChartArea:false,
//                        }
//                    }],
//                    yAxes: [{
//                        gridLines:{
//                            drawOnChartArea:false,
//                        }
//                    }],
//
//                },
//                legend:{
//                    display:false,
//                }
//            }
//        }
//    );
//
//}

function refresh_last_voting(){
    var fd = new FormData();
    fd.append("_token", $("input[name='_token']").val());
    $.ajax({
        url: '/last_voting',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            voting_data = data;
            refresh_voting(voting_data);
        },
        error: function (errors) {
            console.log(errors);
        }
    });
}

function refresh_voting(data){
    $('#ttl').html(data.interview.title);
    if(data.results==0){
        show_voting(data);
    }
    else{
        show_results(data);
    }
}

$(document).ready(function(){

    refresh_last_voting();

    $("#vote").on('click',function(){
        if(vote==undefined){
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Выберите один из предложенных вариантов!</span>", { type: 'danger',delay: 5000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            return 1;
        }
        $(this).prop('disabled',true);
        var fd = new FormData();
        fd.append("qid",vote);
        fd.append("_token", $("input[name='_token']").val());
        $.ajax({
            url: '/add_vote',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                setTimeout(function() {
                    $.bootstrapGrowl("<span style='font-size:1vmax;'>Ваш голос учтен!</span>", { type: 'success',delay: 5000
                        , align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
                }, 0);

                $("#vote").prop('disabled',false);
                return 0;
            },
            error: function (errors) {
                console.log(errors);
            }
        });
        voting_data.vote = vote;
        for(var key in voting_data.questions){
            if(voting_data.questions[key].id==vote){
                voting_data.questions_results[key] = voting_data.questions_results[key]+1;
            }
        }
        voting_data.results = 1;
        refresh_voting(voting_data);


    });
});

