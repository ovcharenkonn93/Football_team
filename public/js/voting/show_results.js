var barChart = "";
function show_results(data){
    $("#answer_options").html("");
    $("#vote").css("display","none");
    $("#other_results").css("display","block");
    $("#resChart").css("display","block");
    var que_titles = Array();
    var que_colors = Array();
    var que_results = Array();
    for(var key in data.questions){
        que_titles[key] = data.questions[key].question;
        if(data.questions[key].id==data.vote){
            que_colors[key] = 'rgba(255, 0, 0, 0.5)';
            //data.questions_results[key] = data.questions_results[key]+1;
        }
        else{
            que_colors[key] = 'rgba(0, 0, 255, 0.5)';
        }

        var sum_results = 0;
        for(var in_key in data.questions_results){
            sum_results = sum_results + data.questions_results[in_key];
        }
        que_results[key] = Number((data.questions_results[key]/sum_results*100).toFixed(2));
    }
    var data = {
        labels: que_titles,
        datasets: [
            {
                label: "Проголосовало (%)",
                backgroundColor: que_colors,
                data: que_results
            }
        ]
    };

    $("#resChart").val("");
    var ctx = $("#resChart").get(0).getContext("2d");
    if(barChart){
        barChart.destroy();
    }
    barChart = new Chart(ctx,{

            type: 'horizontalBar',
            data: data,
            options: {
                scales:{
                    xAxes:[{
                        ticks: {
                            beginAtZero: true,
                            max:100
                        },
                        gridLines:{
                            drawOnChartArea:false,
                        }
                    }],
                    yAxes: [{
                        gridLines:{
                            drawOnChartArea:false,
                        }
                    }],

                },
                legend:{
                    display:false,
                }
            }
        }
    );
}