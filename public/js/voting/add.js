var questions = Array();

function add_question(quest){
    questions.push(""+quest);
    return true;
}

function up_question(quest){
    var qid = Number(quest);
    var temp = questions[qid];
    questions[qid] = questions[qid-1];
    questions[qid-1] = temp;
    return true;
}

function down_question(quest){
    var qid = Number(quest);
    var temp = questions[qid];
    questions[qid] = questions[qid+1];
    questions[qid+1] = temp;
    return true;
}

function delete_question(quest){
    var qid = Number(quest);
    questions.splice(qid,1);
    return true;
}

function clean_questions(){
    questions.splice(0,questions.length);
    return true;
}

$(document).ready(function(){


    function refresh_questions(){

        $("#interview_questions").html("");

        var included = "";

        for(var key in questions){
            included = included + "<tr><td>"+questions[key]+"</td><td>";
            if(key>0){
                included = included + "<a class='que_up' data-up='"+key+"'><i class=\"fa fa-2x fa-arrow-up\"></i></a>";
            }
            included = included + "</td><td>";
            if(key<questions.length-1){
                included = included + "<a class='que_down' data-down='"+key+"'><i class=\"fa fa-2x fa-arrow-down\"></i></a>";
            }
            included = included + "</td><td>";
            included = included + "<a class='que_del' data-del='"+key+"'><i class=\"fa fa-2x fa-remove\"></i></a></td></tr>";
        }
        included = included + "<tr><td><input id=\"add_question\" class=\"form-control\" placeholder=\"Текст вопроса\"/></td>" +
            "<td><a id=\"add_question\" class='que_add'><i class=\"fa fa-2x fa-plus\"></i></a></td></tr>";
        $("#interview_questions").append(included);

        $(".que_add").on("click",function(){
            if($('#add_question').val().trim()!=""){
                add_question($('#add_question').val());
                refresh_questions();
            }
        });
        $(".que_up").on("click",function(){
            up_question($(this).data('up'));
            refresh_questions();
        });
        $(".que_down").on("click",function(){
            down_question($(this).data('down'));
            refresh_questions();
        });
        $(".que_del").on("click",function(){
            delete_question($(this).data('del'));
            refresh_questions();
        });
    }

    $("#add_voting_btn").on("click",function(){
        clean_questions();
        refresh_questions();
        $(".add_voting .title").html("Добавление голосования");
        $('#interview_name').val("");
        $("#add_interview").show();
        $("#edit_interview").hide();
        $(".add_voting").modal("show");
    });

    $("#edit_voting_btn").on("click",function(){
        refresh_questions();
        $(".add_voting .title").html("Изменение голосования");
        clean_questions();
        $('#interview_name').val(voting_data.interview.title);
        for(var key in voting_data.questions){
            add_question(voting_data.questions[key].question);
        }
        refresh_questions();
        $("#add_interview").hide();
        $("#edit_interview").show();
        $(".add_voting").modal("show");

    });

    $('#add_interview').on('click',function() {

        if($('#interview_name').val().trim()==""){
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Название опроса не может быть пустым!</span>", { type: 'danger',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            $('#interview_name').focus();
            return;
        }
        if(questions.length==0){
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Добавьте хотя бы один вариант ответа!</span>", { type: 'danger',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            $('#add_question').focus();
            return;
        }

        var fd = new FormData();
        fd.append("interview", $('#interview_name').val().trim());
        fd.append("questions", JSON.stringify(questions));
        fd.append("_token", $("input[name='_token']").val());
        $.ajax({
            url: '/add_voting',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                $(".add_voting").modal("hide");
                setTimeout(function() {
                    $.bootstrapGrowl("<span style='font-size:1vmax;'>Опрос \""+data+"\" успешно добавлен!</span>", { type: 'success',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
                }, 0);
                refresh_last_voting();
            },
            error: function (errors) {
                console.log(errors);
            }
        });
    });

    $('#edit_interview').on('click',function() {

        if($('#interview_name').val().trim()==""){
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Название опроса не может быть пустым!</span>", { type: 'danger',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            $('#interview_name').focus();
            return;
        }
        if(questions.length==0){
            setTimeout(function() {
                $.bootstrapGrowl("<span style='font-size:1vmax;'>Добавьте хотя бы один вариант ответа!</span>", { type: 'danger',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
            }, 0);
            $('#add_question').focus();
            return;
        }

        var fd = new FormData();
        fd.append("id_interview",voting_data.interview.id);
        fd.append("interview", $('#interview_name').val().trim());
        fd.append("questions", JSON.stringify(questions));
        fd.append("_token", $("input[name='_token']").val());
        $.ajax({
            url: '/edit_voting',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                $(".add_voting").modal("hide");
                setTimeout(function() {
                    $.bootstrapGrowl("<span style='font-size:1vmax;'>Опрос \""+data+"\" успешно изменен!</span>", { type: 'success',delay: 2000, align: 'center', width: 'auto', offset: {from: 'top', amount: 60} });
                }, 0);
                refresh_last_voting();
            },
            error: function (errors) {
                console.log(errors);
            }
        });
    });

});