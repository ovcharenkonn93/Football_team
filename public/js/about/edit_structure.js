$('#edit_structure').on('change',function(){
    var fd = new FormData();
    fd.append("file", $('#edit_structure')[0].files[0]);
    fd.append("_token",$("input[name='_token']").val());

    $.ajax({
        url: 'edit_structure',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            switch (data){
                case 1: alert("Файл слишком большой! Размер файла не должен превышать 5 Мб!");
                    break;
                case 2: alert("Поддерживаются только файлы изображений JPG, PNG и GIF!");
                    break;
                default:
                    location.reload(true);
            };

        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});