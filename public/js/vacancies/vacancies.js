$(document).ready(function() {
    $("button.add_vacancies").click(function() {
        $("div.add_vacancies").modal("show");
    });

    $("a.delete_vacancies").click(function() {
        $("div.delete_vacancies").modal("show");
        $("div.delete_vacancies a#delete_vacancies").attr("href",$(this).attr("data-href"))
    });

    $("a.edit_vacancies").click(function() {
        $.ajax({
            url: 'vacancies/edit',
            type:'get',
            dataType: 'json',
            data: {
                id: $(this).attr("data-id")
            },
            success: function(response) {
                console.log(response);
                $("select[name='department']").append(
                    "<option value='"+response.current_office.id+"'>"+response.current_office.name+"</option>"
                );
                for(var i = 0; i < response.offices.length; i++) {
                    $("select[name='department']").append(
                        "<option value='"+response.offices[i].id+"'>"+response.offices[i].name+"</option>"
                    );
                }
                $("input[name='vacancies']").val(response.vacancies);

                $("input[name='education']").val(response.education);

                $("input[name='experience']").val(response.experience);

                $("input[name='employment']").val(response.employment);

                $("input[name='id']").val(response.id);

                $("select[name='gender']").append(
                    "<option value='"+response.gender.id+"'>"+response.gender.name+"</option>"
                );
                for(var i = 0; i < response.other_gender.length; i++) {
                    $("select[name='gender']").append(
                        "<option value='"+response.other_gender[i].id+"'>"+response.other_gender[i].gender+"</option>"
                    );
                }
                $("div.edit_vacancies").modal("show");
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});