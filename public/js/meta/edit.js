$('#edit_meta').on('click',function(){
    if($('#meta_key').data('orig')!=$('#meta_key').val().trim()||
        $('#meta_desc').data('orig')!=$('#meta_desc').val().trim()){
        var fd = new FormData();
        fd.append("key", $('#meta_key').val().trim());
        fd.append("desc", $('#meta_desc').val().trim());
        fd.append("url", location.pathname);
        fd.append("_token",$("input[name='_token']").val());
        $.ajax({
            url: '../edit_meta',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
               location.reload(true);
            },
            error: function(errors)
            {
                console.log(errors);
            }
        });
    }
});
