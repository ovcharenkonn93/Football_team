$(document).ready(function(){
    $('#office :first').prop('selected',true);
});

$('#files').on('change',function(){
    var fd = new FormData();
    fd.append("file", $('#files')[0].files[0]);
    fd.append("office[]",$('#office').val());
    fd.append("url",window.location.pathname);
    fd.append("_token",$("input[name='_token']").val());

    $.ajax({
        url: 'add_document',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            if(!data){
                alert("Файл слишком большой!");
            }
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});

$('#dd').on('click',function(){
    var fd = new FormData();
    fd.append("id", $(this).data('id'));
    fd.append("office", $(this).data('office'));
    fd.append("_token",$("input[name='_token']").val());
    $.ajax({
        url: 'delete_document',
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: fd,
        success: function (data) {
            location.reload(true);
        },
        error: function(errors)
        {
            console.log(errors);
        }
    });
});

$('.ddbtn').on('click', function(){
    $('#dd').data('id',$(this).data('id'));
    $('#dd').data('office',$(this).data('office'));
    $('#delete_document').modal('show');
});

$('#save').on('click', function(){
    $('.file-name').each(function(){
        if($(this).val()!=$(this).data('original')){
            //alert();
            var fd = new FormData();
            fd.append("id", $(this).data('id'));
            fd.append("office", $(this).data('office'));
            fd.append("newName",$(this).val());
            fd.append("_token",$("input[name='_token']").val());
            $.ajax({
                url: 'update_document',
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: fd,
                success: function (data) {
                    location.reload(true);
                },
                error: function(errors)
                {
                    console.log(errors);
                }
            });
        }
    });
});