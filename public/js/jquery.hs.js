(function ($) {
    //=========================================================
    //                    HS
    //=========================================================
    //                     Содержание
    //=========================================================
    //ВАЛИДАЦИЯ
    //  Валидация текста - hs_valid_text
    //  Валидация чисел - hs_valid_num
    //  Валидация мейла - hs_valid_mail
    //---------------------------------------------------------
    //ЮЗАБИЛИТИ
    //  Скролл бар - hs_scrollbar
    //  Вложенные списки - hs_tree
    //  Сворачивание текста - hs_resize_text
    //  Прокрутка страницы вверх - hs_up_button
    //  Переход на fixed по скролу - hs_tofixed
    //  Слайд-меню - hs_slide_menu
    //  Таймер - hs_timer
    //  Галлерея - hs_gallery
    //  Прокрутка по экранам - hs_scroll_screen
    //---------------------------------------------------------
    //АНИМАЦИЯ
    // zoom
    // float shadow
    // rotate
    //---------------------------------------------------------
    ///ФУНКЦИИ
    // Эффект падающего мяча - hs_bounce
    // Проверка преобразования цвета в 16 систему - hs_transform_color
    //---------------------------------------------------------
    //=========================================================
    //                    /HS
    //=========================================================
    //---------------------------------------------------------
    //=========================================================
    //                 SELECTOR
    //=========================================================
    //---------------------------------------------------------
    // ВАЛИДАЦИЯ
    //---------------------------------------------------------
    //...................................
    // Текст
    //...................................
    //Валидация текста
    $.hs=new Object()
    /*------------------------------------------------------------------*/
    /*******************     hs_valid_text            *******************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Проверка текста на запрещенные символы
        ..................................................
        INTERFASE
        Text - текст который нужно проверить
        ArrayIllegal - массив запрещенных символов
        onValidation - пользовательская функция принимает результат проверки
        ..................................................
    */
    $.fn.hs_valid_text = function(text,illegal){
        var args = {
            Text: text,
            ArrayIllegal: illegal
        }
        return Validate;
        
        function Validate() {
            for (var i=0;i<args.Text.length;i++) {
                if (args.Text.indexOf(args.ArrayIllegal[i]) != -1){                        
                    return false;
                }
            }
            return true;
        }
    }
    // /Текст
    //...................................
    // Числа
    /*------------------------------------------------------------------*/
    /*******************     hs_valid_num          **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Проверка являеться ли данные числом (в десятичной системе, целым,вещественным через ./, )
        ..................................................
        INTERFASE
        Number - проверяемая строка
        onValidation - пользовательская функция принимает результат проверки
        ..................................................
    */
    $.fn.hs_valid_num = function(num){
        var args = {
            Number: num
        }
        return ValidateNumber();
        
        function ValidateNumber() {
            var num=args.Number;
            
            var search=num.indexOf(',')+1;
            if (search) {
                num=num.replace(',','.');
            }
            
            var count = (num.split('.').length)
            if (num.indexOf(',')!=-1 || count>2) {
                return false;
            }
            
            var abcs='0123456789.';
            
            var start=0;
            if (num[0]=='-') {
                start=1;
            }
            
            for (var i=start;i<num.length;i++) {
                if ( abcs.indexOf(num[i]) == -1) {
                    return false;
                }
            }
            
            num=parseFloat(num);
            if (isNaN(num)) {
                return false;
            }
            
            return true;
        }
    }
    // /Числа
    //...................................
    // Мейл
    /*------------------------------------------------------------------*/
    /*******************     hs_valid_mail         **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Проверка корректности мейла по стандартам RFC
        ..................................................
        INTERFASE
        Mail - проверяемый текст
        onValidation - пользовательская функция принимает результат проверки
        ..................................................
    */
    $.fn.hs_valid_mail = function(mail){
        var args = {
            Mail:mail,
        }
        return ValidateMail();
    
        function ValidateMail() {
            var length=args.Mail.length;
                var arr=args.Mail.split('@');
                var name=arr[0];
                var domen=arr[1];                
                
                if (length==0 || arr.length!=2) {
                    return false;
                }
                
                if (checkLogin(name) && checkDomen(domen)){
                    return true;
                }
                
                return false;
                
            //--------------------------------    
            function checkLogin(value) {
                var length=value.length;
                
                if (length==0 || length>64 ) {
                    return false;
                }
                
                if(value.indexOf('..')!=-1){
                    return false;
                }
                
                if (name[0]=='.' || name[length-1]=='.') {
                    return false;
                }
                
                var nums='0123456789';
                var abcsLow='abcdefghijklmnopqrstuvwxyz';
                var abcsHight=abcsLow.toUpperCase();
                var sumbols="!#$%&'*+-/=?^_`{|}~.";
                
                var legal=nums+abcsLow+abcsHight+sumbols;
                
                for (var i=0;i<length;i++) {
                    if ( legal.indexOf(value[i]) == -1 ) {
                        return false;
                    }
                }
                return true;
                
            }
            //--------------------------------
            function checkDomen(value) {
                var length=value.length;
                
                if (length==0 || length>253 ) {
                    return false;
                }
                if (value[0]=='.' || value[length-1]=='.'){
                    return false;
                }
                if (value.indexOf('.')==-1) {
                    return false;
                }
                
                var nums='0123456789';
                var abcsLow='abcdefghijklmnopqrstuvwxyz';
                var sumbol='.-';
                
                var legal=nums+abcsLow+sumbol;
                
                for (var i=0;i<length;i++) {
                    if ( legal.indexOf(value[i]) == -1 ){
                        return false;
                    }
                }
                
                var arr=value.split('.');
                
                var start=nums+'-';
                var end='-';
                
                for (var i=0;i<arr.length;i++) {
                    var tmp=arr[i];
                    if (tmp.length==0 || tmp.length>63) {
                        return false;
                    }
                    for (var j=0;j<tmp.length;j++){
                        if ( start.indexOf(tmp[j]) != -1 || end.indexOf(tmp[j]) != -1 ) {
                            return false;
                        }
                    }
                    
                }
                
                return true;
            }
        }
    }
    // /Мейл
    //---------------------------------------------------------
    // ВАЛИДАЦИЯ
    //---------------------------------------------------------
    //---------------------------------------------------------
    // ЮЗАБИЛИТИ
    //---------------------------------------------------------
    
    //...................................
    // Прокрутка страницы вверх
    /*------------------------------------------------------------------*/
    /*******************     hs_up_button          **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        анимированное появление кнопки и плавная прокрутка на вверх страницы.
        ..................................................
        INTERFASE
        AnimationTime - время анимации появления кнопки в мс, по умолчанию 300
        onScroll - пользовательская функция которая срабатывает при прокрутке, передаються true/false в зависимости того показана или скрыта кнопка.
        ..................................................
    */
    $.fn.hs_up_button = function (Args){
        var args = {
            AnimationTime: 300,//время появления/скрытия "кнопки"
            //SlideTime: 300,
            onScroll: function(){}
        }
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {
                setVars();
                setEvents();
                onScroll();
            }
            //--------------------------------
            //private
            //--------------------------------
            function setVars() {
                Button=Self;
                Delay=5;
            }
            //--------------------------------
            function moveUp(position) { // прокручивает окно на верх
                if (position==undefined) {
                    position=$(window).scrollTop();
                }
                if (position==0) {
                    return false;
                }
                
                number=position-getNumber(position);
                $(window).scrollTop(number);
                setTimeout(moveUp,Delay);
                
                return true;
            }
            //--------------------------------
            function getNumber(x) {
                var number=Math.sqrt(x);
                return number;
            }
            //--------------------------------
            function check() {  // проверяет показать или скрыть кнопку
                var positionWindow=$(window).scrollTop();
                var heightWindow=$(window).height();
                
                if (positionWindow>heightWindow) {
                    return true;
                }else{
                    return false;
                }
            }
            //--------------------------------
            function showButton() {
                $(Button).stop().fadeIn(args.AnimationTime);
                Flag=true;
            }
            //--------------------------------
            function hideButton() {
                $(Button).stop().fadeOut(args.AnimationTime);
                Flag=false;
            }
            //--------------------------------
            function setEvents() {
                $(Button).click(onClick);
                $(window).scroll(onScroll);
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            function onClick() {
                moveUp();
                return false;
            }
            //--------------------------------
            function onScroll() {
                if ( check() ) {
                    if (Flag) return;
                    
                    args.onScroll(true);
                    showButton();
                }else{
                    if (!Flag) return;
                    
                    args.onScroll(false);
                    hideButton();
                }
            }
            //--------------------------------
            //VARS
            var Self=this;
            var Button;
            var Delay;
            var Flag=true;
            //--------------------------------
            //DECLARATION
            //--------------------------------
            //INIT
            init();
        });
    }
    // /Прокрутка страницы вверх
    //...................................
    
    //...................................
    // сворачивание текста
    /*------------------------------------------------------------------*/
    /*******************     hs_resize_text         *********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Если блок больше чем MaxHeight то он будет уменьшен до этих размеров
        И появиться кнопка свернуть/развернуть 3 типов
        1 - до блока
        2 - внутри блока
        3 - после блока
        ..................................................
        INTERFASE
        ..................................................
    */
    $.fn.hs_resize_text = function (Args) {
        var args = {
            MaxHeight: 50,
            CountLine : 0,
            TypeButton: 1,
            WordOpen: 'Развернуть',
            WordClose: 'Свернуть',
            Icon: '<i class="fa fa-chevron-down"></i>',
            ToggleClasses: 'fa-chevron-up fa-chevron-down',
            CustomClass: ''
        };
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {
                check();
                setEvents();
            }
            //--------------------------------
            //private
            function check() {
                if (args.CountLine) {
                    setVars();
                }else{
                    fNormalHeight=$(fSelf).height();
                    if (fNormalHeight>args.MaxHeight) {
                        setVars();
                    }
                }
            }
            //--------------------------------
            function setVars() {
                var word='<span>'+args.WordOpen+'</span>';
                var icon=args.Icon;
                var html='<div class="hs-text-resize-button">'+word+''+icon+'</div>';
                
                switch (args.TypeButton) {
                    case 1:
                        word='<span>Развернуть текст</span>';
                        html='<div class="hs-text-resize-button hs-text-resize-button-top hs-button-hide"><div></div>'+word+'</div>';
                        $(fSelf).before(html);
                        fButton=$(fSelf).next('.hs-text-resize-button');
                        
                        break;
                    case 2:
                        html='<div class="hs-text-resize-button hs-text-resize-button-in '+args.CustomClass+' hs-button-hide"><div></div>'+icon+''+word+'</div>';
                        $(fSelf).append(html);
                        $(fSelf).css('position', 'relative');
                        fButton=$(fSelf).find('.hs-text-resize-button');
                        
                        break;
                    case 3:
                        var html='<div class="hs-text-resize-button hs-text-resize-button-bottom hs-button-hide">'+word+''+icon+'</div>';
                        $(fSelf).after(html);
                        fButton=$(fSelf).next('.hs-text-resize-button');
                        
                        break;
                }
                
                setMaxHeight();
                hide();
            }
            //--------------------------------
            function setMaxHeight() {
                var height=parseInt($(fSelf).css('line-height'));
                if (args.CountLine) {
                    args.MaxHeight=args.CountLine*height;
                }else{
                    var tmp=args.MaxHeight/height;
                    args.MaxHeight=Math.ceil(tmp)*height;
                }
            }
            //--------------------------------
            function setEvents() {
                $(fButton).click(onClick);
            }
            //--------------------------------
            function show() {
                $(fSelf).css('overflow','visible');
                $(fSelf).removeClass('hs-text-child-hidden');
                $(fSelf).height('auto');
                var height=$(fSelf).height();
                $(fSelf).addClass('hs-text-child-hidden');
                $(fSelf).height(args.MaxHeight);
                
                var css={
                    height : height
                };
                $(fSelf).animate(css,300);
                $(fButton).find('span').html(args.WordClose);
                $(fSelf).removeClass('hs-text-child-hidden');
                fResize=false;
            }
            //--------------------------------
            function hide() {
                $(fSelf).css('overflow','hidden');
                
                var css={
                    'height' : args.MaxHeight+'px'
                };
                $(fSelf).animate(css,300);
                $(fButton).find('span').html(args.WordOpen);
                $(fSelf).addClass('hs-text-child-hidden');
                fResize=true;
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            //--------------------------------
            function onClick() {
                if (fResize) {
                    show();
                }else{
                    hide();
                }
                $(fButton).find('.fa').toggleClass(args.ToggleClasses);
                $(fButton).toggleClass('hs-button-show hs-button-hide');
            }
            //--------------------------------
            //VARS
            var fSelf=this;
            var fNormalHeight;
            var fButton;
            var fResize;
            //--------------------------------
            //DECLARATION
            //--------------------------------
            //INIT
            init();
        });
    }
    // /сворачивание текста
    //...................................
    /*------------------------------------------------------------------*/
    /*******************     hs_gallery         *********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        ..................................................
        INTERFASE
        ..................................................
    */
    $.fn.hs_gallery = function (Args) {
        var args = {
            AnimationType : 'fade',
            AnimationTime : 300,
            Popup : false,
            Buttons : false
        };
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {
                //setAnimation();
                setVars();
            }
            //--------------------------------
            //private
            function setVars() {
                var window=$('.hs-gallery');
                
                fBigImg=$(window).find('.hs-gallery-big-img img');
                fList=$(window).find('.hs-gallery-list .hs-gallery-images-sketch');
                
                if (args.Popup) {
                    fPopup=$(window).find('.hs-gallery-popup');
                    fPopupButtonNext=$(fPopup).find('.hs-gallery-popup-next');
                    fPopupButtonPrev=$(fPopup).find('.hs-gallery-popup-prev');
                    fPopupButtonClose=$(fPopup).find('.hs-gallery-popup-next');
                }
                
                if (args.Buttons) {
                    fButtonPrev=$(window).find('.hs-prev-gallery');
                    fButtonNext=$(window).find('.hs-next-gallery');
                }
                
            }
            //--------------------------------
            
            //--------------------------------
            function setEvents() {
                if (args.Popup){
                    $(fBigImg).click(onClickBigImg);
                    $(fPopupButtonNext).click(onClickPopupNext);
                    $(fPopupButtonPrev).click(onClickPopupPrev);
                    $(fPopupButtonClose).click(onClickPopupClose);
                    $(fPopup).keyup(onPressClose);
                }
                
                if (args.Buttons) {
                    $(fButtonPrev).click(onClickPrev);
                    $(fButtonNext).click(onClickNext);
                }
                
                $(fList).click(onClickList);
            }
            //--------------------------------
            function animateBigImg() {
                $(fBigImg).animate({
                    'opacity' : 0.5
                }, args.AnimationTime, function(){
                    $(fBigImg).animate({
                        'opacity' : 1
                    }, args.AnimationTime);
                });
            }
            //--------------------------------
            //--------------------------------
            //public
            //--------------------------------
            //events
            //--------------------------------
            function onPressClose(object){// обработчик нажатия клавиши на клавиатуре
                if (object.which==key){
                    onHide();
                }
            }
            //--------------------------------
            function onClickPrev() {
                
            }
            //--------------------------------
            function onClickNext() {
                
            }
            //--------------------------------
            function onClickList() {
                if ($(this).hasClass(fActiveClass))
                    return;
                
                $(fList).find('.'+fActiveClass).removeClass(fActiveClass);
                $(this).addClass(fActiveClass);
                
                animatiBigImg();
                $(fBigImg).attr('src',$(this).find('>img').attr('src'));
            }
            //--------------------------------
            function onClickBigImg() {
                
            }
            //--------------------------------
            //VARS
            var fSelf=this;
            
            var fButtonPrev;
            var fButtonNext;
            var fList;
            var fBigImg;
            var fPopup;
            
            var fShow;
            var fHide;
            
            var fActiveClass='';
            //--------------------------------
            //DECLARATION
            //--------------------------------
            //INIT
            init();
        });
    }
    // /галлерея
    //...................................
    // вложенные списки
    /*------------------------------------------------------------------*/
    /*******************     hs_tree         ************************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Передаем блок внутри которого списки
        Если есть вложенные списки то к родителю добавляем кнопку +/-
        все вложенные списки по умолчанию свернуты
        ..................................................
        INTERFASE
        Сollapsed : true/false - свернуто/развернуто списко по умолчанию
        ..................................................
    */
    $.fn.hs_tree = function (Args) {
        
        var args = {
            Сollapsed : true
        };
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {                
                setVars();
                setEvents();
            }
            //--------------------------------
            //private
            function setVars() {
                if (args.Сollapsed) {
                    var html='<div class="hs-tree-box hs-tree-show"></div>';
                }else{
                    var html='<div class="hs-tree-box hs-tree-hide"></div>';
                }
                
                fLists=$(fSelf).find('li');
                $(fLists).each(function(){
                    if ( $(this).children().length>1 ){
                        $(this).append(html);
                        $(this).find('ul').css('overflow','hidden');
                    }
                });
                
                fBoxs=$(fLists).find('.hs-tree-box');                
                if (args.Сollapsed) {
                    $(fBoxs).each(function(){
                        hide.call(this);
                    });
                }
            }
            var fTime=1000;
            //--------------------------------
            function show() {
                var css={
                    'height': '100%',
                    'width' : '100%'
                }
                $(this).parent().children('ul').stop().animate(css,fTime);
                
                $(this).parent().children('ul').toggleClass('hs-tree-hide-before hs-tree-show-before');
                $(this).toggleClass('hs-tree-show hs-tree-hide');
            }
            //--------------------------------
            function hide() {
                var css={
                    'height': '0',
                    'width' : '0'
                }
                $(this).parent().children('ul').stop().animate(css,fTime,function(){
                    $(this).css('overflow','hidden');
                });
                 
                $(this).parent().children('ul').toggleClass('hs-tree-hide-before hs-tree-show-before');
                $(this).toggleClass('hs-tree-show hs-tree-hide');
            }
            //--------------------------------
            function setEvents() {
                $(fBoxs).click(onClick);
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            //--------------------------------
            function onClick() {
                var flag=$(this).hasClass('hs-tree-hide');
                if (flag) {
                    show.call(this);
                }else{
                    hide.call(this);
                }
            }
            //--------------------------------
            //VARS
            var fSelf=this;
            var fLists;
            var fBoxs;
            //--------------------------------
            //DECLARATION
            //--------------------------------
            //INIT
            init();
        });
    }
    // /вложенные списки
    //...................................
    // скролл бар
    /*------------------------------------------------------------------*/
    /*******************     hs_scrollbar         ***********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Передется блок, содержимое блока копируется в новый блок,
        Добавляется блок с полосой прокрутки
        После прокрутки до конца блока управление передается скроллу в браузере
        ..................................................
        INTERFASE
        Height - высота скролла
        changeHeight - функция для корректировки высоты скролла
        ..................................................
    */
    $.fn.hs_scrollbar = function (Args) {
        
        var args = {
            Height : 150,
            changeHeight: function(scroll){
            }
        };
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {
                if ($(fSelf).height() < args.Height) return;
                
                create();
                setVars();
                setEvents();
            }
            //--------------------------------
            //private
            function create() {
                var content=$(fSelf).html();
                $(fSelf).empty();
                
                var html='<div id="hs-scroll-id" class="hs-scroll-content">'+content+'</div>';// перенос контента
                $(fSelf).append(html);
                html='<div class="hs-scroll-bar"><div class="hs-scroll-bar-pointer"></div></div>'//создание скроллбара и ползунка
                $(fSelf).append(html);
                
                $(fSelf).css('overflow','hidden');
                $(fSelf).height(args.Height);
            }
            //--------------------------------
            function setVars() {
                fContent=$(fSelf).find('.hs-scroll-content');
                fBar=$(fSelf).find('.hs-scroll-bar');
                fScroll=$(fBar).find('.hs-scroll-bar-pointer');
                fSelf.changeHeight=args.changeHeight;
                
                
                fHeight=changeHeight(fScroll);//корректировка высоты
                if (fHeight==undefined) {
                    fHeight=$(fScroll).height();
                }
                fBottom=$(fBar).height()- fHeight;
                fStep=$(fContent).height()/fBottom;// масштаб
                //fBottom-=parseInt(args.Height)/fStep;
                fTop=0;
            }
            //--------------------------------
            function moveContent(top) {
                $(fContent).css('top',top*fStep);
            }
            //--------------------------------
            function correctValueScrolling(value,correctDirection) {
                if (value < fTop) {
                    fValue=0;
                    $('body').scrollTop( $('body').scrollTop() + correctDirection );
                }else if (value > fBottom) {
                    fValue = fBottom;
                    $('body').scrollTop( $('body').scrollTop() + correctDirection );
                }else{
                    fValue=value;
                }
            }
            //--------------------------------
            function correctValueDrag(value) {
                if (value < fTop) {
                    fValue=0;
                }else if (value > fBottom) {
                    fValue = fBottom;
                }else{
                    fValue=value;
                }
            }
            //--------------------------------
            function cursorPosition(top) {
                var cursorInScroll=top-$(fScroll).offset().top;
                
                if (cursorInScroll<0) {
                    cursorInScroll=0;
                }else if ( cursorInScroll >= fHeight ) {
                    cursorInScroll=fHeight;
                }
                
                return top - $(fBar).offset().top-cursorInScroll;
            }
            //--------------------------------
            function setEvents() {
                $(fScroll).mousedown(onDragStart);
                
                $(fBar).click(onClick);
                
                $('html').mouseup(onDragOver);
                $('html').mousemove(onDrag);
                
                // wheel
                var wheel_handle = null;
                
                var mouse_wheel = function(event) {
                    if (false == !!event) event = window.event;
                    var direction = ((event.wheelDelta) ? event.wheelDelta/120 : event.detail/-3) || false;
                    if (direction && !!wheel_handle && typeof wheel_handle == "function") {
                        if (event.preventDefault) event.preventDefault();
                        event.returnValue = false;
                        wheel_handle(direction);
                    }
                } 
                
                // скролиться только над элементом
                var set_handle = function(id, func) {
                    document.getElementById(id).onmouseover = function() {
                        wheel_handle = func;
                    }
                    document.getElementById(id).onmouseout = function() {
                        wheel_handle = null;
                    }
                }
                
                // функция которая юзаеться при скроле
                var set_red = function(direction) {
                    var correctDirection=direction*-1;// вниз + , вверх -
                    correctValueScrolling(fValue + correctDirection,correctDirection);                    
                    onScrollStart();
                }
                
                // set events
                if (window.addEventListener) window.addEventListener("DOMMouseScroll", mouse_wheel, false);
                window.onmousewheel = document.onmousewheel = mouse_wheel;
                set_handle("hs-scroll-id", set_red);
                // /wheel
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            function onDragStart() { // нажатие мышью
                $('body').addClass('hs-off-focus');
                $('body').attr('onselectstart','return false');
                fFlag=true;
            }
            //--------------------------------
            function onDragOver() { // срабатывает после отпускания кнопки
                $('body').removeClass('hs-off-focus');
                $('body').attr('onselectstart','');
                fFlag=false;
            }
            //--------------------------------
            function onClick(e) {
                var position = cursorPosition(e.pageY);
                correctValueDrag(position);
                
                $(fScroll).css('top',fValue);
                moveContent(fValue*-1);
            }
            //--------------------------------
            function onDrag(e) {
                if (fFlag) {
                    var position = cursorPosition(e.pageY);
                    correctValueDrag(position);
                    
                    $(fScroll).css('top',fValue);
                    moveContent(fValue*-1);
                }
            }
            //--------------------------------
            function onScrollStart() {
                $(fScroll).css('top',fValue);
                moveContent(fValue*-1);
            }
            //--------------------------------
            function changeHeight(obj) {
                if (fSelf.changeHeight!=undefined) {
                    return fSelf.changeHeight(obj);
                }
                return undefined;
            }
            //--------------------------------
            //VARS
            var fSelf=this;
            var fContent;
            var fBar;
            var fScroll;
            
            var fHeight;
            var fStep;
            var fFlag;
            var fFlagScroll;
            var fTop;
            var fBottom;
            var fValue=0;
            //--------------------------------
            //DECLARATION
            this.onChange;
            //--------------------------------
            //INIT
            init();
        });
    }
    // /скролл бар
    //...................................
    // чек-бокс
    /*------------------------------------------------------------------*/
    /*******************     hs_slide_menu         **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        устанавливает анимацию при наведении на пункты меню
        ..................................................
        INTERFASE
        AnimationType - Тип анимации 'fade'/'slide', по умолчанию 'slide'
        AnimationTime -  Время анимации в мс, по умолчанию 500
        AnimationDelay -  Задержка перед началом анимации в мс, по умолчанию 500
        ShowLevel -  Скрыть вложеность 3 уровня, по умолчанию true
        onHover - пользовательская функция которая срабатывает при наведении, по умолчанию function(){} 
        onLeave - пользовательская функция которая срабатывает когда курсор выходит за границы элемента, по умолчанию function(){}
        ..................................................
    */
    $.fn.hs_slide_menu = function (Args) {
        var args = {
            AnimationType: 'slide',
            AnimationTime: 500,
            AnimationDelay: 500,
            ShowLevel: true,
            onHover: function(){},
            onLeave:function(){}
        };
        
        $.extend (args, Args);
        
        this.each (function (I) {
            //CONF
            //--------------------------------
            //FUNCKTION
            function init() {
                setAnimationType();
                setVars();
                setEvents();
                
                $(fSelf).find('li>ul').each(setHeight);
                allHide();
            }
            //--------------------------------
            //private
            function setAnimationType() { // устанавливает тип анимации
                switch (args.AnimationType) {
                    
                    case 'fade': {
                        fShow=fadeShow;
                        fHide=fadeHide;
                        
                        break;
                    }
                    
                    case 'slide': {
                        fShow=slideShow;
                        fHide=slideHide;
                        
                        break;
                    }
                }
            }
            //--------------------------------
            function setVars() {
                if (args.ShowLevel) {
                    fLists=$(fSelf).find('li');
                }else{
                    fLists=$(fSelf).find('>ul>li');
                }
                if (args.onHover!=undefined) {
                    fSelf.onHover=args.onHover;
                }
                if (args.onLeave!=undefined) {
                    fSelf.onLeave=args.onLeave;
                }
            }
            //--------------------------------
            function setHeight() {  //записывает в атрибуты изначальную высоту блока
                var height=$(this).css('height');
                $(this).attr('data-height',height);
            }
            //--------------------------------
            function setEvents() {
                $(fLists).mouseenter(onHover);
                $(fLists).mouseleave(onOut);
            }
            //--------------------------------
            function allHide() {    //имитирует наведение курсора, скрывая все элементы
                var tmpTime=args.AnimationTime;
                var tmpDelay=args.AnimationDelay;
                args.AnimationTime=0;
                args.AnimationDelay=0;
                
                $(fLists).each(function(){
                    var hide=$(this).find('>ul');
                    fHide.call(hide);
                })
                
                args.AnimationDelay=tmpDelay;
                args.AnimationTime=tmpTime;
            }
            //--------------------------------
            function fadeShow() {
                $(this).css('display','block');
                var css={
                    'opacity':1
                };
                
                $(this).stop().animate(css,args.AnimationTime);
            }
            //--------------------------------
            function fadeHide() {
                var css={
                    'opacity':0
                };
                
                $(this).stop().animate(css,args.AnimationTime,function(){
                    $(this).css('display','none');
                });
            }
            //--------------------------------
            function slideShow() {
                var css={
                    'height': $(this).attr('data-height')
                };
                
                $(this).stop().animate(css,args.AnimationTime,function(){
                    $(this).css('overflow','visible');
                });
            }
            //--------------------------------
            function slideHide() {
                var css={
                    'height':0
                };
                
                $(this).css('overflow','hidden');
                $(this).stop().animate(css,args.AnimationTime);
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            //--------------------------------
            function onHover() {
                var element=$(this).find('>ul');
                
                fTimeIn=setTimeout(function(){fShow.call(element)},args.AnimationDelay);
                
                fSelf.onHover();
            }
            //--------------------------------
            function onOut() {
                
                clearTimeout(fTimeIn);
                
                var element=$(this).find('>ul');
                
                fTimeOut=setTimeout(function(){fHide.call(element)},args.AnimationDelay);
                
                fSelf.onLeave();
            }
            //--------------------------------
            //VARS
            var fSelf=this;
            var fLists;
            var fTimeIn;
            var fTimeOut;
            
            var fShow;
            var fHide;
            //--------------------------------
            //DECLARATION
            this.onHover;
            this.onLeave;
            //--------------------------------
            //INIT
            init();
        })
    }
    // /Слайд-меню
    //...................................
    // Переход на fixed по скролу
    /*------------------------------------------------------------------*/
    /*******************     hs_tofixed            **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        делает элемент fixed когда окно просмотра опускается ниже элемента
        ..................................................
        INTERFASE
        top - отступ от верхней границы экрана (значение в пикселях), по умолчанию 0 
        right - отступ от правой границы экрана (значение в пикселях) по умолчанию 0
        bottom - если указан используется вместо top (значение в пикселях)
        left - если указан используется вместо right (значение в пикселях)
        
        OnScroll - пользовательская функция которая срабатывает при прокрутке ниже элемента, по умолчанию function(){}
        ..................................................
    */
    $.fn.hs_tofixed = function (Args) {
        var args = {
            top:    0,
            right:  0,
            scrollAfter: 0,
            position : undefined,
            OnScroll: function(){}
        };
        
        $.extend (args, Args);
        
        var Css={};
        // провека какие значения ставить
        if (Args.bottom!=undefined) {
            delete args.top;
            args.bottom=Args.bottom;
            Css.bottom=args.bottom;
        }else{
            Css.top=args.top;
        }
        
        if (Args.left!=undefined) {
            delete args.right;
            args.left=Args.left;
            Css.left=args.left;
        }else{
            Css.right=args.right;
        }
        
        this.each (function (I) {
            //CONF
            var CssSelectorAdd='hs-fixed';
            //--------------------------------
            //FUNCKTION
            function init() {
                setVars();
                setEvents();
                OnScroll();
            }
            //--------------------------------
            //private
            function setVars() {
                ElementScroll=Self;
                var topSelector     = $(ElementScroll).offset().top;
                if (args.position===undefined) {
                    Position = topSelector+args.scrollAfter;
                }else{
                    Position = args.position;
                }
            }
            //--------------------------------
            function add() {    // доваить css селектор кторый сделает элемент fixed + значения top/bottom right/left
                $(ElementScroll).after('<div class="hs-fixed-emulate-block" style="height:'+$(ElementScroll).height()+'px"></div>');
                $(ElementScroll).addClass(CssSelectorAdd);
                $(ElementScroll).css(Css);
                Flag=true;
            }
            //--------------------------------
            function remove() { // удалять css селектор кторый сделает элемент fixed
                $(ElementScroll).next().remove();
                $(ElementScroll).removeClass(CssSelectorAdd);
                Flag=false;
            }
            //--------------------------------
            function check() {   // проверка показать или скрыть блок
                var positionNow=$(window).scrollTop();
                var height=$(window).height()+$(ElementScroll).height();
                if (positionNow>Position) {
                    return true;
                }else if (positionNow+height<Position){
                    return true;
                }else{
                    return false;
                }
            }
            //--------------------------------
            function setEvents() {
                $(window).scroll(OnScroll);
            }
            //--------------------------------
            //public
            //--------------------------------
            //events
            function OnScroll() {
                if ( check() ) {
                    if (Flag)
                        return;
                    args.OnScroll(true);
                    add();
                }else{
                    if (!Flag)
                        return;
                    args.OnScroll(false);
                    remove();
                }
            }
            //--------------------------------
            //VARS
            var Position;
            var ScrollingClone;
            var ElementScroll;
            var Flag;
            var Self=this;
            var StartFlag=false;
            //--------------------------------
            //INIT
            init();
        });
    }
    // /Переход на fixed по скролу
    //...................................
    /*------------------------------------------------------------------*/
    /*******************     hs_timer              **********************/
    /*------------------------------------------------------------------*/
    //...................................
    /*
        StartNumber – Число с которого начинается отсчет
        EndNumber - До которого идет отсчет
        TimeAnimation – Время анимации
        TypeAnimation -  slide/fade тип анимации
        onFinish() – пользовательская функция которая срабатывает по окончанию.
    */
    //...................................
    // ЮЗАБИЛИТИ
    $.fn.hs_timer = function (Args) {  // устанавливает таймер отсчета
        var args={
            EndNumber: 0,
            TimeAnimation: 100,
            TypeAnimation: 'slide',
            onFinish: function(){}
        }
        $.extend(args,Args);
        
        this.each (function (I) {
            //CONF
            var Time=1000;
            //--------------------------------
            //FUNCKTION
            function init() {
                setAnimationType();// устанавливает анимацию
                setEvents();
            }
            //--------------------------------
            //private
            function setAnimationType() {
                switch (args.TypeAnimation) {
                    
                    case 'fade': {
                        Animation=animationFade;
                        break;
                    }
                    
                    case 'slide': {
                        Animation=animationSlide;
                        break;
                    }
                }
            }
            //--------------------------------
            function animationSlide(number) {
                var text=String(number);
                var blocks=$(fSelf).find('div');
                var raznost=blocks.length-text.length;  // добавляем недостающее количество 0 для числа
                var str='';
                for (var i=0;i<raznost;i++)
                    str+='0';                    
                str+=text;
                for (var i=0;i<blocks.length;i++) 
                    $(blocks[i]).find('span').html(str[i]);
            }
            //--------------------------------
            function animationFade(number) {
                
            }
            //--------------------------------
            function setEvents() {
                Timer=setInterval(onTimer,Time);    // определяем таймер
            }
            //--------------------------------
            function onTimer() {
                if (args.StartNumber==args.EndNumber) {
                    onFinish();
                }
                Animation(args.StartNumber--); // вызываем анимацию передавая уменьшеное число
            }
            //--------------------------------
            //public
            function onFinish() {
                clearInterval(Timer);
                if (args.onFinish!=undefined) {
                    args.onFinish.call(fSelf);
                }
            }
            //--------------------------------
            //events
            //--------------------------------
            //VARS
            var fSelf=this;
            var Timer;
            var Animation;
            //--------------------------------
            //DECLARATION
            //--------------------------------
            //INIT
            init();
        });
    }
    // /hs_timer
    //...................................
    // Прокрутка по экранам
    /*------------------------------------------------------------------*/
    /*******************     hs_scroll_screen           *****************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Анимация увеличения/уменьшения объекта
        ..................................................
        INTERFASE
        ..................................................
    */
    $.fn.hs_scroll_screen = function(Args){
        // CONF
        var args = {
            AnimateStep    : 10,
            AnimateDellay  : 5,
            onBeforeScroll  : function () {},
            onAfterScroll   : function () {},
            onScroll        : function () {},
        };
        
        $.extend (args, Args);
        
        var Self = this;
        //--------------------------------
        // FUNCTIONS
        function init () {
            setUserEvents ();
            Self.Index = 0;
            setEvents ();
            toAnchore ();
        }
        //--------------------------------
        function setEvents () {
            $(document).mousewheel (onScroll);
        }
        //--------------------------------
        function setUserEvents () {
            Self.onBeforeScroll = args.onBeforeScroll;
            Self.onAfterScroll = args.onAfterScroll;
            Self.onScroll = args.onScroll;
        }
        //--------------------------------
        function getY () {
            var current_wrapper = $(Self)[Self.Index];
            var y = $(current_wrapper).offset ().top;
            return y;
        }
        //--------------------------------
        function setNewIndex (E) {
            var fVector = E.deltaY || E.detail || E.wheelDelta;
            var index = Self.Index;
            index -= fVector;
            
            if (index < 0) index = 0;
            if (index == Self.length) index--;
            
            Self.Index = index;
        }
        //--------------------------------
        function toScreen () {
            stop ();
            onBeforeScroll ();
            
            fTop = getY (); 
            fAnimateTimer = setTimeout (onAnimate, args.AnimateDellay);
        }
        //--------------------------------
        function fastToScreen () {
            stop ();
            onBeforeScroll ();
            
            fTop = getY ();
            $('body').scrollTop (fTop);
            
            onAfterScroll ();
        }
        //--------------------------------
        function toAnchore () {
            var index = $.inArray ($(Self.Anchore)[0], $(Self));
            Self.Index = index;
        }
        //--------------------------------
        function fastToAnchore () {
            
        }
        //--------------------------------
        function setAnchore (Anchore) {
            document.location.hash = Anchore;
        }
        //--------------------------------
        function getAnchore () {
            var anchore = document.location.hash;
            if (anchore.trim () == '') anchore = '#' + $(Self[0]).attr ('id');
            
            return anchore;
        }
        //--------------------------------
        function getIndex () {
            return fIndex;
        }
        //--------------------------------
        function setIndex (Index) {
            fVector = 1;
            if (fIndex > Index) fVector = -1;
            
            fIndex = Index;
            toScreen ();
        }
        //--------------------------------
        function chechScreen () {
            
        }
        //--------------------------------
        function animate () {
            var top = $(document).scrollTop ();
            var client_height = $(document).height () - $(window).height ();
            
            top += args.AnimateStep * fVector;
            
            if ((top >= fTop && fVector > 0) || (top <= fTop && fVector < 0) || top >= client_height) {
                top = fTop;
                $(document).scrollTop (top);
                stop ();
                onAfterScroll ();
            } else {
                $(document).scrollTop (top);
                fAnimateTimer = setTimeout (animate, args.AnimateDellay);
            }
        }
        //--------------------------------
        function stop () {
            clearTimeout (fAnimateTimer);
        }
        //--------------------------------
        // EVENTS
        function onScroll (E) {
            if (!fAnimating) {
                E.preventDefault ();
                setNewIndex (E);
                
                if (Self.onScroll != undefined) {
                    Self.onScroll.call (Self, E);
                }
            }
            
            return false;
        }
        //--------------------------------
        function onAnimate () {
            animate ();
        }
        //--------------------------------
        function onChangeScreen () {
            
        }
        //--------------------------------
        function onBeforeScroll () {
            fAnimating = true;
            
            if (Self.onBeforeScroll != undefined) {
                Self.onBeforeScroll.call (Self, Self.Index);
            }
        }
        //--------------------------------
        function onAfterScroll () {
            fAnimating = false;
            Self.Anchore = $(Self[Self.Index]).attr ('id');
            
            if (Self.onAfterScroll != undefined) {
                Self.onAfterScroll.call (Self, Self.Index);
            }
        }
        //--------------------------------
        // VARS
        var fIndex = 0;
        var fTop;
        var fVector;
        var fAnimating = false;
        
        var fAnimateTimer;
        //--------------------------------
        // INTERFACE
        // fields
        // methods
        // events
        Self.onBeforeScroll;
        Self.onAfterScroll;
        Self.onScroll;
        // properties
        Object.defineProperty (Self, 'Index', {get: getIndex, set: setIndex});
        Object.defineProperty (Self, 'Anchore', {get: getAnchore, set: setAnchore});
        //--------------------------------
        // INIT
        init ();
        return Self;
        //--------------------------------
    }
    // /Прокрутка по экранам
    //...................................
    
    //---------------------------------------------------------
    // /ЮЗАБИЛИТИ
    //---------------------------------------------------------
    //---------------------------------------------------------
    // АНИМАЦИЯ
    //---------------------------------------------------------
    //...................................
    // зум объекта
    /*------------------------------------------------------------------*/
    /*******************     hs-zoom           **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Анимация увеличения/уменьшения объекта
        ..................................................
        INTERFASE
        Zoom - коэфициент увеличения ,
            Zoom > 1 - увеличивате
            Zoom < 1 - уменьшает
            Zoom = 1 - возвращает прежний размер
        Time - время анимации в секундах
        ..................................................
    */
    /* zoom */
    $.fn.hs_zoom = function(Args){
        var args = {
            Zoom : 1.5,
            Time : 0.5
        }
        $.extend(args,Args);
        
        $(this).css({
            'transform' : 'scale(1)',
            'transition': 'all '+args.Time+'s'
        });
        
        $(this).css({
            'transform' : 'scale('+args.Zoom+')',
        });
        
    }
    /* /zoom */
    //...................................
    // Выслывающая тень под блоком
    /*------------------------------------------------------------------*/
    /*******************     hs-shadow           **********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Блок поднимается вверх, и под ним создаеться тень
        ..................................................
        INTERFASE
        Translate - изменение положения блока по оси Х в px
            Translate > 0 - Блок опускаеться 
            Translate < 0 - Блок поднимаеться 
            Translate = 0 - Не меняет положения
        Time - время анимации в секундах
        ..................................................
    */
    /* shadow */
    $.fn.hs_shadow = function(Args){
        var args = {
            Translate: -5,
            Time : 0.5
        }
        $.extend(args,Args);
        
        var block=$(this).find('.hs-animate-shadow');
        if (block.length>0) {
            block.remove();
            $(this).css({
                'transition': 'all '+args.Time+'s',
                'transform' : 'translateY(0px)'
            });
        }else{
            $(this).append('<div class="hs-animate-shadow"></div>');
            var shadow=$(this).find('.hs-animate-shadow');
            
            $(shadow).css({
                'opacity' : '0',
                'background': '#000',
                'height': '7px',
                'border-radius': '50%',
                'display': 'block',
                'margin-top': '5px',
                'transition': 'all '+args.Time+'s'
            });
            
            
            $(this).css({
                'transition': 'all '+args.Time+'s',
                'transform' : 'translateY('+args.Translate+'px)'
            });
            
            $(shadow).css({
                'opacity' : '0.3'
            });
        }
    }
    /* / shadow */
    //...................................
    // увеличение и поворот объекта
    /*------------------------------------------------------------------*/
    /*******************     hs-rotate           ************************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Поворачивает и увеличивает объект
        ..................................................
        INTERFASE
        Scale - коэфициент увеличения ,
            Scale > 1 - увеличивате
            Scale < 1 - уменьшает
            Scale = 1 - возвращает прежний размер
        Rotate - градус поворота
            Rotate > 0 - Поворот вправо
            Rotate < 0 - Поворот влево
            Rotate = 0 - Не поворачивает
        Time - время анимации в секундах
        ..................................................
    */
    $.fn.hs_rotate = function(Args){
        var args = {
            Scale : 1.4,
            Rotate : 15,
            Time : 0.5
        }
        
        $.extend(args,Args);
        
        $(this).css({
            'transform' : 'rotate(0deg) scale(1)',
            'transition': 'all '+args.Time+'s'
        });
        
        $(this).css({
            'transform' : 'rotate('+args.Rotate+'deg) scale('+args.Scale+')',
        });
    }
    /* /rotate */
    //---------------------------------------------------------
    // /АНИМАЦИЯ
    //---------------------------------------------------------
    //---------------------------------------------------------
    // ФУНКЦИИ
    //---------------------------------------------------------
    // bounce
    /*------------------------------------------------------------------*/
    /*******************     hs_bounce         ************************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Эффект падающего мячика
        ..................................................
        INTERFASE
        Start - начальное значени,
        Interval - конечное значени,
        onInterval - пользовательская функция принимает значение и таймер
        ..................................................
    */
    $.fn.hs_bounce = function (Args) {
        var args={
            Start : 0,
            Interval : 10,
            Speed : 1.3 ,
            ReboundStep : 0.15,
            onInterval : function() {}
        }
        
        $.extend(args,Args);
            
        //CONF
        //--------------------------------
        //FUNCKTION
        function init() {
            fHeight=0;
            fRebound=0;
            
            fReboundStep=args.ReboundStep;
            fSpeed=args.Speed;
            fValue=args.Start;
            
            fTimer=setInterval(function(){
                fValue=fall(fValue,args.End);
                onInterval();
            },args.Interval);
        }
        //--------------------------------
        //private
        function fall(positionNow,positionEnd) {
            positionNow += fRebound;
            fRebound += fReboundStep;
            positionNow += fSpeed;
            
            if (positionNow+fHeight>positionEnd-fHeight) {
                
                positionNow=positionEnd-fHeight*2;
                fRebound=-Math.abs(fRebound);
            }
            
            return positionNow;
        }
        //--------------------------------
        //events
        //--------------------------------
        function onInterval() {// вызывает пользовательскую функцию onInterval
            if (args.onInterval!=undefined) {
                args.onInterval(fValue,fTimer);
            }
            return '';
        }
        //--------------------------------
        //VARS
        var fSelf=this;
        var fHeight;
        var fRebound;
        var fReboundStep;
        var fValue;
        var fTimer;
        //--------------------------------
        //DECLARATION
        //--------------------------------
        //INIT
        init();
    }
    // /bounce
    //---------------------------------------------------------
    // bounce
    /*------------------------------------------------------------------*/
    /*******************    hs_transform_color       ********************/
    /*------------------------------------------------------------------*/
    /*
        ..................................................
        DESCRIPTION
        Переводит цвет в указуную систему cчисления
        ..................................................
        INTERFASE
        Color - код цвета
        System - систему в которую переводится цвет
        ..................................................
    */
    $.fn.hs_transform_color = function(Args){
        var args={
            Color: '000',
            System: 16
        }
        
        $.extend(args,Args);
        
        var color=args.Color;
        var system=args.System;
        var new_color = color.toString(system);
        
        if (new_color.length < 6) { // проверка на корректность числа
            if (new_color.length > 3 || new_color.length < 3){// если не хватает цыфр то добавляем незначащие нули
                var number=6-new_color.length;
                var str='';
                for (var i=0;i<number;i++){
                    str+='0';
                }
                new_color=str+new_color;
            }
        }
        
        return new_color;
    }
    // /hs_tranform_color
    //---------------------------------------------------------
    // /ФУНКЦИИ
    //---------------------------------------------------------
})(jQuery);