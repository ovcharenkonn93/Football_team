var x1, y1, x2, y2 = 0;
var  jcrop_api;
$(document).ready (function() {
    $("button#add_player").click(function() {
        $("div.add_player").modal("show");
    });

    $(" div.add_player").on("change", "input[name='img_player']", function(event) {
        event.preventDefault();
        if(this.files && this.files[0]) {
            $('img#img_player').show();
            var reader = new FileReader();
            reader.onload = function (e) {
                typeof jcrop_api != "undefined" ? jcrop_api.destroy() : false;
                var x = 0, y = 0, x_2 = 0, y_2 = 0;
                $("div.add_player div.modal-dialog").css({
                    width: "70%"
                });
                $('img#img_player').attr('src', e.target.result);
                $('img#img_player').attr('width','100%');
                $('img#img_player').attr('height','auto');
                setTimeout(function() {
                    console.log(jcrop_api);
                    $("input[name='div_w']").val($("img#img_player").width());
                    $("input[name='div_h']").val($("img#img_player").height());

                    if($("img#img_player").width() > $("img#img_player").height()) {
                        y = 0;
                        y_2 = $("img#img_player").height();
                        var width = ($("img#img_player").height()/4) * 3;
                        x = $("img#img_player").width()/2 - width/2;
                        x_2 = $("img#img_player").width()/2 + width/2;
                    } else {
                        x = 0;
                        x_2 = $("img#img_player").width();
                        var height = $("img#img_player").width()/3 * 4;
                        y = $("img#img_player").height()/2 - height/2;
                        y_2 = $("img#img_player").height()/2 + height/2;
                    }
                    jcrop_api = $.Jcrop($('img#img_player'),{
                        onChange: updateCoords,
                        onSelect: updateCoords,
                        minSize:  [ 3, 4 ],
                        maxSize:  [ 264, 352 ],
                        aspectRatio: 3/4,
                        setSelect: [x, y, x_2, y_2]
                    });
                }, 300);

            };
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function updateCoords(c){
    x1 = c.x;
    y1 = c.y;
    x2 = c.x2;
    y2 = c.y2;

    $("input[name='x1']").val(x1);
    $("input[name='y1']").val(y1);
    $("input[name='x2']").val(x2);
    $("input[name='y2']").val(y2);
    $("input[name='w']").val(c.w);
    $("input[name='h']").val(c.h);
}