$(document).ready(function() {
    tinymce.init({
        language: 'ru',
        theme: 'modern',
        relative_urls: false,
        selector: 'textarea.news',
        plugins : ['jbimages',
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'],
        menubar: 'edit insert view format table tools',
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages',
        toolbar2: 'print preview | forecolor backcolor emoticons',
        image_advtab: true
    });

    $("a.delete_news").click(function() {
        $("div.delete_news").modal("show");
        $("div.delete_news a#delete_news").attr("href",$(this).attr("data-url"))
    });

    $("div.ad-news").on("click","a.sort_news",function(event) {
        event.preventDefault();
        var $this = $(this).parents("div.ad-new1");
        var news_block = $(this).parents("div.ad-new1");
        var prev_news_block = $(this).parents("div.ad-new1").prev("div.ad-new1");
        var url = $(this).attr("data-href");
        var id = $(this).attr("data-id");
        var fl = prev_news_block.length;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "json",
            data: {id: id},
            success: function(response) {
                console.log(response);
                if(fl) {
                    $this.remove();
                    prev_news_block.before(news_block);
                    //parseInt(location.href.split("?")[1].split("=")[1]) == 1
                    //    ? ($("div.ad-new1:first a.sort_news").length ? $("div.ad-new1:first a.sort_news").remove() : false)
                    //    : false
                } else {
                    location.replace(location.href.split("?")[0] + "?page=" + (parseInt(location.href.split("?")[1].split("=")[1]) - 1));
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("button.title_img_modal").click(function() {
        $("div.edit_title_img_news").modal("show");
        $("div.edit_title_img_news input[name='id']").val($(this).attr("data-id"));
    });

    $("input[name='title_img']").change(function() {
        if(this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("div.edit_title_img_news div.modal-dialog").css({
                    width: "70%"
                });
                $('img#title_img').attr('src', e.target.result);
                $('img#title_img').attr('width','100%');
                $('img#title_img').attr('height','auto');
                $('img#preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
            //$('img#title_img').Jcrop({
            //    aspectRatio: 0,
            //    onChange: updateCoords,
            //    onSelect: updateCoords
            //});
        }
    });
});

function updateCoords(c) {
    var rx = 200 / c.w;
    var ry = 200 / c.h;
    console.log(c);
    $("img#preview").css({
        width: Math.round(rx * 800) + 'px',
        height: Math.round(ry * 600) + 'px',
        marginLeft: '-' + Math.round(rx * c.x) + 'px',
        marginTop: '-' + Math.round(ry * c.y) + 'px'
    });
}