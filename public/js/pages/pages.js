$(document).ready(function() {
    tinymce.init({
        language: 'ru',
        theme: 'modern',
        relative_urls: false,
        selector: 'textarea.edit_page',
        plugins : [
            'advlist autolink lists link preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern'],
        menubar: 'edit insert view format table tools',
        toolbar1: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons',
        image_advtab: true
    });

    $('.save-txt').on('click',function(){
        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent().replace(/<p>&nbsp;<\/p>/g,""));
        tinyMCE.activeEditor.setContent(tinyMCE.activeEditor.getContent().replace(/<p><br \/>/g,"<p>"));
    });

    $("select[name='name']").change(function() {
        var id = $("select#name option:selected").val();
        $.ajax({
            url:"contact/get_data",
            type:"GET",
            dataType:"json",
            data: {
                id: id
            },
            success: function(response) {
                console.log(response);
                $("p#phone_number").text(response.phone_number + " ");
                $("p#email").text(response.email);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    $("#hpic").on('change',function(){
        var fd = new FormData();
        fd.append("file", $(this)[0].files[0]);
        fd.append("url",window.location.pathname);
        fd.append("_token",$("input[name='_token']").val());
        $.ajax({
            url: 'edit_page_picture',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {
                switch (data){
                    case 1: alert("Файл слишком большой! Размер файла не должен превышать 5 Мб!");
                        break;
                    case 2: alert("Поддерживаются только файлы изображений JPG, PNG и GIF!");
                        break;
                    default:
                        location.reload(true);
                };
            },
            error: function(errors)
            {
                console.log(errors);
            }
        });
    });
});