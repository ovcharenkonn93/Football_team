﻿	
//  Функции ============================================================================================================

// Заполняем select значениями из справочника
// FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения')
function FillOptions (base, table, field, selector) {
    var element = base[table];
    $.each(element, function(index, value){
        var elementvalue = element[index];
        $(selector).append("<option value="+elementvalue["id"]+">"+elementvalue[field]+"</option>");
    });
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Квартира"
function ApartmentClick () {
    $(this).siblings().css("border-bottom","solid 1px #DDDDDD");
    $(this).css("border-bottom","none");
    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Дом"
function HouseholdClick () {
    $(this).siblings().css("border-bottom","solid 1px #DDDDDD");
    $(this).css("border-bottom","none");
    $('.llg-for-rent').hide();
    $('.llg-for-land').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-household').show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Участок"
function LandClick () {
    $(this).siblings().css("border-bottom","solid 1px #DDDDDD");
    $(this).css("border-bottom","none");
    $('.llg-for-rent').hide();
    $('.llg-for-household').hide();
    $('.llg-for-appartment').hide();
    $('.llg-for-land').show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Выбор кнопки "Аренда"
function RentClick () {
    $('.llg-for-rent').show();
}
// Выбор кнопки "Продажа"
function SaleClick () {
    $('.llg-for-rent').hide();
}
// ---------------------------------------------------------------------------------------------------------------------
// Сворачиваем критерии по клику на минус
function CriteriaContract () {
    $(this).hide();
    $(this).siblings().hide();
    $(this).siblings(".ll-search-form__diapason-label").show();
    $(this).siblings(".ll-search-form__plus").show();
}
// ---------------------------------------------------------------------------------------------------------------------
// Разворачиваем критерии по клику на плюс
function CriteriaExpand () {
    $(this).siblings().show();
    $(this).hide();
}
// ---------------------------------------------------------------------------------------------------------------------
// Изменение минимального значения слайдера
function MinCostChange () {
    var value1=$(".ll-search-form__min-value").val();
    var value2=$(".ll-search-form__max-value").val();

    if(parseInt(value1) > parseInt(value2)){
        value1 = value2;
        $(".ll-search-form__min-value").val(value1);
    }
    $(this).siblings(".ll-search-form__slider").slider("values",0,value1);
}
// ---------------------------------------------------------------------------------------------------------------------
// Изменение максимального значения слайдера
function MaxCostChange () {
    var value1=$(".ll-search-form__min-value").val();
    var value2=$(".ll-search-form__max-value").val();

    if (value2 > 1000) { value2 = 1000; $(".ll-search-form__max-value").val(1000)}

    if(parseInt(value1) > parseInt(value2)){
        value2 = value1;
        $(".ll-search-form__max-value").val(value2);
    }
    $(this).siblings(".ll-search-form__slider").slider("values",1,value2);
}
// ---------------------------------------------------------------------------------------------------------------------
// Заполняем результаты поиска
// FillSeachResult (массив с объектами, селектор блока внутри которого будут вставляться данные, номер элемента массива)
function FillSeachResult (dataarray, selector, index) {
// Читаем данные из базы данных
    function ReadField (field) {
        try {
            var dataitem = dataarray[index];
            var value= dataitem[field];
        }
        catch(error) {
            var value= '';
        }
        return value;
    }

    $(selector).show();

// Для универсальности выносим значения на верхний уровень
	try {dataarray[index].street = dataarray[index].address.street;} catch(error) {}
	try {dataarray[index].city = dataarray[index].address.type_district.city.city;} catch(error) {}
	try {dataarray[index].city = dataarray[index].address.type_administrative_district.city.city;} catch(error) {}
	try {dataarray[index].type_apartment = dataarray[index].type_apartment.type_apartment;} catch(error) {}
	try {dataarray[index].phone_mob = dataarray[index].employee_assigned.phone_mob;} catch(error) {}
	try {dataarray[index].employee_assigned_id = dataarray[index].employee_assigned.id;} catch(error) {}
	try {dataarray[index].employee_assigned_src = dataarray[index].employee_assigned.src;} catch(error) {}
	
// Заполняем стандартные текстовые блоки
    var textfields = ['name', 'surname', 'patronymic', 'city', 'district', 'district_admin', 'street', 	'floor',
        'floor_all', 'total_area', 'living_area', 'photo_count', 'text_website', 'phone_mob', 'type_apartment',
        'created_at', 'email', 'photo_count'];
    $.each(textfields, function(index, value){
		$(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).text(ReadField(value));
    });

// Заполняем стандартные блоки изображений
	var host = 'http://82.146.52.50/landlord-site/';
    var imagefields = ['cover_src', 'employee_assigned_src', 'src'];
    $.each(imagefields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr('src',host+ReadField(value));
    });

// Заполняем стандартные блоки ссылок
    var linkfields = ['', ''];
    $.each(linkfields, function(index, value){
        $(selector).find('.ll-data__'+value.replace(/[._]/g, '-')).attr("href",ReadField(value));
    });

// Заполняем нестандартные блоки
    if ((dataarray[index].special_price == '') ||
        (dataarray[index].special_price == null) ||
        (dataarray[index].special_price == 0)) {
            $(selector).find('.ll-data__new-price').text(ReadField('cost'));
            $(selector).find('.ll-data__old-price').text('');
            $(selector).find('.ll-estate-info__old-price').hide ();
            $(selector).find('.ll-search-result-list__old-price').hide ();
            $(selector).find('.ll-search-result-grid__old-price').hide ();
    } else {
        $(selector).find('.ll-data__new-price').text(ReadField('special_price'));
        $(selector).find('.ll-data__old-price').text(ReadField('cost'));
    }

    if ((dataarray[index].text_website == '') || (dataarray[index].text_website == null)) {
        $('.ll-estate-info__description').hide ();
    }

    $(selector).find('.ll-data__link').attr("href",'info/'+ReadField('id'));
    $(selector).find('.ll-data__agent-link').attr("href",'../agent-info/'+ReadField('employee_assigned_id'));

    $(selector).find('.add-to-favorites').attr("id",ReadField('id'));
    $(selector).find('.fa-star-o').attr("id",'fa-star-o'+ReadField('id'));
    star_color(); // Изменяем цвет звёздочки для избранного

	var latitude = 0;
	var longitude = 0;
	try {latitude = dataarray[index].address.latitude;} catch(error) {}
	try {longitude = dataarray[index].address.longitude;} catch(error) {}
	//if (latitude != 0) and (longitude != 0) {
		//console.log('Широта:',latitude);
		//console.log('Долгота:',longitude);
		var myMap ;
		ymaps.ready(init);
		function init () {
		myMap=new ymaps.Map("map", {
			center: [latitude, longitude],
			zoom: 7,
			type: "yandex#map",
			// Карта будет создана без
			// элементов управления.
			controls: []
		});
	   /* }, {
			searchControlProvider: 'yandex#search'
		});*/
		}
	//}

// Ещё нужно заполнить такие блоки

//ll-estate-info__title
//ll-data__adress
//ll-data__city
//ll-estate-info__date-of-publication
}

// ---------------------------------------------------------------------------------------------------
// Запрос к API для получения списка
function AjaxRequest(criteria) {
//console.log('Параметры запроса к API:', criteria);
$.ajax(
	{
		url: 'api',
		type: 'GET',
		data: criteria,
		success: function(data){
			var estate = JSON.parse(data);
			//console.log('Результат запроса к API:', estate);
						$('.data-list').show();
						$('.search-result_empty').hide();
						$('.ll-pagination').show();
						$('.ll-search-results-limit').show();
						$('.ll-search-results-sorting').show();
			
				if (1<$('.ll-search-result-list').length) { // Удалить все блоки кроме первого 
					$(".ll-search-result-list").show();
					for (var l=0;l<$('.ll-search-result-list').length; l++){
						$('.ll-search-result-list').last().remove();
					}
					$('.ll-search-result-list').last().attr("id","ll-search-result-list-item0");
				}
				
				//$(".ll-search-result-list").show();
				
				for(var i=1;i<estate.length; i++)// Создание новых блоков
				{
					estate_block = $('.ll-search-result-list').last().clone();
					estate_block.prependTo('#data-list');
					$('.ll-search-result-list').last().attr("id","ll-search-result-list-item"+i.toString());
				}
				
				if (estate.length==0){
						$('.data-list').hide();
						$('.search-result_empty').show();	
						$('.ll-pagination').hide();	
						$('.ll-search-results-limit').hide();
						$('.ll-search-results-sorting').hide();						
						}



			//$(".ll-search-result-list").hide();
			$(".ll-search-result-grid").hide();
			$.each(estate, function(index, value){
				FillSeachResult (estate, '#ll-search-result-list-item'+index, index);
				FillSeachResult (estate, '#ll-search-result-grid-item'+index, index);
			});
		}
	});
	setTimeout(paginationList, 3000);		
}

// ---------------------------------------------------------------------------------------------------
// Пагинация результатов поиска
function paginationList() {
    window.tp = new Pagination('#script-pagination', {
        itemsCount: $('.ll-search-result-list-data').length, //количество видимых блоков
        onPageSizeChange: function (ps) {
            console.log('changed to ' + ps);
        },
        onPageChange: function (paging) {
            //custom paging logic here
            console.log(paging);
            var start = paging.pageSize * (paging.currentPage - 1),
                end = start + paging.pageSize,
                $rows = $('#data-list').find('.ll-search-result-list-data');

            $rows.hide();

            for (var i = start; i < end; i++) {
                $rows.eq(i).show();
            }
        }
    });
}
// ---------------------------------------------------------------------------------------------------
// Выдеяем первый элемент табуляции
function FirstElementInTabulation () {
    $(".ll-toggle li").each(function() {
        if ($(this).css('display') !== 'none') {
            $(this).addClass("active");
            return false;
        }
    });
    $('.llg-for__all').removeClass("active");
}

// ---------------------------------------------------------------------------------------------------
// Изменяем цвет звёздочки для избранного
function star_color()
{
    order=$.cookie('favorites'); //получаем куки
    order ? order=JSON.parse(order): order=[]; //если заказ есть, то куки переделываем в массив с объектами
    var el = document.getElementById('ll-favorites-star');
    var like;

    if(order.length>0)
    {
        el.classList.remove("fa-star-o");
        el.classList.add("fa-star");
        el.style.color = '#FFD700'; //Желтый
    } else
    {
        el.classList.remove("fa-star");
        el.classList.add("fa-star-o");
        el.style.color = '#555555';//Цвет ссылки
    }

    for (var i = 0; i < 10; i++) { //10 — количество результатов поиска

        for(var j=0; j<order.length; j++) //перебираем массив в поисках наличия товара в корзине
        {
            if(order[j].item_id==i)
            {
                like = document.getElementById("fa-star-o"+i.toString());
                like.classList.remove("fa-star-o");
                like.classList.add("fa-star");
                like.style.color = '#FFD700';	//Желтый

                like = document.getElementById("grid-fa-star-o"+i.toString());
                like.classList.remove("fa-star-o");
                like.classList.add("fa-star");
                like.style.color = '#FFD700'; //Желтый
            }
        }
    }
}
// ---------------------------------------------------------------------------------------------------
// Вызываем контактную форму
function CallContactForm () {
    $('.ll-agent-contact-form-content').css('display','block');
    $('.ll-agent-contact-form-overlay-black').css('display','block');
}
// ---------------------------------------------------------------------------------------------------
// Прячем контактную форму
function HideContactForm () {
    $('.ll-agent-contact-form-content').css('display','none');
    $('.ll-agent-contact-form-overlay-black').css('display','none');
}
// =====================================================================================================================

$(document).ready (function () {

// Начальные установки - начало ----------------------------------------------------------------------------------------

    $('.ll-search-form__type-apartment').css("border-bottom","none"); // Кнопка "Квартиры"
    FirstElementInTabulation (); // Выдеяем первый элемент табуляции

// Начальные установки - конец -----------------------------------------------------------------------------------------

// Обработчики - начало ------------------------------------------------------------------------------------------------

    $('.ll-search-form__type-apartment').click (ApartmentClick); // Кнопка "Квартиры"
    $('.ll-search-form__type-household').click (HouseholdClick); // Кнопка "Дом"
    $('.ll-search-form__type-land').click (LandClick); // Кнопка "Участок"
    $('.ll-search-form__minus').click (CriteriaContract); // Сворачиваем критерии по клику на минус
    $('.ll-search-form__plus').click (CriteriaExpand); // Разворачиваем критерии по клику на плюс
    $('#ll-search-form__radio-sale').click (SaleClick); // Кнопка "Продажа"
    $('#ll-search-form__radio-rent').click (RentClick); // Кнопка "Аренда"
    $(".ll-search-form__min-value").change(MinCostChange); // Изменение минимального значения слайдера
    $(".ll-search-form__max-value").change(MaxCostChange); // Изменение максимального значения слайдера
    $('.llg-button-for-contact-form').click (CallContactForm); // Вызываем контактную форму
    $('.ll-agent-contact-form-overlay-black').click (HideContactForm); // Прячем контактную форму

// Обработчики - конец -------------------------------------------------------------------------------------------------

window.global_data; //глобальный справочник
window.max_cost = 20;

// Заполнение формы из справочников - начало ---------------------------------------------------------------------------

$.ajax({
          url:'api',
          data:
                {
                    type: 'reference',
                    tables:'all',
                    format: 'plain'
                },
          type: "GET",
          success: function (data) {
            if(data)
            {
                window.global_data = JSON.parse(data);
                //console.log('Справочники:', window.global_data);

				//Очистка полей----------------------------------------------------------------------------------------------
				$('select[name="ll-search-form__city"]').find('option').remove();
				$('select[name="ll-search-form__city"]').append('<option disabled="" selected="" value="">'+'Город'+'</option>');
				$('select[name="ll-search-form__district"]').find('option').remove();
				$('select[name="ll-search-form__subdistrict"]').find('option').remove();				
				//----------------------------------------------------------------------------------------------Очистка полей
				
                // FillOptions (data, 'Имя таблицы','Имя поля','селектор CSS куда будут подставляться значения');
                FillOptions (window.global_data, 'type_cities','city','.ll-search-form__city');
                FillOptions (window.global_data, 'type_cities','city','.ll-sell-form-city-select');
                FillOptions (window.global_data, 'type_apartment','type_apartment','.ll-search-form__apartment');
                FillOptions (window.global_data, 'type_realestate_state','state','.ll-search-form__state');
                FillOptions (window.global_data, 'type_wall_material_apartment','wall_material','.ll-search-form__walls-apartment');
                FillOptions (window.global_data, 'type_wall_material_households','wall_material','.ll-search-form__walls-households');
                FillOptions (window.global_data, 'type_entry','entry','.ll-search-form__entry');
                FillOptions (window.global_data, 'type_furniture','furniture','.ll-search-form__furniture');

				/*if (window.location.href.search('city') != -1) { //если параметры поиска переданы 
					if ($.urlParam('city') != 'null') { 
						//вписать город в select 
					}
				}*/
// Обработка района - начало -------------------------------------------------------------------------------------------
	
					var city_id = $(".ll-search-form__city").val(); //Value из списка 
					
					var list_district = [];
					var j = 0;					
                    
						for(var i=0;i<window.global_data.type_administrative_district.length; i++)
						{
							var added = 0;
							
							if(window.global_data.type_administrative_district[i].id_city==city_id)
							  {
									for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
									{
										if(window.global_data.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
										  {
											 added = 1;
										  }
									}
									
									if(added==0)
										  {
											 list_district.push(window.global_data.type_administrative_district[i]);
											 j++;
										  }									
							  }
						}
					
					//FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
					//FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');	
					if (list_district.length<2) {$('.ll-search-form__district').hide();}
						for (i in list_district)
						{
							$('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
						}					
// Обработка района - конец --------------------------------------------------------------------------------------------
            }
          }
        });

// Обработка района - начало -------------------------------------------------------------------------------------------	
$('select[name="ll-search-form__city"]').change(function() {
	
	$('select[name="ll-search-form__district"]').find('option').remove();
	$('select[name="ll-search-form__district"]').append('<option disabled="" selected="" value="">'+'Район'+'</option>');
    $('.ll-search-form__district').show();
	$('select[name="ll-search-form__subdistrict"]').find('option').remove();
	$('.ll-search-form__subdistrict').hide();
	//$('select[name="ll-search-form__subdistrict"]').append('<option disabled selected value="">'+'Ориентир'+'</option>');
	
	
	var data_district = window.global_data;
	
					var city_id = $(".ll-search-form__city").val(); //Value из списка 
					
					var list_district = [];
					var j = 0;					
                    
						for(var i=0;i<data_district.type_administrative_district.length; i++)
						{
							var added = 0;
							
							if(data_district.type_administrative_district[i].id_city==city_id)
							  {
									for(var k=0;k<list_district.length; k++)//есть ли уже такое значение
									{
										if(data_district.type_administrative_district[i].administrative_district==list_district[k].administrative_district)
										  {
											 added = 1;
										  }
									}
									
									if(added==0)
										  {
											 list_district.push(data_district.type_administrative_district[i]);
											 j++;
										  }									
							  }
						}
					
					//FillOptions (data, 'type_administrative_district','district','.ll-search-form__district');
					//FillOptions (data, 'type_subdistrict','subdistrict','.ll-search-form__subdistrict');	
						if (list_district.length<2) {$('.ll-search-form__district').hide();} 
						for (i in list_district)
						{
							$('.ll-search-form__district').append('<option value="'+list_district[i].administrative_district+'">'+list_district[i].administrative_district+'</option>');
						}		

			});
// Обработка района - конец --------------------------------------------------------------------------------------------

// Обработка ориентира - начало --------------------------------------------------------------------------------------------
$('select[name="ll-search-form__district"]').change(function() {
	
	$('select[name="ll-search-form__subdistrict"]').find('option').remove();
	$('select[name="ll-search-form__subdistrict"]').append('<option disabled="" selected="" value="">'+'Ориентир'+'</option>');
	$('.ll-search-form__subdistrict').show();
	
	var data_subdistrict = window.global_data;
	
	var district= $(".ll-search-form__district").val(); //Value из списка 
	var city_id = $(".ll-search-form__city").val(); //Value из списка 
					
					var list_subdistrict = [];
				
                    for(var i=0;i<data_subdistrict.type_administrative_district.length; i++)
						{
							if((data_subdistrict.type_administrative_district[i].administrative_district==district)&&(data_subdistrict.type_administrative_district[i].id_city==city_id))
							 {								
								list_subdistrict.push(data_subdistrict.type_administrative_district[i]);
							 }							 
						}
			
				if (list_subdistrict.length==1) { $('.ll-search-form__subdistrict').hide(); }
						for (i in list_subdistrict)
						{
							$('.ll-search-form__subdistrict').append('<option value="'+list_subdistrict[i].subdistrict+'">'+list_subdistrict[i].subdistrict+'</option>');
						}							
			  
			});			
// Обработка ориентира - конец --------------------------------------------------------------------------------------------

// Заполнение формы из справочников - конец ----------------------------------------------------------------------------

// Запуск поиска при загрузке----------------------------------------------------------------------------

	//Критерии поиска по умолчанию----------------------------------------------
	$( "#ll-search-form__estate-type_apartment" ).prop( "checked", true ); 
	$( "#ll-search-form__radio-sale" ).prop( "checked", true );
	//----------------------------------------------Критерии поиска по умолчанию
	
	$.urlParam = function(name){ //получить параметры из адресной строки
			 var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
			 return results[1] || 0;
			}
			
			var id_city = '';
			var cost = '';
			var rooms = '';
			var realestate_type_value = 'apartment';
			
			if (window.location.href.search('city') != -1) { //если параметры поиска переданы 
				if ($.urlParam('city') != 'null') { 						
					id_city = '&id_city='+$.urlParam('city');
				}
				if ($.urlParam('price') != 'null') { 						
					cost = '&cost='+$.urlParam('price');
					cost = cost.replace("-",":");
				}	
				if ($.urlParam('rooms') != 'null') { 						
					rooms = '&type_apartment='+$.urlParam('rooms');
					rooms = rooms.replace("one","1-к гостинка 12м;1-к гостинка 17м;1-к гостинка;");
					rooms = rooms.replace("two","2-к гостинка;2-к квартира;");
					rooms = rooms.replace("three","3-к квартира;");
					rooms = rooms.replace("more","4-к квартира;5-к квартира;");
				}				
				if ($.urlParam('select_type') != 'null') { 						
					realestate_type_value = $.urlParam('select_type');
				}
			}	
			var realestate_type = '&realestate_type='+realestate_type_value;			
			// Обязательные параметры: [ type, format, realestate_type]
			var criteria = 'type=search&format=plain'+realestate_type+id_city+cost; //+rooms;
			AjaxRequest(criteria); // Запрос к API для получения списка			
// Запуск поиска при загрузке - конец----------------------------------------------------------------------------

	
//максимальное значение диапазона
	
	function max_cost () {
	    $.ajax(
        {
            url: 'api',
            type: 'GET',
            data: {
                // Обязательные параметры: [ type, format, realestate_type]
                type: 'search',
                format: 'plain',
                realestate_type: 'apartment'
            },
            success: function(data){
				window.max_cost = 60;
                var estate = JSON.parse(data);
				//console.log('Недвижимость:', estate);
				
						//for(var i=0;i<window.global_estate.length; i++)
			//{ 
				//if(window.global_estate[i].cost>window.max_cost)
					//{
						window.max_cost = 50;//window.global_estate[i].cost;								
					//}
			//} 
            }
        });
	}

		//	max_cost ();
	
// Cлайдер диапазона - начало ------------------------------------------------------------------------------------------
    $(".ll-search-form__slider").slider({
        min: 0,
        max: window.max_cost,
        values: [0,1000],
        range: true,
        stop: function(event, ui) {
            $(this).siblings(".ll-search-form__min-value").val($(this).slider("values",0));
            $(this).siblings(".ll-search-form__max-value").val($(this).slider("values",1));

        },
        slide: function(event, ui){
            $(this).siblings(".ll-search-form__min-value").val($(this).slider("values",0));
            $(this).siblings(".ll-search-form__max-value").val($(this).slider("values",1));
        }
    });
// Cлайдер диапазона - конец -------------------------------------------------------------------------------------------

// Обработка изменения формы поиска - начало ---------------------------------------------------------------------------
    $(function(){
        //var ajax_timeout=false, delay_beforesend=3000;
        var ajax_timeout=false, delay_beforesend=0;
        function sendForm(){
        // Запуск поиска

            // [id_city] - id города из справочника type_cities. Значения могут быть перечислены через ';' или ':'
            var id_city = '';
            if ($(".ll-search-form__city").val() != null) {
                id_city = '&id_city='+$(".ll-search-form__city").val();
            }

            //[district] - полное название района города из справочника type_district. Значения могут быть перечислены через ';'.
            var district = '';
            if ($(".ll-search-form__district").val() != null) {
                district = '&district='+ $(".ll-search-form__district").val();
            }

            // [subdistrict] - полное название подрайона города из справочника type_district. Значения могут быть перечислены через ';'
            var subdistrict = '';
            if ($(".ll-search-form__subdistrict").val() != null) {
                subdistrict = '&subdistrict='+ $(".ll-search-form__subdistrict").val();
            }

			// [floor] - этаж. Значения могут быть перечислены через ';' или ':'
            var floor = '';
            if (($(".ll-search-form__floor .ll-search-form__min-value").val() != '') && ($(".ll-search-form__floor .ll-search-form__max-value").val() != '')) {
                floor = '&floor='+ $(".ll-search-form__floor .ll-search-form__min-value").val()+':'+$(".ll-search-form__floor .ll-search-form__max-value").val();
                console.log('floor', floor);
            }
			
			// [total_area] - общая площадь. Значения могут быть перечислены через ';' или ':'
            var total_area = '';
            if (($(".ll-search-form__total-area .ll-search-form__min-value").val() != '') && ($(".ll-search-form__total-area .ll-search-form__max-value").val() != '')) {
                total_area = '&total_area='+$(".ll-search-form__total-area .ll-search-form__min-value").val()+':'+$(".ll-search-form__total-area .ll-search-form__max-value").val();
            }

            //[sort] - поле сортировки. Поддерживаемые значения: [sort] - date_create / total_area / price / price_per_meter
            var sort = '&sort='+$(".ll-search-results-sorting select").val().split('|')[0];

            //[order] - тип сортировки. Поддерживаемые значения: [order] - asc / desc
            var order='&order='+ $(".ll-search-results-sorting select").val().split('|')[1];

            // Обязательные параметры: [ type, format, realestate_type ]
            // [format] - формат API-запроса. Возможные значения: plain, json
            // [realestate_type] - тип недвижимости. Возможные значения: apartment, rent, households, rent, commercial
			
			var realestate_type = 'apartment';
			
			if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-sale").prop("checked"))) realestate_type = 'apartment';
			if (($("#ll-search-form__estate-type_apartment").prop("checked"))&&($("#ll-search-form__radio-rent").prop("checked"))) realestate_type = 'rent';
			if ($("#ll-search-form__estate-type_household").prop("checked")) realestate_type = 'households';
			if ($("#ll-search-form__estate-type_land").prop("checked")) realestate_type = 'commercial';
			realestate_type = '&realestate_type='+realestate_type;
			
			// Обязательные параметры: [type, format, realestate_type]
            var criteria = 'type=search&format=plain'+realestate_type+id_city+district+subdistrict+floor+total_area+sort+order;
			AjaxRequest(criteria); // Запрос к API для получения списка
        }
		

        $('#ll-search-form').find('select, input').change(function(){
            if(ajax_timeout) clearTimeout(ajax_timeout);
            ajax_timeout=setTimeout(sendForm,delay_beforesend)
        })
        $('.ll-search-results-sorting').find('select, input').change(function(){
            if(ajax_timeout) clearTimeout(ajax_timeout);
            ajax_timeout=setTimeout(sendForm,delay_beforesend)
        })
    })
// Обработка изменения формы поиска - конец ----------------------------------------------------------------------------




// Фильтрация ввода в поля
        $('.ll-search-form__min-value').add('.ll-search-form__max-value').keypress(function(event){
            var key, keyChar;
            if(!event) var event = window.event;

            if (event.keyCode) key = event.keyCode;
            else if(event.which) key = event.which;

            if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
            keyChar=String.fromCharCode(key);

            if(!/\d/.test(keyChar))	return false;
        });

// Колонки одинаковой высоты
    var maxheight = 0;
    $("div.llg-columns-with-similar-height").each(function() {
        if($(this).height() > maxheight) { maxheight = $(this).height(); }
    });

    $("div.llg-columns-with-similar-height").height(maxheight);

})