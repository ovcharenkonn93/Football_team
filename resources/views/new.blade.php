@include('header',['MetaVisibility'=>'hide-meta-tags-edit'])

<div class="container">
	<div class="block-wrapper block-wrapper-main block-wrapper-new">
		<div class="row">
			@include('blocks.block-new')			
		</div>
		<div class="row">
			@include('blocks.block-new-social-buttons')				
		</div>		
	</div>			
</div>

@include('footer')
