@include('header')

<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-calendar">
		<div class="row">
			@if(!empty(Session::get('user')))
				<a class="block-news-link" href="{{ url('/add_match') }}">Добавить матч</a>
				<a class="block-news-link" href="{{ url('/add_match') }}">Добавить команду соперника</a>
			@endif
		</div>

		<div class="row">
			<div class="col-md-4 block-calendar-season">
				<label>СЕЗОН:</label>
						
				<div id="">
					<select class="block-calendar-season-list">
						<option selected="selected" value="2016-2017">2016-2017</option>
						<option value="2015-2016">2015-2016</option>
						<option value="2014-2015">2014-2015</option>
					</select>
				</div>								
			</div>
			<div class="col-md-4 col-md-offset-4 block-calendar-tournament">
				<label>ТУРНИР:</label>
						
				<div id="">
					<select class="block-calendar-tournament-list">
						<option selected="ВСЕ ТУРНИРЫ" value="2">ВСЕ ТУРНИРЫ</option>
					</select>
				</div>			
			</div>
        </div>
        <div class="row">
		
		<h3 class="text-center blue-text">Сентябрь</h3>
		<hr>		
			@if(count($matches) > 0)
				@foreach($matches as $match)
					<div class="block-calendar-item">
						<div class="block-calendar-item-date">
							<p>{{$match->date}}</p>
							<p>{{$match->type_event->title}}<p>
						</div>
						<div class="block-calendar-item-logo">
							<img src="{{ url($match->team_master->logo)}}">
							<p>{{$match->team_master->title}}</p>
						</div>
						<div class="block-calendar-item-bill">3{{$match->goals_master}}&nbsp;:&nbsp;{{$match->goals_guest}}7</div>
						<div class="block-calendar-item-logo">
							<img src="{{ url($match->team_guest->logo)}}">
							<p>{{$match->team_guest->title}}</p>
						</div>
						<div class="block-calendar-item-button">
							<button class="btn block-news-link">Отчет</button>
							@if(!empty(Session::get('user')))
								<button><i class="fa fa-edit" id="edit_match" data-id="{{$match->id}}"></i></button>
							@endif
						</div>
					</div>
				@endforeach
			@endif
        </div>	
    </div>
</div>
<script src="{{ url('public/plugins/jquery-serialize-object/jquery.serialize-object.js') }}"></script>
<script src="{{asset("public/plugins/input-mask/jquery.inputmask.bundle.js")}}"></script>
<script src="{{ url('public/js/matches/matches.js') }}"></script>
@include('lightboxes.edit_match')
@include('footer')