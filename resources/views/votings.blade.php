@include('header')
<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-main-home">
        <div class="row">
            @for($i=0;$i<=11;$i++)
                <div class="col-md-4">
                    <h4 class="block-home-title title" id="{{"month_title".$i}}"></h4>
                    <div id="{{"data_month".$i}}"></div>
                    {{--@foreach($interviews as $interview)--}}
                        {{--@if((new DateTime($interview->created_at))->format('m')==$i)--}}
                            {{--<a name="test" class="show_results" href="#test" data-value={{$interview->id}}>{{$interview->title}}</a><br />--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                </div>
            @endfor
        </div>
    </div>
</div>
@include('footer')
@include('lightboxes.show_voting_results')
<script src="/js/voting/show_results.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        var fd = new FormData();
//        fd.append("id", $(this).data('value'));
        fd.append("_token", $("input[name='_token']").val());
        $.ajax({
            url: '/voting_by_month',
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: fd,
            success: function (data) {

                var diff, title_diff;
                for(var i=0; i<12; i++){
                    title_diff = Math.abs(data.this_month-i);
                    if(i>data.this_month){
                        title_diff=data.this_month+title_diff;
                    };
                    $("#month_title"+title_diff).html(months[i]);
                }
                for(var key in data.interviews){
                    var include = "<a name='voting"+key+"' class='show_results' href='#voting"+key+"' data-value="
                            +data.interviews[key].id+">"+data.interviews[key].title+"</a><br />";
                    diff=Math.abs(data.this_month-(new Date(data.interviews[key].created_at)).getMonth());
                    if((new Date(data.interviews[key].created_at)).getMonth()>data.this_month){
                        diff=data.this_month+diff;
                    };
                    $("#data_month"+diff).append(include);
                }

                $('.show_results').on('click', function(){
                    var fd = new FormData();
                    fd.append("id", $(this).data('value'));
                    fd.append("_token", $("input[name='_token']").val());
                    $.ajax({
                        url: '/get_voting',
                        type: "post",
                        dataType: "json",
                        contentType: false,
                        processData: false,
                        data: fd,
                        success: function (data) {
                            show_results(data);
                            $('#ttl').html(data.interview.title);
                            $('#show_results').modal('show');
                        },
                        error: function (errors) {
                            console.log(errors);
                        }
                    });
                });
                //console.log((new Date(data.interviews[0].created_at)).getMonth());
//                show_results(data);
//                $('#ttl').html(data.interview.title);
//                $('#show_results').modal('show');
            },
            error: function (errors) {
                console.log(errors);
            }
        });


    });
</script>