<div class="block-partners">
   <h3>Партнеры клуба</h3>

    <div class="block-item-list">
        <div class="block-item">
            <img src="{{ url('public/uploads/partners/logo1.png')}}" alt="" title="">
        </div>
        <div class="block-item">
            <img src="{{ url('public/uploads/partners/logo2.png')}}" alt="" title="">
        </div>
        <div class="block-item">
            <img src="{{ url('public/uploads/partners/logo3.png')}}" alt="" title="">
        </div>
    </div>
</div>