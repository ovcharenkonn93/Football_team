<div class="col-md-12 margin-top-20px block-home-news">
    <h1 class="text-center">Последние новости</h1>

            <h4 class="text-center">Последние новости</h4>

            <div class="block-home-news__content">
				@foreach($news['news'] as $new)

                        <div class="block-home-news__content-row margin-top-10px">
                            <div class="block-home-news__content-img">
                                <a href="news/{{$new->id}}"><img src="{{ url($new->title_img_news)}}"></a>
                            </div>
                            <div class="block-home-news__content-text">
                                <p class="block-home-news__date">{{$new->created_at}}</p>
                                <a href="news/{{$new->id}}"><h5 class="">{{$new->name}}</h5></a>
                                <p class="clip block-home-news__content-p">
                                    {{$new->description}}
                                </p>
                            </div>
                        </div>

				@endforeach			
            </div>

    <div class="block-home-news__buttons">
        <a class="block-news-link" href="{{ url('/news') }}">Другие новости</a>
        @if(!empty(Session::get('user')))
            <a class="block-news-link" href="{{ url('/add_news') }}">Добавить новость</a>
        @endif
    </div>
	
</div>