<?php
namespace App\Http\Controllers;
use DB;
$video=DB::select("select id, user_id, link, type, match_id FROM video");
?>
<div class="block-view-video">
    @foreach($video as $v)
    @if($v->type==1)
    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$v->link}}" frameborder="0" allowfullscreen></iframe>
    @elseif($v->type==2)
    <iframe src="https://vk.com/video_ext.php?{{$v->link}}" width="560" height="315" frameborder="0" allowfullscreen></iframe>
    @elseif($v->type==3)
    <iframe width="560" height="315" src="//rutube.ru/play/embed/{{$v->link}}" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
    @elseif($v->type==4)
    <iframe src="https://player.vimeo.com/video/{{$v->link}}" width="640" height="268" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    @endif
    {{--<iframe width="560" height="315" src="https://www.youtube.com/embed/64h5KCrw1no" frameborder="0" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/64h5KCrw1no" frameborder="0" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/64h5KCrw1no" frameborder="0" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/64h5KCrw1no" frameborder="0" allowfullscreen></iframe>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/64h5KCrw1no" frameborder="0" allowfullscreen></iframe>--}}
    @endforeach
</div>