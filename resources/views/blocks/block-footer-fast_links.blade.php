<div class="block-footer-fast_links">

<h2 class="blue-text">Быстрые ссылки</h2>
	<div class="block-footer-fast_links-list">
		<ul>
			<li><a href="{{ url('/') }}">Главная</a></li>
			<li><a href="{{ url('/team') }}">Клуб</a></li>
			<li><a href="{{ url('/media') }}">Медиа</a></li>
			<li><a href="{{ url('/news') }}">Новости</a></li>
		</ul>
	</div>
</div>