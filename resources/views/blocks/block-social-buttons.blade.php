<div class="block-social-buttons">
    <ul>
        <li><a href="https://vk.com/fcoddr" title="Вконтакте"><i class="fa fa-vk"> </i></a></li>
        <li><a href="{{ url('news/rss')}}" title="Подписаться на новости"><i class="fa fa-rss"> </i></a></li>
         {{--У команды пока нет других аккаунтов--}}
        {{--<li><a href="" title="Одноклассники"> <i class="fa fa-odnoklassniki"></i></a></li>--}}
        {{--<li><a href="" title="Facebook"> <i class="fa fa-facebook"></i></a></li>--}}
        {{--<li><a href="" title="Twitter"><i class="fa fa-twitter"> </i></a></li>--}}
        {{--<li><a href="" title="Instagram"><i class="fa fa-instagram"> </i></a></li>--}}
    </ul>
 </div>