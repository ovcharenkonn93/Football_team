<div class="block-new-social-buttons">
	<ul>
		<li><a onclick="Share.vkontakte('{{ url('/news') }}/{{$id}}','{{$title}}','{{ url('/images/placeholders') }}/news{{$id}}.jpg','{{$description}}')"><i class="fa fa-vk"> </i></a></li>
		<li><a onclick="Share.odnoklassniki('{{ url('/news') }}/{{$id}}')"> <i class="fa fa-odnoklassniki"></i></a></li>
		<li><a onclick="Share.facebook('{{ url('/news') }}/{{$id}}')"> <i class="fa fa-facebook"></i></a></li>
		<li><a onclick="Share.twitter('{{ url('/news') }}/{{$id}}','{{$title}}')"><i class="fa fa-twitter"> </i></a></li>
	</ul>
</div>	


<script>
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},
	odnoklassniki: function(purl) {
		url  = 'https://connect.ok.ru/offer?';
		url += 'url='    + encodeURIComponent(purl);
		Share.popup(url);
	},
	facebook: function(purl) {
		url  = 'http://www.facebook.com/sharer.php?';
		url += 'u=' + purl;
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};
</script>