<div class="block-header-menu">
    <a href="{{ url('/') }}">Главная</a>
    <a href="{{ url('/news') }}">Новости</a>
    <a href="{{ url('/team') }}">Команда</a>
    <a href="{{ url('/calendar') }}">Календарь</a>
    <a href="{{ url('/standings') }}">Турнирная таблица</a>
    <a href="{{ url('/media') }}">Фото и видео</a>
    <a href="{{ url('/contacts') }}">Контакты</a>
</div>

<script>
jQuery('ul.nav > li').hover(function() {

    jQuery(this).find('.dropdown-menu').stop(true, true).delay(70).fadeIn();

}, function() {

    jQuery(this).find('.dropdown-menu').stop(true, true).delay(70).fadeOut();

})
</script>