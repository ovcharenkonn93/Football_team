<div class="block-home-matches">
    <ul class="nav nav-tabs">
	  @if(isset($content_matches['following_match']))
      	<li class="active"><a data-toggle="tab" href="#panel1">Следующий матч</a></li>
      @endif
	  <li><a data-toggle="tab" href="#panel2">Последний матч</a></li>
    </ul>
     
    <div class="tab-content">
	  @if(isset($content_matches['following_match']))
		  <div id="panel1" class="tab-pane fade in active">
			<p class="text-center">{{$content_matches['following_match']->date}}</p>
			  <div class="block-home-matches-image-left text-center">
				  <div class="block-home-matches-image-block">
					<img src="{{$content_matches['following_match']->team_master->logo}}" alt="" title="">
				  </div>
				<p>{{$content_matches['following_match']->team_master->title}}</p>
			  </div>
			  <div class="block-home-matches-image-right text-center">
				<div class="block-home-matches-image-block">
					<img src="{{$content_matches['following_match']->team_guest->logo}}" alt="" title="">
				</div>				  
				<p>{{$content_matches['following_match']->team_guest->title}}</p>
			  </div>
			<p class="text-center">{{$content_matches['following_match']->stadium}}</p>
		  </div>
	  @endif
      <div id="panel2" class="tab-pane fade">
        <p class="text-center">{{$content_matches['last_match']->date}}</p>
		  <div class="block-home-matches-image-left text-center">
			<div class="block-home-matches-image-block">
				<img src="{{$content_matches['last_match']->team_master->logo}}" alt="" title="">
			</div>		  
			<p>{{$content_matches['last_match']->team_master->title}}</p>
		  </div>
		  <div class="block-home-matches-image-right text-center">
				<div class="block-home-matches-image-block">
					<img src="{{$content_matches['last_match']->team_master->logo}}" alt="" title="">
				</div>			  
			<p>{{$content_matches['last_match']->team_master->title}}</p>
		  </div>
        <p class="text-center">{{$content_matches['last_match']->stadium}}</p>
      </div>
    </div>
</div>