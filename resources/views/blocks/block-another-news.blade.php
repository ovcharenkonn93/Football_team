<div class="block-another-news">
	<a href="{{'football'}}"><h2 class="text-center">Другие новости</h2></a>
		@foreach($another_news as $another_new)
				<div class="col-md-4 block-another-news__item">
					<div class="item-image">
						<a href="{{$another_new->id}}">
							<img src="{{ url($another_new->title_img_news)}}" alt="">
						</a>
					</div>
					<div class="block-another-news-item_bottom_text">
						<a href="{{$another_new->id}}">{{$another_new->name}}</a>
					</div>
				</div>
		@endforeach	
</div>