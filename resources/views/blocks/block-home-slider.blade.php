<div class="block-home-slider">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
         <!-- Маркеры слайдов -->
      <div class="carousel-indicators">
        <div data-target="#carousel-example-generic" data-slide-to="0" class="carousel-indicator carousel-indicator-1 active">Открытая тренировка прошла сегодня</div>
        <div data-target="#carousel-example-generic" data-slide-to="1" class="carousel-indicator">Новый сезон начался!</div>
        <div data-target="#carousel-example-generic" data-slide-to="2" class="carousel-indicator">Старт реконструкции стадиона</div>
        <div data-target="#carousel-example-generic" data-slide-to="3" class="carousel-indicator">Фото с матча</div>
      </div>

      <!-- Содержимое слайдов -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="{{ url('public/images/visuals/main-slide-1.jpg')}}" alt="...">
        </div>

        <div class="item">
            <img src="{{ url('public/images/visuals/main-slide-2.jpg')}}" alt="...">
        </div>

        <div class="item">
            <img src="{{ url('public/images/visuals/main-slide-3.jpg')}}" alt="...">
        </div>

      <div class="item">
          <img src="{{ url('public/images/visuals/main-slide-4.jpg')}}" alt="...">
      </div>


      </div>

    </div>
</div>
