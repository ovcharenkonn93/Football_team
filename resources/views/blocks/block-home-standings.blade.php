<div class="block-home-standings">
    <h4 class="block-home-title">
        Турнирная таблица
    </h4>
    <div class="block-home-standings-standings_table">
        <table>
            <thead>
            <tr>
                <td>№</td>
                <td>Команда</td>
                <td>И</td>
                <td>О</td>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>1</td>
                <td>"Энергомаш"</td>
                <td>1</td>
                <td>1</td>
            </tr>
            <tr>
                <td>2</td>
                <td><b>Донецкая республика</b></td>
                <td>2</td>
                <td>2</td>
            </tr>
            <tr>
                <td>3</td>
                <td>"Торпедо Москва"</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>4</td>
                <td>"Сатурн"</td>
                <td>4</td>
                <td></td>
            </tr>
            <tr>
                <td>5</td>
                <td>"Металлург"</td>
                <td>5</td>
                <td>5</td>
            </tr>
            <tr>
                <td>6</td>
                <td>"Энергомаш"</td>
                <td>6</td>
                <td>6</td>
            </tr>
            <tr>
                <td>7</td>
                <td>"Авангард"</td>
                <td>7</td>
                <td>7</td>
            </tr>
            <tr>
                <td>8</td>
                <td>"Торпедо Москва"</td>
                <td>8</td>
                <td>8</td>
            </tr>
            <tr>
                <td>9</td>
                <td>"Металлург"</td>
                <td>9</td>
                <td>9</td>
            </tr>
            <tr>
                <td>10</td>
                <td>"Энергомаш"</td>
                <td>10</td>
                <td>10</td>
            </tr>

            </tbody>

        </table>
    </div>
</div>