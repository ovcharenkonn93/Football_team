<div class="login-box ad-auth-admin">
    <div class="login-box-body">
        <div class="callout callout-danger" style="display:none">
            <h4 class="text-center"></h4>
        </div>
        <p class="login-box-msg">Смена пароля</p>
        <form action="{{URL::to("change_password")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Текущий пароль" name="old_password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Новый пароль" name="new_password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Повторите новый пароль" name="repeat_new_password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <!-- /.col -->
                <div class="col-xs-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="change_password">Сменить пароль</button>
                </div>
                <!-- /.col -->
            </div>
            <div class="row" id="response">
            </div>
        </form>
        <div class="overlay" style="display: none;">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>



<script>
    $(document).ready(function () {
        $("button#change_password").click(function (event) {
            event.preventDefault();
            var new_password = $('input[name="new_password"]').val();
            var repeat_new_password = $('input[name="repeat_new_password"]').val();
            var old_password = $('input[name="old_password"]').val();

            $.ajax({
                url: '{{URL::to("change_password")}}',
                type: "post",
                typeData: 'json',
                data: {
                    old_password: old_password,
                    new_password: new_password,
                    _token: "{{csrf_token()}}"
                },
                success: function (response) {
                    console.log(response);
                    if(response == 0) {
                        $('.login-box-body').find('.callout').css('display', 'block');
                        $('.login-box-body').find('.callout').find('h4').css('color', 'red').empty().append('Старый пароль введен не верно!');
                    }
                    else {
                        $('.login-box-body').find('.callout').css('display', 'block');
                        $('.login-box-body').find('.callout').find('h4').css('color', 'green').empty().append('Смена пароля прошла успешно!');
                        window.location.replace("login");
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });
</script>
