<div class="block-new">
	<div class="block-new-img">
		<img src="{{ url($title_img_new)}}">
	</div>
	<div class="block-new-description">
		<h2 class="">{{$title}}</h2>
	    <p>{{$created_at}}</p>
	    <p>{{$description}}</p>
	</div>
</div>

<div class="col-md-12">		
	<div class="block-new-content">
		@if(empty(Session::get('user')))
			{!! $content !!}
		@else
		<form id="edit_news" method="post" action="{{URL::to("news/edit_news")}}" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{ $id }}">
				<div class="row margin-top-20px">
					<div class='col-md-12'>
						<div class="col-md-5">
							<label class="">Дата создания:</label>
						</div>
						<div class="col-md-7 block-new__date">
							<input readonly="readonly" class="form-control" name="date_create" type="text" value="{{$created_at}}">
						</div>
					<div class="row margin-top-20px">
						<div class="col-md-5">
							<label class="field-required">Заголовок новости:</label>
						</div>
						<div class="col-md-7">
							<input class="form-control input-required" name="title" type="text" value="{{$title}}" required>
						</div>
					</div>
					<div class="row margin-top-20px">
						<div class="col-md-5">
							<label class="field-required">Краткое описание:</label>
						</div>
						<div class="col-md-7">
							<textarea class="form-control input-required block-new__description" name="description" data-orig="{{$description}}">{{$description}}</textarea>
							<p class="block-new__hint">Это описание будет выводиться на главной странице, странице со списком новостей, а также использоваться поисковыми системами на страницах результатов поиска. Создавайте отдельное описание для каждой страницы. Использование идентичных или похожих описаний на каждой странице сайта не несет никакой пользы, когда в результатах поиска появляются отдельные страницы. По возможности, создавайте описания, которые точно отражают содержание конкретной страницы.
						</div>
					</div>
					<div class="row margin-top-20px">
						<div class="col-md-5">
							<label class="field-required">Ключевые слова:</label>
						</div>
						<div class="col-md-7">
							<textarea class="form-control block-new__keywords" name="meta_keywords" id="meta_key" data-orig="{{$meta_keywords}}">{{$meta_keywords}}</textarea>
							<p class="block-new__hint">Ключевые слова должны отражать наиболее важные темы этой страницы и помогают поисковым системам лучше находить сайт. По возможности ключевые слова должны быть уникальными для каждой страницы. Список ключевых слов может быть разделен запятыми или пробелами.
						</div>
					</div>
					<div class="row margin-top-20px">
						<div class="col-md-5">
							<label class="field-required">Иллюстрация:</label>
						</div>
						<div class="col-md-7">
							<input class="input-required block-new__image-choose" name="title_img" type="file">
						</div>
					</div>
					</div>
								<div class="row margin-top-20px">
									<div class="col-md-12">
										<label class="field-required">Текст новости:</label>
										<textarea id="news" rows="10" name="content_news" class="form-control news">{{$content}}</textarea>
									</div>
								</div>
				<div class="row edit-news-submit_btn">
									<div class="col-md-offset-5 col-md-4">
										<button type="submit" class="btn btn-block btn-flat btn-primary block-news-link">
											<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения
										</button>
									</div>
						<div class="col-md-3">
										<a data-url="{{URL::to("news/delete/".$id)}}" class="delete_news btn btn-block btn-primary block-news-link">
											<i class="fa fa-remove"></i>&nbsp;Удалить новость
										</a>
						</div>
				</div>
			</div>
		</form>
		@endif
	</div>
</div>		
						

@include("lightboxes.delete_new") 