<div class="block-news">
	@foreach($news as $new)


<div class="col-lg-3 col-md-3 col-sm-5 block-news-element">
<div class="block-news-element_img">
	<a href="news/{{$new->id}}">
		<img src="{{ url($new->title_img_news)}}" alt="">
	</a>
</div>
	<div class="block-news-element_description">
		<h4><a href="news/{{$new->id}}" class="block-news-element_title">{{$new->name}}</a></h4>
		<p>{{$new->description}}</p>

	</div>
</div>



	@endforeach
	
	<div class="col-md-12 text-center ad-pagination">
		{!! $news->render() !!}
	</div>

</div>
@include("lightboxes.delete_new")