@include("lightboxes/add_voting")
<div class="block-home-votes">

    <h4 class="block-home-title title" id="ttl">
       Каким будет результат матча с «Авангардом»?
    </h4>
    <canvas id="resChart" style=" display: none; width: 100%;"></canvas>

<div class="block-votes-list" id="answer_options">
    <div class="votes-list-line">
            <div class="block-input-radio">
                <input type="radio" name="vote" value="1" id="vote1" checked />
                <label for="vote1">Победа «Донецкой Республики» в основное время</label>
            </div>
    </div>
    <div class="votes-list-line">
            <div class="block-input-radio">
                <input type="radio" name="vote" value="2" id="vote2"  />
                <label for="vote2">Победа «Донецкой Республики» в дополнительное  время</label>
            </div>
    </div>
    <div class="votes-list-line">
        <div class="block-input-radio">
            <input type="radio" name="vote" value="3" id="vote3" />
            <label for="vote3">Победа «Донецкой Республики» в серии пенальти</label>
        </div>
    </div>
    <div class="votes-list-line">
        <div class="block-input-radio">
            <input type="radio" name="vote" value="4" id="vote4"  />
            <label for="vote4">Победа «Авангарда» в основное время</label>
        </div>
    </div>
    <div class="votes-list-line">
        <div class="block-input-radio">
            <input type="radio" name="vote" value="5" id="vote5"  />
            <label for="vote5">Победа «Авангарда» в дополнительное  время</label>
        </div>
    </div>
    <div class="votes-list-line">
        <div class="block-input-radio">
            <input type="radio" name="vote" value="6" id="vote6"  />
            <label for="vote6">Победа «Авангарда» в серии пенальти</label>
        </div>
    </div>
</div>

    @if(!empty(Session::get('user')))
        <button id="add_voting_btn" class="btn btn-primary">Добавить голосование</button>
        <button id="edit_voting_btn" class="btn btn-primary">Изменить голосование</button>
    @else
        <a class="block-news-link" href="{{ url('/votes/1') }}">Голосовать</a>
        <button id="vote" data-qid="" class="btn btn-primary" style="display: none">Голосовать</button>
    @endif
    <a id="other_results" class="btn-link" style="display: none" href="{{ url('/votings') }}">Результаты остальных голосований</a>

    {{--@if(!empty(Session::get('user')))--}}
        {{--<button id="add_voting_btn" class="btn btn-primary">Добавить голосование</button>--}}
    {{--@endif--}}
    {{--Голосования - block-home-votes--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-10 col-md-offset-1" style="border: 1px solid black">--}}
            {{--<h4 class="title center-block" id="ttl"></h4>--}}
            {{--<canvas id="resChart" style=" display: none; width: 100%;"></canvas>--}}
            {{--<table id="answer_options" style="width: 100%"></table><br />--}}
            {{--<button id="vote" data-qid="" class="btn btn-primary" style="display: none">Голосовать</button>--}}
            {{--<a id="other_results" class="btn-link" style="display: none" href="{{ url('/votes/1') }}">Результаты остальных голосований</a>--}}
            {{--@if(!empty(Session::get('user')))--}}
                {{--<button id="add_voting_btn" class="btn btn-primary">Добавить голосование</button>--}}
                {{--<button id="edit_voting_btn" class="btn btn-primary">Изменить голосование</button>--}}
            {{--@endif--}}
        {{--</div>--}}

    {{--</div>--}}


</div>
<script src="/js/voting/index.js"></script>
<script src="/js/voting/add.js"></script>
<script src="/js/voting/show_results.js"></script>