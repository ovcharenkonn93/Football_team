				<div class="block-standings">
				
					<h2 class="text-center">Чемпионат года</h2>
								
					<table  class="block-standings-table1">
					<tr>
					<th><p>Место</p></th><th><p>Команда</p></th><th><p>И</p></th><th><p>В</p></th><th><p>Н</p></th><th><p>П</p></th><th><p>ГЗ</p></th><th><p>ГП</p></th><th><p>О</p></th>
					</tr>	
					<tr>
					<td><p>1</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>
					<tr>
					<td><p>2</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>
					<tr>
					<td><p>3</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>
					<tr>
					<td><p>4</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>	
					<tr>
					<td><p>5</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>	
					<tr>
					<td><p>6</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>	
					<tr>
					<td><p>7</p></td><td><img src="{{ url('public/images/logo.png')}}" alt="" title=""> <p>Энергомаш</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td><td><p>1</p></td>
					</tr>						
					</table>	

					<table  class="block-standings-table2 margin-top-50px">	
						<tr>
							<td><p><i>И</i></p></td><td><p>Игры</p></td>
							<td><p><i>П</i></p></td><td><p>Пропущено</p></td>
							<td><p><i>ГЗ</i></p></td><td><p>Голов забито</p></td>
							<td><p><i>О</i></p></td><td><p>Очки</p></td>					
						</tr>
						<tr>
							<td><p><i>В</i></p></td><td><p>Виграно</p></td>
							<td><p><i>Н</i></p></td><td><p>Ничья</p></td>
							<td><p><i>ГП</i></p></td><td><p>Голов пропущено</p></td>
							<td></td><td></td>	
						</tr>									
					</table>					
				</div>				