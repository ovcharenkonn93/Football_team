<div class="login-box ad-auth-admin">
    <div class="login-box-body">
        <div class="callout callout-danger" style="display:none">
            <h4 class="text-center"></h4>
        </div>
        <p class="login-box-msg">Авторизация</p>
        <form action="{{URL::to("login")}}" method="post">
            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Логин" name="login" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Пароль" name="password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    {{--<div class="checkbox">--}}
                        {{--<label>--}}
                            {{--<input type="checkbox" name="remember_me" id="remember_me">&nbsp;&nbsp;Запомнить меня--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    <label style="float:left">
                        <a href="{{URL::to("change_password")}}">Сменить пароль</a>
                    </label>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="auth">Войти</button>
                </div>
                <!-- /.col -->
            </div>
            <div class="row" id="response">
            </div>
        </form>
        <div class="overlay" style="display: none;">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>



<script>
    $(document).ready(function () {
        $("button#auth").click(function (event) {
            event.preventDefault();
//            $('.overlay').css('display', 'block');
            var login = $('input[name="login"]').val();
            var password = $('input[name="password"]').val();

            $.ajax({
                url: '{{URL::to("login")}}',
                type: "post",
                typeData: 'json',
                data: {
                    login: login,
                    password: password,
                    _token: "{{csrf_token()}}"
                },
                success: function (response) {
                    console.log(response.split("/")[0]);
                   if(response == 0) {
//                       $('.overlay').css('display', 'none');
                       $('.login-box-body').find('.callout').css('display', 'block');
                       $('.login-box-body').find('.callout').find('h4').css('color', 'red').empty().append('Неверное имя пользователя или пароль!');
                   }
                    else {
//                        $('.overlay').css('display', 'none');
                        $('.login-box-body').find('.callout').css('display', 'block');
                        $('.login-box-body').find('.callout').find('h4').css('color', 'green').empty().append('Авторизация успешна!');
                        response.split("/")[0] == "http:" ? window.location.replace(response)
                                : window.location.replace(""+response.split("/")[1]+"");
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });
</script>
