<div class="block-add-video">
	<button class="block-add-video__view-form">Добавить видео</button>
	<div class="block-add-video__add-form-container">
		<input type="url" class="block-add-video__link" placeholder="введите ссылку на видео">
        <input type="date" class="block-add-video__date" placeholder="выберите дату матча">
		{{--<input type="text" class="block-add-video__match" placeholder="выберите матч">--}}
        <select class="block-add-video__match" placeholder="выберите матч">
        </select>
		<button class="block-add-video__send">Отправить видео</button>
		{{--<p class="block-add-video__answer"></p>--}}
        <div class="block-add-video__answer"></div>
	</div>
</div>
<style>
	.block-add-video__add-form-container{
		display: none;
	}
</style>
<script>
	$(".block-add-video__view-form").click(function(){
		if($(".block-add-video__add-form-container").css("display")=="none"){
			$(".block-add-video__add-form-container").css("display", "block");
		}
		else{
			$(".block-add-video__add-form-container").css("display", "none");
		}
	});
    $(".block-add-video__date").change(function(){
        console.log("Дата: "+$(".block-add-video__date").val());
        $.ajax({
            url: "{{url("add_video/get_matches")}}"+'/'+$(".block-add-video__date").val(),
            type: "GET",
            success: function(answer){
                matches=JSON.parse(answer);
                //sel="<select class=\"block-add-video__match\" placeholder=\"выберите матч\">";
                sel="";
                matches.forEach(function(item){
                    sel+="<option value='"+item.id+"'>"+item.team_master+" - "+item.team_guest+"</option>";
                });
                //sel+="</select>";
                $(".block-add-video__match").html(sel);
            }
        })
    });
	$(".block-add-video__send").click(function(){
        console.log($(".block-add-video__date").val());
		console.log("Функция $(\".block-add-video__send\").click() запущена");
		$.ajax({
			url: "{{url("add_video")}}",
			type: "GET",
			data: {
				link: $(".block-add-video__link").val(),
				match: $(".block-add-video__match").val()
			},
			success: function(answer){
				console.log("Ветка success, ответ сервера: "+answer);
				/*if(answer==1){					
					$(".block-add-video__answer").text("Видео успешно добавленно");
					$(".block-add-video__answer").css("color", "#2010ff");
				}
				else{
					$(".block-add-video__answer").text("Ошибка добавления видео");
					$(".block-add-video__answer").css("color", "#ff1020");
				}*/
                $(".block-add-video__answer").html(answer);
			},
			error: function(){
				$(".block-add-video__answer").text("Ошибка запроса к серверу");
				$(".block-add-video__answer").css("color", "#ff1020");
			}
		});
	});
</script>