<div class="ll-menu-panel-left">
	<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>

	<div class="ll-menu-panel-left-list">
		  <div class="list-group panel">
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/') }}">Главная</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/news') }}">Новости</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/team') }}">Команда</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/calendar') }}">Календарь</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/standings') }}">Турнирная таблица</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/media') }}">Фото и видео</a>
			  <a class="list-group-item" data-parent="#SubMenu1" href="{{ url('/contacts') }}">Контакты</a>
		  </div>
		</div>	
		
</div>
<a class="ll-menu-panel-trigger" href="#"><i class="fa fa-reorder"></i></a>

<script type="text/javascript">
$(document).ready(function(){
    $(".ll-menu-panel-trigger").click(function(){
        $(".ll-menu-panel-left").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
});

$(document).click( function(event){
	if( $(event.target).closest('.ll-menu-panel-left').length )
		return;
	$('.ll-menu-panel-left').fadeOut("normal");
	event.stopPropagation();
});

</script>