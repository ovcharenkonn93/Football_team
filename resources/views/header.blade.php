<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="{{$description}}">
    <meta name="keywords" content=" {{$meta_keywords}} "/>
    <title>ФК «Донецкая Республика» — {{$title}}</title>
    <!-- <link href="{{ url('public/css/menu.css') }}" rel="stylesheet">  -->
    <link href="{{ url('public/css/reset.css') }}" rel="stylesheet">
    <link href="{{ url('public/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ url('public/css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('public/css/football.css') }}" rel="stylesheet">
    <link href="{{ url('public/plugins/Jcrop/css/jquery.Jcrop.css') }}" rel="stylesheet">
    <![if !IE]>
    <link href="{{ url('public/css/ie.css') }}" rel="stylesheet">
    <![endif]>

    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto+Condensed">

    <!-- temporarily css style -->
    <link rel="stylesheet" href="{{ url('css/style.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ url('public/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ url('public/js/bootstrap.js') }}"></script>
    <script src="{{ url('public/js/jquery.bootstrap-growl.min.js') }}"></script>
    <script src="{{ url('public/js/slider/highlight.pack.js') }}"></script>
    <script src="{{ url('public/js/slider/tabifier.js') }}"></script>
    <script src="{{ url('public/js/slider/js.js') }}"></script>
    <script src="{{ url('public/js/slider/jPages.js') }}"></script>
    <script src="{{ url('public/js/jquery-ui.js')}}"></script>

    <script src="{{asset("public/plugins/tinymce/tinymce.min.js")}}"></script>
    <script src="{{asset("public/plugins/Jcrop/js/jquery.Jcrop.min.js")}}"></script>
    <script src="{{asset("public/plugins/Chartjs/Chart.js")}}"></script>
    {{--<script src="{{asset("public/plugins/Chartjs/dx.chartjs.js")}}"></script>--}}
    {{--<script src="{{asset("public/plugins/Chartjs/globalize.min.js")}}"></script>--}}
    <script src="{{asset("public/js/news/news.js")}}"></script>
    <script src="{{asset("public/js/pages/pages.js")}}"></script>

    <!-- pagination -->
    <script src="{{asset("public/js/pagination.js")}}"></script>
    <!-- pagination -->

</head>

<body>

@include('blocks.block-menu-panel-left')

<div class="block-wrapper-header">
    <div class="container">
        <div class="row">
            <div id="left_side-of-header_menu" class="col-md-8">
                @include('blocks.block-header-logo')
            </div>
            <div id="left_side-of-header_menu" class="col-md-4 text-center">
                @include('blocks.block-social-buttons')
            </div>
        </div>
        <div id="right_side-of-header_menu" class=" col-md-12">
            @include('blocks.block-header-menu')
        </div>

    </div>
</div>
<div class="container">
    <div class="block-wrapper block-wrapper-meta-edit">
        @if(empty(Session::get('user')))
            {{--мета теги --}}
        @else
            <div class="row {{ $MetaVisibility or '' }}">
                @include('blocks.block-meta-edit')
            </div>
        @endif
    </div>
</div>
{{--csrf - токен--}}
<input type="hidden" name="_token" value="{{ csrf_token() }}">