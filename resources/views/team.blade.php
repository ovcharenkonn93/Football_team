@include('header')
<script src="{{ url('public/js/team/team.js') }}"></script>
<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-team">
        @if(Session::get('user'))
            <div class="row">
                <div class="col-md-offset-4 col-md-3">
                    <button class="btn btn-primary" id="add_player">Добавить нового игрока</button>
                </div>
            </div>
        @endif
        @foreach($list_players as $key => $position)
            <div class="row">
                <div class="col-md-offset-3 col-md-6" style="border-bottom: 3px solid red">
                    <h2 class="text-center">{{$position['title']}}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        @foreach($position['list_players'] as $player)
                            <div class="col-md-3 block-team__player">
                                <div class="row">
                                    <img src="{{ url($player->portrait) }}" alt="" title="">
                                </div>
                                <div class="row block-team__player-title">
                                    <div class="col-md-2">
                                        <h3>{{$player->number}}</h3>
                                    </div>
                                    <div class="col-md-10">
                                        <h3>{{$player->name}}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        @endforeach
                    </div>
                </div>
                {{--<img src="{{ url('public/uploads/team/player')}}{{ $i }}.jpg" alt="" title="">--}}
            </div>
        @endforeach
    </div>
</div>
@include('lightboxes.add_player')
@include('footer')