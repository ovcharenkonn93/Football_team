@include('header')

<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-media">
        <div class="row">
            @if(!empty(Session::get('user')))
                @include("blocks.block-add-video")
            @endif
            @include("blocks.block-view-video")
        </div>
    </div>
</div>

@include('footer')