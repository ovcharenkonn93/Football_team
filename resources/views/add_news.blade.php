@include('header',['MetaVisibility'=>'hide-meta-tags-edit'])
<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-add-news">
        <div class="row">
            <div class="col-md-12 margin-top-20px">
                <div class="col-md-offset-2 col-lg-8 col-md-8 col-sm-8 form_add_news">
                    <div class="col-md-12">
                <div class="box">
                    <div class="box-header block-wrapper-title">
                        <h1>Добавление новости</h1>
                    </div>
                    <form id="add_news" method="post" action="{{URL::to("add_news")}}" enctype="multipart/form-data">
                        <div class="box-body collapse in" id="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-5">
                                    <label class="">Дата</label>
                                </div>
                                <div class="col-md-4">
                                    <input readonly="readonly" class="form-control" name="date_create" type="text" value="{{date('d.m.Y H:i:s')}}">
                                </div>
                            </div>
                            <div class="row margin-top-20px">
                                <div class="col-md-5">
                                    <label class="field-required">Заголовок новости</label>
                                </div>
                                <div class="col-md-7">
                                    <input class="form-control input-required" name="title" type="text" required>
                                </div>
                            </div>
                            <div class="row margin-top-20px">
                                <div class="col-md-5">
                                    <label class="field-required">Краткое описание</label>
                                </div>
                                <div class="col-md-7">
                                    <input class="form-control input-required" name="description" type="text" required>
                                </div>
                            </div>
                            <div class="row margin-top-20px">
                                <div class="col-md-5">
                                    <label class="field-required">Иллюстрация</label>
                                </div>
                                <div class="col-md-7">
                                    <input class="input-required" name="title_img" type="file" required>
                                </div>
                            </div>
                            <div class="row margin-top-20px">
                                <div class='col-md-12'>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        {{--<label class="">Контент</label>--}}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea id="news" rows="10" name="content_news" class="form-control news"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row" style="margin: 20px 0 20px 0">
                                <div class="col-md-offset-7 col-md-5">
                                    <button type="submit" class="btn btn-block btn-flat btn-primary block-news-link">
                                        Опубликовать новость
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('footer')