@include('header')


<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-change_password">
        <div class="row text-center">
            @include('blocks.change_password')
        </div>
    </div>
</div>

@include('footer')