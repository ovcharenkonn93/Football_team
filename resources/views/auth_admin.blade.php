@include('header')


<div class="container">
	<div class="block-wrapper block-wrapper-main block-wrapper-auth-admin">
		<div class="row text-center">
			@include('blocks.auth_admin')
		</div>
	</div>
</div>

@include('footer')