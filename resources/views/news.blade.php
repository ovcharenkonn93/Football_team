@include('header')

<div class="container">
	<div class="block-wrapper block-wrapper-main block-wrapper-news">
        @if($no_find_news)
<h2 style="text-align: center; color: red;">Новость не найдена!</h2>
@endif
		@include('blocks.block-news')
	</div>
</div>

@include('footer')
