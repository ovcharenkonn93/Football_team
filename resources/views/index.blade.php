@include('header')

<div class="container">
	<div class="block-wrapper block-wrapper-main block-wrapper-main-home">
		<div class="row">
			<div class="col-md-7 padding_left-right-0">
				@include('blocks.block-home-slider') <!-- Последние фотоальбомы -->
				@include('blocks.block-home-news')  <!-- Новости -->
				@include('blocks.block-partners')  <!-- Партнеры -->
			</div>
			<div class="col-md-5 padding-right-0">
				@include('blocks.block-home-matches')  <!-- Матчи -->
				@include('blocks.block-home-standings')  <!-- Турнирная таблица -->
				@include('blocks.block-home-votes')  <!-- Голосования -->
			</div>
		</div>
    </div>
</div>

@include('footer')