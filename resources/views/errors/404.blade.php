<html>
	<head>
	<link href="{{ url('public/css/bootstrap.css') }}" rel="stylesheet">
		<style>
			body{
				background-color: #F5F5F5;
				font-family: 'San Francisco', 'Arial', sans-serif;	
			}
			.error_page{
				width: 100%;
				height: 100%;
			}
			.error {
				width: 100%;
				color: #ffc100;
				font-size: 130px;
				text-align: center;
				pading: 30px;
				margin-top: 30px;
				border-bottom: 2px solid #ffc100;
			}
			.error img {
				width:150px;
				height: auto;
				margin:0 auto 30px auto;
			}
			h1, h2 {
				color: gray;
				text-align: center;
				font-weight: lighter;
				padding:20px 0;
			}
			h2 {
				font-size:26px;
			}
			a {
				font-weight: lighter;
			}
			iframe {
				border: none;
				min-height: 470px;
				margin-top:-30px;
			}
		</style>
	</head>

	<body>
		<div class="error_page">
			<div class="container">
				<div class="row">
					<div class="error">
						<a href="{{ url('/')}}"><img src="{{ url('public/images/logo.png')}}" alt="" title=""></a>
						404
					</div>
					<h1>Такой адрес не найден</h1>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h2>Почему страницы нет?</h2>
						<p>Это могло произойти по нескольким причинам:
						<ul>
							<li>страница была перемещена по другому адресу, удалена или никогда не существовала
							<li>была допущена ошибка или опечатка в написании ссылки
						</ul>
					</div>
					<div class="col-md-6">
						<h2>Что делать?</h2>
						<p>Если Вы вводили адрес самостоятельно, то могли допустить ошибку. В этом случае проверьте написание ссылки и исправьте на правильный вариант.
						<p>Также ниже Вы можете перейти по одной из ссылок из списка страниц на сайте или отправить сообщение разработчикам сайта.
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h2>Список страниц на сайте:</h2>
						@include("blocks.block-sitemap")
					</div>
					<div class="col-md-6">
						<h2>Отправить сообщение разработчикам сайта</h2>
						<iframe src="{{ url('/public/feedback/feedback.htm') }}" width="100%" />
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
