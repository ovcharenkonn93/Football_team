@include('header',['MetaVisibility'=>'hide-meta-tags-edit'])
<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-add-matches">
        <div class="row">
            <div class="col-md-12 margin-top-20px">
                <div class="col-md-offset-2 col-lg-8 col-md-8 col-sm-8 form_add_news">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header block-wrapper-title">
                                <h1>Добавление матча</h1>
                            </div>
                            <form name="add_matches" method="post" action="{{URL::to("add_new_match")}}" enctype="multipart/form-data">
                                <div class="box-body collapse in" id="form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="">Дата проведения</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control" name="date" type="text">
                                        </div>
                                        {{--<div class="col-md-3">--}}
                                            {{--<div class="end">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="row margin-top-20px">
                                        <div class="col-md-4">
                                            <label class="field-required">Тип турнира</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="type_event" class="form-control" required>
                                                @foreach($type_events as $event)
                                                    <option value="{{$event->id}}">{{$event->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row margin-top-20px">
                                        <div class="col-md-4">
                                            <label class="field-required">Тип матча</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="type_match" class="form-control" required>
                                                <option value="0">Хозяин</option>
                                                <option value="1">Гость</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row margin-top-20px">
                                        <div class="col-md-4">
                                            <label class="field-required">Команда соперника</label>
                                        </div>
                                        <div class="col-md-7">
                                            <select name="team_rival" class="form-control" required>
                                                @foreach($football_teams as $team)
                                                    <option value="{{$team->id}}">{{$team->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <i class="fa fa-2x fa-plus new_rival" style="cursor:pointer"></i>
                                        </div>
                                    </div>
                                    <div class="row margin-top-20px">
                                        <div class="col-md-4">
                                            <label class="">Стадион</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control" name="stadium" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="row" style="margin: 20px 0 20px 0">
                                        <div class="col-md-offset-7 col-md-5">
                                            <button type="submit" class="btn btn-block btn-flat btn-primary block-news-link" onclick="return add_match(this)">
                                                Добавить матч
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('footer')
<script src="{{ url('public/plugins/jquery-serialize-object/jquery.serialize-object.js') }}"></script>
<script src="{{asset("public/plugins/input-mask/jquery.inputmask.bundle.js")}}"></script>
<script src="{{ url('public/js/matches/matches.js') }}"></script>