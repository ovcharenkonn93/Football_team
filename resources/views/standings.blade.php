@include('header')

<div class="container">
    <div class="block-wrapper block-wrapper-main block-wrapper-standings">
        <div class="row">
			<div class="col-md-12">				
				@include('blocks.block-standings')
			</div>
        </div>
    </div>
</div>

@include('footer')