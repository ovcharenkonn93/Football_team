<div class="modal fade delete_cultivation" id="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Вы хотите удалить запись?</h4>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" id="delete_cultivation">
                    <i class="fa fa-remove"></i>&nbsp;Удалить
                </a>
                <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>