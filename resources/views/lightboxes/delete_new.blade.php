<div class="modal fade delete_news" id="">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Вы хотите удалить новость?</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            {{--<div class="modal-body">--}}

            {{--</div>--}}
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <a class="btn btn-primary" id="delete_news">
                    <i class="fa fa-remove"></i>&nbsp;Удалить
                </a>
                <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>