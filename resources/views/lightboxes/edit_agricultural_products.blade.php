<div class="modal fade edit_agricultural_products">
    <div class="modal-dialog">
        <form action="{{URL::to("agricultural-products/update")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Добавление удобрения или средств защиты растений</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Название</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Описание</label>
                        </div>
                        <div class="col-md-6">
                            <textarea name="description" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Стоимость</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="cost" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Действие</label>
                        </div>
                        <div class="col-md-6">
                            <select name="action" class="form-control">
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="edit_agricultural_products" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>