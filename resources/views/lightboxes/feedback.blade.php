<div id="feedback-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                {{--<iframe src="https://agrodonbass.on.spiceworks.com/portal" width="100%" />--}}
                <iframe src="{{ url('/public/feedback/feedback.htm') }}" width="100%" />
            </div>
            <!-- Футер модального окна -->
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>