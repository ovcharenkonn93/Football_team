<div class="modal fade add_voting">
    <div class="modal-dialog">
        {{--<form action="{{URL::to("/add_voting")}}" method="post">--}}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="title">Добавление опроса</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div id="content" class="row margin-top-10px">
                        <div class="col-md-12">
                            <label for="interview_name">Название опроса</label>
                            <input class="form-control" id="interview_name" />
                            <br />
                            <label for="interview_questions">Ответы</label>
                            <table id="interview_questions" class="table" style="width:100%">
                                <tr>
                                    <td>Какой-то вопрос</td>
                                    <td><a><i class="fa fa-2x fa-arrow-up"></i></a>
                                        <a><i class="fa fa-2x fa-remove"></i></a></td>
                                </tr>
                                <tr>
                                    <td><input id="add_question" class="form-control" placeholder="Текст вопроса"/></td>
                                    <td><a><i class="fa fa-2x fa-plus"></i></a></td>
                                </tr>
                            </table>
                        </div>

                        <div></div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="add_interview" type="button">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button class="btn btn-primary" id="edit_interview" type="button">
                        <i class="fa fa-save"></i>&nbsp;Сохранить
                    </button>
                    <button type="button" id="close" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        {{--</form>--}}
    </div>
</div>