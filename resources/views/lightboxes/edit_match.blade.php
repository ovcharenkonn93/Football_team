<div class="modal fade edit_match">
    <div class="modal-dialog">
        <form action="{{URL::to("calendar/update_match")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Редактирование матча</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-3">
                            <label>Дата проведения</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" name="date" type="text">
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-3">
                            <label>Тип турнира</label>
                        </div>
                        <div class="col-md-9">
                            <select name="type_event" class="form-control" required>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-3">
                            <label>Тип матча</label>
                        </div>
                        <div class="col-md-9">
                            <select name="type_match" class="form-control" required>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-3">
                            <label>Команда соперника</label>
                        </div>
                        <div class="col-md-9">
                            <select name="team_rival" class="form-control" required>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-3">
                            <label>Стадион</label>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" name="stadium" type="text">
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-floppy-o"></i>&nbsp;Сохранить изменения
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>