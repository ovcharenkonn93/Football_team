<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<div class="modal fade add_player">
    <div class="modal-dialog">
        <form action="{{URL::to("team/add_player")}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Добавление игрока</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Имя игрока</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="name_player" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Номер игрока</label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" name="number_player" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Позиция</label>
                        </div>
                        <div class="col-md-6">
                            <select name="position_player" id="" class="form-control">
                                @foreach($list_position as $position)
                                    <option value="{{$position->id}}">{{$position->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Портрет</label>
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-2 col-md-8 col-sm-8 col-xs-8" id="img_player">
                                    <img id="img_player" src="#" alt="" hidden/>
                                    <input type="file" name="img_player" required/>
                                </div>
                                <input type="hidden" name="x1">
                                <input type="hidden" name="y1">
                                <input type="hidden" name="x2">
                                <input type="hidden" name="y2">
                                <input type="hidden" name="w">
                                <input type="hidden" name="h">
                                <input type="hidden" name="div_w">
                                <input type="hidden" name="div_h">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>