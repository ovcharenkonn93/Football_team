<div class="modal fade edit_title_img_news">
    <div class="modal-dialog">
        <form method="post" action="{{URL::to('edit_title_img_news')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Изменение титульной картинки для новости</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">

                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <img id="title_img" src="#" alt=""/>
                                <input type="file" name="title_img" required/>
                            </div>
                        </div>

                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary"  type="submit">
                        <i class="fa fa-edit"></i>&nbsp;Изменить
                    </button>
                    <button type="button" id="close" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>