<div class="modal fade add_vacancies">
    <div class="modal-dialog">
        <form action="{{URL::to("vacancies/add")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Добавление вакансии</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Филиал</label>
                        </div>
                        <div class="col-md-6">
                            <select name="department" class="form-control" required>
                                @foreach($departments as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Вакансия</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="vacancies" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Пол</label>
                        </div>
                        <div class="col-md-6">
                            <select name="gender" class="form-control" required>
                                <option value="-1">Любой</option>
                                <option value="1">Мужской</option>
                                <option value="0">Женский</option>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Образование</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="education" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Опыт работы</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="experience" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Занятость</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="employment" class="form-control" required>
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="add_vacancies" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>