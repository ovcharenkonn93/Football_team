<div id="show_results" class="modal fade">
    <div class="modal-dialog">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">х</button>
                <h4 class="title">Результаты голосования</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div id="content" class="row margin-top-10px">
                    <div class="col-md-12">
                        <h4 class="block-home-title title" id="ttl">
                            Каким будет результат матча с "Авангардом"?
                        </h4>
                        <canvas id="resChart" style="width: 100%;"></canvas>
                    </div>
                </div>
            </div>
            <!-- Футер модального окна -->
            {{--<div class="modal-footer">--}}
                {{--<button class="btn btn-primary" id="add_interview" type="button">--}}
                    {{--<i class="fa fa-plus"></i>&nbsp;Добавить--}}
                {{--</button>--}}
                {{--<button class="btn btn-primary" id="edit_interview" type="button">--}}
                    {{--<i class="fa fa-save"></i>&nbsp;Сохранить--}}
                {{--</button>--}}
                {{--<button type="button" id="close" class="btn btn-primary" data-dismiss="modal">Закрыть</button>--}}
            {{--</div>--}}
        </div>
    </div>
</div>