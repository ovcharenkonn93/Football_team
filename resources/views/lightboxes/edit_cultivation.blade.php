<div class="modal fade edit_cultivation">
    <div class="modal-dialog">
        <form action="{{URL::to("cultivation/update")}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4>Добавление обработки земли</h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Вспашка зяби</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="plowing" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Агрегаты</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="aggregates" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Стоимость услуг</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="cost_services" class="form-control" required>
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Максимальная стоимость услуг</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="cost_services_max" class="form-control">
                        </div>
                    </div>
                    <div class="row margin-top-10px">
                        <div class="col-md-6">
                            <label>Единицы измерения</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="units" class="form-control">
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button class="btn btn-primary" id="edit_vacancies" type="submit">
                        <i class="fa fa-edit"></i>&nbsp;Редактировать
                    </button>
                    <button type="button" id="close" class="btn btn-default btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>