		<footer id="footer" class="footer">
			<div class="container">			
				<div class="llg-wrapper llg-wrapper-footer">					
					<div class="col-md-6">
						@include('blocks.block-footer-fast_links')
					</div>	
					<div class="col-md-6">
						@include('blocks.block-footer-contacts')
					</div>					
				</div>
			</div>
			
            <div class="block-footer-login" id="icon-foot">

                      @if(empty(Session::get('user')))
                        <a href="{{URL::to('login')}}" title="Войти в режим редактирования">  <i class="fa fa-2x fa-pencil-square-o "></i> </a>
                      @else
						<a id="feedback-link" title="Отправить сообщение разработчикам" data-toggle="modal" data-target="#feedback-modal"> <i class="fa fa-2x fa-comments-o"></i> </a>
                        <a href="{{URL::to('logout')}}" title="Выйти из режима редактирования">  <i class="fa fa-2x fa-sign-out"></i> </a>
                      @endif
             </div>
		</footer>
		@include('lightboxes.feedback')
	</body>
</html>