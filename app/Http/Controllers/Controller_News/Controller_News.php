<?php

namespace App\Http\Controllers\Controller_News;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResizeImage;
use App\Models\Models_Football\Football_Teams;
use App\Models\Models_Football\Matches;
use App\Models\Models_News\News;
use App\Models\Offices;
use App\Models\Contacts;
use App\Models\Site_Pages\Site_Pages;
use App\Models\Visuals;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Helpers\BackupDatabaseHelper;
use App\Helpers\RSSHelper;

class Controller_News extends Controller
{


//Схема вызова функции:
//ResizeImage($image_from,$image_to, $максимальная_ширина,$максимальная_высота,$jpg_качество_уменьшенного_изображения)

//Пример:
//Resizeimage("tempimage.jpg","i/news/$newid-mini.jpg",550,500,90);


	public function index() {

		//Заголовок и описание страницы
		$page = \DB::table('site_pages')->where('url', '/')->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;

				!empty(Session::get('page')) ? Session::forget('page') : false;
				Session::put('page',URL::current());
			}
			
		//Вывод новостей
		
        $news = News::orderBy('priority','desc')->take(3)->get();
        $last_new = News::orderBy('priority', 'asc')->take(1)->get();

        $list = [];

        foreach($news as $new) {
            $list['news'][] = (object) ['id' => $new->id, 'name' => $new->name, 'description' => $new->description,
                       'content' => $new->content, 'created_at' => date('d.m.Y', strtotime($new->created_at)),
					   'title_img_news' => $new->title_img_news];
        }

        foreach($last_new as $new)
            $list['last_new'] = (object) ['id' => $new->id, 'name' => $new->name, 'description' => $new->description,
                'content' => $new->content, 'created_at' => date('d.m.Y', strtotime($new->created_at)), 'title_img_news' => $new->title_img_news];

		$last_match = Matches::orderBy('date', 'desc')->where('date', '<=', date('Y-m-d'))->first();

        $following_match = Matches::orderBy('date','asc')->where('date', '>=', date('Y-m-d'))->first();

        $month_last = '';
        $month_following = '';

        $content_matches = [];

        switch (date('M',strtotime($last_match->date))) {
            case "Jan": $month_last = "января"; break;
            case "Feb": $month_last = "февраля"; break;
            case "Mar": $month_last = "марта"; break;
            case "Apr": $month_last = "апреля"; break;
            case "May": $month_last = "мая"; break;
            case "Jun": $month_last = "июня"; break;
            case "Jul": $month_last = "июля"; break;
            case "Aug": $month_last = "августа"; break;
            case "Sep": $month_last = "сентября"; break;
            case "Oct": $month_last = "октября"; break;
            case "Nov": $month_last = "ноября"; break;
            case "Dec": $month_last = "декабря"; break;
        }

        if(isset($following_match->date)) {
            switch (date('M', strtotime($following_match->date))) {
                case "Jan":
                    $month_following = "января";
                    break;
                case "Feb":
                    $month_following = "февраля";
                    break;
                case "Mar":
                    $month_following = "марта";
                    break;
                case "Apr":
                    $month_following = "апреля";
                    break;
                case "May":
                    $month_following = "мая";
                    break;
                case "Jun":
                    $month_following = "июня";
                    break;
                case "Jul":
                    $month_following = "июля";
                    break;
                case "Aug":
                    $month_following = "августа";
                    break;
                case "Sep":
                    $month_following = "сентября";
                    break;
                case "Oct":
                    $month_following = "октября";
                    break;
                case "Nov":
                    $month_following = "ноября";
                    break;
                case "Dec":
                    $month_following = "декабря";
                    break;
            }
        }

        !empty($last_match) ? $content_matches['last_match'] = (object) [
            'team_master' => Football_Teams::find($last_match->id_team_master),
            'team_guest' => Football_Teams::find($last_match->id_team_guest),
            'type_event' => Football_Teams::find($last_match->id_type_event),
            'goals_master' => $last_match->goals_master,
            'goals_guest' => $last_match->goals_guest,
            'date' => date('d',strtotime($last_match->date)).' '.$month_last.' '.date('Y',strtotime($last_match->date)),
            'stadium' => $last_match->stadium
        ] : false;

        !empty($following_match) ? $content_matches['following_match'] = (object) [
            'team_master' => Football_Teams::find($following_match->id_team_master),
            'team_guest' => Football_Teams::find($following_match->id_team_guest),
            'type_event' => Football_Teams::find($following_match->id_type_event),
            'goals_master' => $following_match->goals_master,
            'goals_guest' => $following_match->goals_guest,
            'date' => date('d',strtotime($following_match->date)).' '.$month_following.' '.date('Y',strtotime($following_match->date)),
            'stadium' => $following_match->stadium
        ] : false;

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        return view('index',['footer_contact'=>$footer_contact,'news' => $list, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,
                             'content_matches' => $content_matches]);
    }

	public function form_add() {

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
		$path = 'public/uploads/news';
        
		if(!is_dir($path))     
			
			mkdir($path,0777,true); 
			
		return view('add_news',['footer_contact'=>$footer_contact]);
	}  
	
	public function add_news(Request $request) { 
//		dd(News::orderBy("priority",'desc')->first()->priority);
		$news = News::create(['name' => $request->title, 'description' => $request->description, 'content' => $request->content_news,
		'created_at' => date('Y-m-d H:i:s',strtotime($request->date_create)), 'priority' => News::orderBy("priority",'desc')->first()->priority + 1]);
		if( isset($request->title_img) ) {
			$path = 'public/uploads/news';
			if(!is_dir($path))
				mkdir($path,0777,true);
			$request->title_img->move($path, 'news'.$news->id.'.'.$request->title_img->getClientOriginalExtension());
			$news->title_img_news = $path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension();
			$news->save();
			ResizeImage::ResizeImage($path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension(),$path.'/news'.$news->id.'.'.$request->title_img->getClientOriginalExtension());
		}
		return redirect('/');     
	}
	
	public function news_page(Request $request) { //Новости на сранице новостей
		
		//Заголовок и описание страницы
		$page = \DB::table('site_pages')->where('url', '/news')->get();		
			if (!$page) {					
				return \View::make('errors.404');					
			}
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;
				  $picture=$p->picture;
				  !empty(Session::get('page')) ? Session::forget('page') : false;
				  Session::put('page',URL::current());
			}
			
		//Вывод новостей
		if($request->no_find_news){
            $no_find_news=1;
        }
        else{
            $no_find_news=0;
        }
        $news = News::orderBy('priority','desc')->paginate(3);
		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        return view('news',['no_find_news'=>$no_find_news, 'news' => $news, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'picture'=>$picture, 'footer_contact'=>$footer_contact]);
    }
	
	public function new_page($new_id) { //Одна новость

		$page = \DB::table('news')->where('id', $new_id)->get();

			if (count($page)==0) {
				return redirect('news?no_find_news=1');
			}
        
			foreach ($page as $p) {
				  $id=$p->id;
				  $title=$p->name;
				  $description=$p->description;
				  $meta_keywords=$p->meta_keywords;
				  $content =$p->content ;
				  $created_at=date('d.m.Y', strtotime($p->created_at));
				  $updated_at=$p->updated_at;
				  $title_img_new=$p->title_img_news;
				  !empty(Session::get('page')) ? Session::forget('page') : false;
				  Session::put('page',URL::current());
			}

        $another_news = News::where('id', '!=', $new_id)->orderBy('created_at','desc')->take(3)->get();

		$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        return view('new',['id'=>$id,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,
		'content'=>$content, 'created_at'=>$created_at, 'updated_at'=>$updated_at, 'title_img_new'=>$title_img_new,
		'another_news'=>$another_news,'footer_contact' => $footer_contact]);
    }

	public function edit_news(Request $request) {

		$news = News::find($request->id);

		$news->update(['name' => $request->title, 'description' => $request->description, 'meta_keywords' => $request->meta_keywords, 'content' => $request->content_news, 'updated_at' => date('Y-m-d H:i:s')]);

		if( isset($request->title_img) ) {
			$path = 'public/uploads/news';
			file_exists($news->title_img_news) ? unlink($news->title_img_news) : false;
			if(!is_dir($path))
				mkdir($path,0777,true);
			$uniqid = uniqid();
			$request->title_img->move($path, 'news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension());
			$news->title_img_news = $path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension();
			$news->save();
			ResizeImage::ResizeImage($path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension(),$path.'/news'.$news->id.$uniqid.'.'.$request->title_img->getClientOriginalExtension());

		}
		$dbuser = "";
		$dbname = "";
		$command = "mysqldump -u ".$dbuser." world >";
		system($command);
		return back();
	}

	public function delete_news($id) {
		$news = News::find($id);
		$title = explode("/",$news->title_img_news);
		$path = 'public/uploads/news';
		file_exists($path.'/'.$title[count($title) - 1]) ? unlink($path.'/'.$title[count($title) - 1]) : false;
		$news->delete();
		return redirect("/news");
	}

	public function sort(Request $request) {
		$news_current = News::find($request->id);
		$priority_new_current  = $news_current->priority;
		$news = News::orderBy("priority","desc")->where("priority","<",$priority_new_current)->first();
		$news_current->update(["priority" => $news->priority]);
		$news->update(["priority" => $priority_new_current]);
		echo json_encode(1);
	}

	public function edit_title_img_news(Request $request) {
//		dd($request->all());
		$title_img = Visuals::find($request->id);
		$title = explode("/", $title_img->title_img)[3];
		if( isset($request->title_img) ) {
			$type = $request->title_img->getClientOriginalExtension();
			$path = 'public/uploads/visuals';
			if(!is_dir($path))
				mkdir($path,0777,true);
			if(file_exists($path."/".$title)) {
				$type_old = explode(".", $title)[1];
				$old = explode("_",explode(".", $title)[0])[0];
				rename($path . "/" . $title, $path . "/" . $old."_old_".date("H-i-s_Y-m-d") . "." . $type_old);
			}
			$new = explode("_",explode(".", $title)[0])[0];
			$src = $request->title_img;
			$img = imagecreatefromjpeg($src);

			$dest = imagecreatetruecolor($request->w * (imagesx($img) / $request->div_w), $request->h * (imagesy($img) / $request->div_h));
//			imagecopyresampled($dest, $img, 0, 0, $request->x1 * (imagesx($img)/$request->div_w), $request->y1 * (imagesy($img)/$request->div_h),
//				$request->w, $request->h, imagesx($img), imagesy($img));
			imagecopy($dest, $img, 0, 0, $request->x1 * (imagesx($img) / $request->div_w), $request->y1 * (imagesy($img) / $request->div_h),
				$request->w * (imagesx($img) / $request->div_w), $request->h * (imagesy($img) / $request->div_h));
			$filename = $path . "/" . $new . "_" . date("H-i-s_Y-m-d") . "." . $type;
			imagejpeg($dest, $filename, 90);
			if($request->w * (imagesx($img) / $request->div_w) > 1200 && $request->h * (imagesy($img) / $request->div_h) > 900) {
				$img = imagecreatefromjpeg($filename);
				$dest = imagecreatetruecolor(1200, 900);
				imagecopy($dest, $img, 0, 0, 0, 0, 1200, 900);
				imagejpeg($dest, $filename, 90);
			}
			$title_img->title_img = $filename;
			$title_img->save();
//			$request->title_img->move($path, $new."_".date("H-i-s_Y-m-d").".".$type);
		}
		return back();
	}

	public function rss() {
		$news = News::orderBy('created_at','desc')->take(30)->get();
		$rss_news = RSSHelper::print_rss($news);
		return $rss_news;
	}
}