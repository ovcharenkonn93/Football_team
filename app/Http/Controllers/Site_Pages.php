<?php

namespace App\Http\Controllers;

use App\Models\Agricultural_Products;
use App\Models\Contacts;
use App\Models\For_Plants;
use App\Models\Models_News\News;
use App\Models\Offices;
use App\Models\Schedule;
use App\Models\Tariff_Trucking;
use App\Models\Trucking;
use App\Models\Vacancies;
use App\Models\Cultivation;
use App\Models\Documents;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ResizeImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use PhpParser\Comment\Doc;

class Site_Pages extends Controller
{
	public function index($page_name) //Страницы без разделов
		{
			$page = \DB::table('site_pages')->where('url', '/'.$page_name)->get();
						
						if ($page->count()<1){ //ошибка 404
						return \View::make('errors/404');
					}		
			
			$contact = NULL;
			$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
			foreach ($page as $p) {
				 $id = $p->id;
				 $url=$p->url;
				 $title=$p->title;
				 $description=$p->meta_description;
				 $meta_keywords=$p->meta_keywords;
				$picture = $p->picture;

				 Session::put('page',$p->url);

				if(!empty(Session::get('user'))) {
					$content = "<form class='ad-edit-form' method='post' action='" . URL::to("edit_page") . "'>
						<input type='hidden' name='_token' value='" . csrf_token() . "'>
						<input type='hidden' name='page' value='" . $p->url . "'>
						<div class='row'>
							<textarea name='_content' class='form-control edit_page' rows='5'>" . $p->content . "</textarea>
						</div>
						<div class='row col-md-offset-10 col-md-2  margin-top-10px margin-bottom-10px'>
							<button type='submit' class='btn btn-primary save-txt'>
								<i class='fa fa-floppy-o'></i>&nbsp;Сохранить изменения
							</button>
						</div></form>";
				} else {
					$content = $p->content;
				}

			}
			if (!$page || ($page_name == "add_news") && empty(Session::get('user'))) {
				return redirect("news");
			}

			if($this->is_document_page($page_name)) {

				$offices = Offices::whereHas('documents',function($q){
					$q->whereHas('page',function($q){
						preg_match ("/\/[^\/]*$/", $_SERVER['REQUEST_URI'], $matches);
						$request_uri = $matches[0];
						$q->where('url','=',$request_uri);
					});
				})->get();

				$all_offices = Offices::orderBy('name')->get();
				return \View::make('documents',["footer_contact"=>$footer_contact, 'title'=>$title, 'url'=>$url, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'content' => $content,'offices' => $offices,'all_offices'=>$all_offices,'contact' => $contact,"picture" => $picture]);
			}

			return \View::make($page_name,['id_page' => $id,'title'=>$title, 'url'=>$url, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'content' => $content, 'contact' => $contact, "footer_contact" => $footer_contact, "picture" => $picture]);
		}

	private function is_document_page($page_name){
		return $page_name == "registration_documents"||$page_name == "licenses"||$page_name == "typical_forms";
	}
	public function edit_structure(Request $request){

		$ext = getimagesize($_FILES["file"]["tmp_name"]);
		if($ext[2]!=1 && $ext[2]!=2 && $ext[2]!=3){
			return json_encode(2);
		}
		if($_FILES["file"]["size"]>5242880){
			return json_encode(1);
		}

		@unlink("public/uploads/interface/about_us.png");
		copy($_FILES["file"]["tmp_name"], "public/uploads/interface/about_us.png");
		$correct = ResizeImage::ResizeImage("public/uploads/interface/about_us.png", "public/uploads/interface/about_us.png");
		if($correct==false){
			return json_encode(2);
		}
		return json_encode(0);
	}

	public function edit_page_picture(Request $request){

		preg_match ("/\/[^\/]*$/", $request->url, $matches);
		$request_uri = $matches[0];
		$page = \App\Models\Site_Pages\Site_Pages::where('url','=', $request_uri)->first();

		$ext = getimagesize($_FILES["file"]["tmp_name"]);
		if($ext[2]!=1 && $ext[2]!=2 && $ext[2]!=3){
			return json_encode(2);
		}
		if($_FILES["file"]["size"]>5242880){
			return json_encode(1);
		}
		$dir = "public/images/interface/";
		@unlink($page->picture);
		if(file_exists($dir.$_FILES["file"]["name"])){
			$pct = $dir.(''.time()).$_FILES["file"]["name"];
			copy($_FILES["file"]["tmp_name"], $pct);
			$page->picture = $pct;
		}
		else{
			copy($_FILES["file"]["tmp_name"], $dir.$_FILES["file"]["name"]);
			$page->picture = $dir.$_FILES["file"]["name"];
		}
		//На случай если нужно изменять размер
//		$correct = ResizeImage::ResizeImage($dir.$_FILES["file"]["name"], $dir.$_FILES["file"]["name"]);
//		if($correct==false){
//			return json_encode(2);
//		}


		$page->save();

		return json_encode(0);
	}

	public function edit_page(Request $request) {

		\App\Models\Site_Pages\Site_Pages::orderBy('id', 'asc')->where('url', $request->page)->first()->update(['content' => $request->_content]);
		return back();
	}

	public function edit_meta(Request $request){

		//preg_match ("/\/[^\/]*$/", $request->url, $matches);

		$request_uri = $request->url;
		$number = last(explode("/",$request_uri));
		if(is_numeric($number)){
			$model = News::find($number);
		}
		else
		{
			$model = \App\Models\Site_Pages\Site_Pages::where('url','=', $request_uri)->first();
		}

		$model->meta_keywords = $request->key;
		$model->meta_description = $request->desc;
		$model->save();
		return json_encode(true);
	}
		
	public function contacts() //Контакты
		{		
			$page = \DB::table('site_pages')->where('url', '/contacts')->get();
			$schedule = [];
			$footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
			foreach ($page as $p) {
				  $title=$p->title;
				  $description=$p->meta_description;
				  $meta_keywords=$p->meta_keywords;

				  Session::put('page',$p->url);
			}	
			
			return \View::make('contacts',['footer_contact'=>$footer_contact,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);
		}

	public function contact_edit_page(Request $request) {
		\App\Models\Site_Pages\Site_Pages::find($request->page)->update(["id_contact" => $request->name]);
		return back();
	}

}
