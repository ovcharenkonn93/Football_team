<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Models_Football\Football_Teams;
use App\Models\Models_Football\Matches;
use App\Models\Models_Football\Type_Event;
use App\Models\Offices;
use App\Models\Contacts;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;

class Controller_Matches extends Controller {

    public function form_add_matches() {
        $page = \DB::table('site_pages')->where('url', '/add_match')->get();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        foreach ($page as $p) {
            $title=$p->title;
            $description=$p->meta_description;
            $meta_keywords=$p->meta_keywords;

            Session::put('page',$p->url);
        }

        if (empty(Session::get('user'))) {
            return redirect("/calendar");
        }

        $type_events = Type_Event::orderBy('id','asc')->get();

        $football_teams = Football_Teams::orderBy('id','asc')->whereNotIn('own',[1])->get();

        return view('add_matches',['footer_contact'=>$footer_contact,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,
                                   'type_events' => $type_events,'football_teams' => $football_teams]);
    }

    public function add_matches(Request $request) {
        $rival = null;
        if(isset($request->logo_rival) && isset($request->title_rival)) {
            $new_rival = Football_Teams::create(['title' => $request->title_rival]);
            $path = 'public/uploads/logo_team/';
            if(!is_dir($path))
                mkdir($path,0777,true);
            $request->logo_rival->move($path, 'logo_team_'.$new_rival->id.'.'.$request->logo_rival->getClientOriginalExtension());
            $new_rival->logo = $path.'logo_team_'.$new_rival->id.'.'.$request->logo_rival->getClientOriginalExtension();
            $new_rival->save();
            $rival = $new_rival->id;
        } else {
            $rival = $request->team_rival;
        }
        Matches::create([
            'date' => date('Y-m-d',strtotime($request->date)),
            'id_type_event' => $request->type_event,
            'id_team_master' => $request->type_match == 1 ? $rival : 1,
            'id_team_guest' => $request->type_match == 0 ? $rival : 1,
            'goals_master' => 0,
            'goals_guest' => 0,
            'stadium' => $request->stadium
        ]);
        return redirect('/calendar');
    }

    public function edit_matches(Request $request) {
        $match = Matches::find($request->id);

        $list_data = [];

        foreach (Type_Event::orderBy('id', 'asc')->whereNotIn('id', [$match->id_type_event])->get() as $event) {
            $list_data['events'][] = $event;
        }

        foreach (Football_Teams::orderBy('id', 'asc')->whereNotIn('id', [$match->id_team_master, $match->id_team_guest])->get() as $team) {
            $list_data['team_rival'][] = $team;
        }

        echo json_encode(['match' => ["id" => $match->id, "date" => date('d.m.Y',strtotime($match->date)), "stadium" => $match->stadium,
                                      "type_match" => $match->id_team_master == 1 ? 0 : 1,
                                      "team_rival" => $match->id_team_master == 1 ? Football_Teams::find($match->id_team_guest) : Football_Teams::find($match->id_team_master),
                                      "event" => Type_Event::find($match->id_type_event)],
                          'list_data' => $list_data]);
    }

    public function update_match(Request $request) {
        $match = Matches::find($request->id);
        $match->date = date('Y-m-d', strtotime($request->date));
        $match->stadium = $request->stadium;
        $match->id_type_event = $request->type_event;
        if($request->type_match == 0) {
            $match->id_team_master = 1;
            $match->id_team_guest = $request->team_rival;
        } else {
            $match->id_team_guest = 1;
            $match->id_team_master = $request->team_rival;
        }
        $match->save();
        return back();
    }
}