<?php

namespace App\Http\Controllers\Controller_Voting;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Site_Pages\Site_Pages;
use App\Models\Voting\Interview;
use App\Models\Voting\Interview_Question;
use App\Models\Voting\Interview_Answer;
use App\Models\Offices;
use App\Models\Contacts;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Request;


class Controller_Interview extends Controller
{
    public function index(){
        $interview = Interview::orderBy('id','desc')->first();
        $questions = $interview->questions;

        $user_question = $interview->questions()->whereHas('answers',function($q){
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                //check ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //to check ip is pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $q->where('ip','=',''.$ip);
        });

        $user_questions = $user_question->count();

        $questions_results = array();
        foreach($questions as $question){
            $val = $question->answers()->count();
            array_push($questions_results,$val);
        }

        if($user_questions>0){
            $results=1;
            $vote = $user_question->get()[0]->id;
        }
        else{
            $results=0;
            $vote = -1;
        }
        return json_encode(array(
            'interview'=>$interview,
            'questions'=>$questions,
            'results'=>$results,
            'questions_results'=>$questions_results,
            'vote'=>$vote
        ));

        //$answers = Interview_Answer::where()
    }

    public function store(Request $request){
        $title = $request->interview;
        $questions = json_decode($request->questions);
        $interview = Interview::create(array(
            'title'=>$title
        ));
        foreach($questions as $question){
            $interview_question = Interview_Question::create(array(
                'id_interview' => $interview->id,
                'question' => $question
            ));
        }
        return json_encode($interview->title);
    }

    public function update(Request $request){
        $title = $request->interview;
        $questions = json_decode($request->questions);
        $interview = Interview::find($request->id_interview);
        $old_questions = $interview->questions;

        $interview->title = $title;
        $interview->save();
//        return json_encode($interview->title);
        for($i=0;$i<max(count($questions),count($old_questions));$i++){
            if(isset($questions[$i])){
                if(isset($old_questions[$i])){
                    $old_questions[$i]->question = $questions[$i];
                    $old_questions[$i]->save();
                }
                else{
                    Interview_Question::create(array(
                        'id_interview' => $interview->id,
                        'question' => $questions[$i]
                    ));
                }
            }
            else{
                $old_questions[$i]->delete();
            }
        }
        return json_encode($interview->title);
    }

    public function vote(Request $request){
        //return json_encode($request);
        if (!empty($_SERVER['HTTP_CLIENT_IP'])){
            //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $qid = $request->qid;
        $answer = Interview_Answer::create(array(
            'id_interview_question'=>$qid,
            'ip'=>$ip
        ));
        return json_encode(0);
    }

    public function votings(){
        $page = Site_Pages::where('url', '/votings')->get();
        if (!$page) {
            return \View::make('errors.404');
        }
        foreach ($page as $p){
            $title=$p->title;
            $description=$p->meta_description;
            $meta_keywords=$p->meta_keywords;

            !empty(Session::get('page')) ? Session::forget('page') : false;
            Session::put('page',URL::current());
        }
        $months=["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
        $interviews = Interview::where('created_at','>=',(new Carbon('first day of January '.Carbon::now()->year))->toDateString())->get();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        //return new Carbon('first day of January '.Carbon::now()->year);
        return view('votings',['footer_contact'=>$footer_contact, 'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords,'interviews'=>$interviews, 'months'=>$months]);
    }

    public function voting_by_month(){
        //info(Carbon::now()->startofMonth()->addYear()->toDateString());
        $interviews = Interview::wherebetween('created_at',array(Carbon::now()->startofMonth()->subYear()->toDateString(),Carbon::now()->toDateString()))->get();
        $this_month = Carbon::now()->month-1;
        return json_encode(array(
            'interviews'=>$interviews,
            'this_month'=>$this_month
        ));
    }

    public function get_voting(Request $request){
        $interview = Interview::find($request->id);
        $questions = $interview->questions;

        $user_question = $interview->questions()->whereHas('answers',function($q){
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                //check ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //to check ip is pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $q->where('ip','=',''.$ip);
        });

        $user_questions = $user_question->count();

        $questions_results = array();
        foreach($questions as $question){
            $val = $question->answers()->count();
            array_push($questions_results,$val);
        }

        if($user_questions>0){
            $results=1;
            $vote = $user_question->get()[0]->id;
        }
        else{
            $results=0;
            $vote = -1;
        }
        return json_encode(array(
            'interview'=>$interview,
            'questions'=>$questions,
            'results'=>$results,
            'questions_results'=>$questions_results,
            'vote'=>$vote
        ));
    }

//    public function results(Request $request){
//
//    }

}