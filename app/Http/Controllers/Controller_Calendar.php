<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Models_Football\Football_Teams;
use App\Models\Models_Football\Matches;
use App\Models\Models_Football\Type_Event;
use App\Models\Offices;
use App\Models\Contacts;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;

class Controller_Calendar extends Controller {

    public function calendar() {
        $page = \DB::table('site_pages')->where('url', '/contacts')->get();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        foreach ($page as $p) {
            $title=$p->title;
            $description=$p->meta_description;
            $meta_keywords=$p->meta_keywords;

            Session::put('page',$p->url);
        }

        $matches = Matches::orderBy('date','desc')->get();
        $list = [];

        foreach ($matches as $match) {
            $month = '';

            switch (date('M',strtotime($match->date))) {
                case "Jan":
                    $month = "января";
                    break;
                case "Feb":
                    $month = "февраля";
                    break;
                case "Mar":
                    $month = "марта";
                    break;
                case "Apr":
                    $month = "апреля";
                    break;
                case "May":
                    $month = "мая";
                    break;
                case "Jun":
                    $month = "июня";
                    break;
                case "Jul":
                    $month = "июля";
                    break;
                case "Aug":
                    $month = "августа";
                    break;
                case "Sep":
                    $month = "сентября";
                    break;
                case "Oct":
                    $month = "октября";
                    break;
                case "Nov":
                    $month = "ноября";
                    break;
                case "Dec":
                    $month = "декабря";
                    break;
            }

            $list[] = (object) ['id' => $match->id, 'team_master' => Football_Teams::find($match->id_team_master),
                        'team_guest' => Football_Teams::find($match->id_team_guest), 'type_event' => Type_Event::find($match->id_type_event),
                        'date' => date('d',strtotime($match->date)).' '.$month, 'goals_master' => $match->goals_master, 'goals_guest' => $match->goals_guest];
        }

        return view('calendar',['footer_contact'=>$footer_contact,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords, 'matches' => $list]);
    }

}