<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\Models\Video;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddVideoController extends Controller
{
    function get_date(){
        $dates=DB::select("SELECT DISTINCT DATE(`date`) as `date` FROM `matches`");
        return json_encode($dates);
    }
    function get_matches($date){
        $matches=DB::select("SELECT 
	`matches`.`id`, 
    (SELECT `football_teams`.`title` FROM `football_teams` WHERE `football_teams`.`id`=`matches`.`id_team_master`) AS team_master,
    (SELECT `football_teams`.`title` FROM `football_teams` WHERE `football_teams`.`id`=`matches`.`id_team_guest`) AS team_guest
FROM 
	`matches`
WHERE 
	DATE(`matches`.`date`)=?", [$date]);
        return json_encode($matches);
    }
    function add_video(Request $request){
		$user=Session::get('user');
		if($user){
			$link=$request->get('link');
			$match=$request->get('match');
			$type;
			echo "user: $user, link=$link, match: $match";
            
            //youtube
			if(preg_match("#^(https?://)?(www\.)?youtube\.com/watch\?v=[\w-]+#",$link,$matches)){
				$type=1;
				//echo " hosting: youtube";
                $link=preg_filter("#^(https?://)?(www\.)?youtube\.com/watch\?v=#","", $matches[0]);
                //echo " link=$link";
            }
            elseif(preg_match("#^(https?://)?youtu.be/[\w-]+#", $link, $matches)){
                $type=1;
                $link=preg_filter("#^(https?://)?youtu.be/#", "", $matches[0]);
            }
            elseif(preg_match("#<iframe[^>]* src=\"(https?://)?(www\.)?youtube.com/embed/([\w-]+)\"[^>]*>#", $link, $matches)){
                $type=1;
               // var_dump($matches);
                $link=$matches[3];
                //echo "Youtube iframe";
            }
            //vk
            elseif(preg_match("#^(https?://)?(www\.)?vk.com/video-?[\d]+_[\d]+#", $link, $matches)){
                /*$type=2;
                echo " hosting: vk";
                $link=preg_filter("#^(https?://)?(www\.)?vk.com/video-?#","",$matches[0]);
                echo " link=$link";*/
                echo "<p style=\"color: #ff0020;\">Вставка видео из vk по ссылке не работает из-за особенностей видеохостинга. Вставьте код для встраивания (клик правой кнопкой мыши по видео-Копировать код для встраивания)</p>";
                return;
            }
            elseif(preg_match("#<iframe[^>]* src=\"(https?://)?(www\.)?vk.com/video_ext.php\?([^\"]+)\"[^>]*>#", $link, $matches)){
                $type=2;
                //var_dump($matches);
                $link=$matches[3];
            }
            //rutube
            elseif(preg_match("#^(https?://)?(www\.)?rutube.ru/video/[\w]+#", $link, $matches)){
                $type=3;
                //echo " hosting: rutube";
                $link=preg_filter("#^(https?://)?(www\.)?rutube.ru/video/#","",$matches[0]);
                //echo " link=$link";
            }
            elseif(preg_match("#<iframe[^>]* src=\"//rutube.ru/play/embed/([\d]+)\"[^>]*>#", $link, $matches)){
                //var_dump($matches);
                $type=3;
                $link=$matches[1];
            }
            //vimeo
            elseif(preg_match("#^(https?://)?(www\.)?vimeo.com/([\d]+)#", $link, $matches)){
                $type=4;
                //echo " hosting: rutube";
                $link=$matches[3];
                //echo " link=$link";
            }
            elseif(preg_match("#<iframe[^>]* src=\"(https?://)?(www\.)?player.vimeo.com/video/([\d]+)[\?\"]#", $link, $matches)){
                //var_dump($matches);
                $type=4;
                $link=$matches[3];
            }
            //no_hosting
            else {
                echo "<p style=\"color: #ff0020;\">Вы неверно ввели ссылку или ресурс не поддерживается</p>";
                return;
                
            }
            
            if(DB::select("SELECT id FROM video WHERE link=?",[$link])){
                echo "<p style=\"color: #ff0020;\">Видео с таким адресом уже добавленно</p>";
                return;
            }
                
            if(!DB::select("select id from matches where id=?",[$match])){
                echo "<p style=\"color: #ff0020;\">Матч, к которому привязано видео, не найден. Проверьте правильность ввода</p>";
                return;
            }
            
			$v=new Video;
			$v->user_id=$user;
			$v->link=$link;
			$v->type=$type;
			$v->match_id=$match;
			$v->save();
			echo "<p style=\"color: #2000ff;\">Видео $link добавленно</p>";
		}
        else {
            echo "<p style=\"color: #ff0020;\">Вы неавторизированны, перезайдите в свой профиль</p>";
            return;
        }
	}
}
