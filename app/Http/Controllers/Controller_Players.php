<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Offices;
use App\Models\Contacts;
use App\Models\Team\Positions;
use App\Models\Team\Team_Players;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Controller_Players extends Controller {

    public function index() {
        $page = \DB::table('site_pages')->where('url', '/team')->get();
        $footer_contact = ["email" => Offices::find(1)->email,"address" => Offices::find(1)->actual_addr, "telephone" => Offices::find(1)->phone_number];
        foreach ($page as $p) {
            $title=$p->title;
            $description=$p->meta_description;
            $meta_keywords=$p->meta_keywords;

            Session::put('page',$p->url);
        }

        $title_position = [1 => 'ВРАТАРИ', 2 => 'ЗАЩИТНИКИ', 3 => 'ПОЛУЗАЩИТНИКИ', 4 => 'НАПАДАЮЩИЕ', 5 => 'ТРЕНЕР'];


        $list_players = [];
        foreach (Positions::orderBy('id', 'asc')->get() as $key=>$position) {
            $list_players[$position->title]['title'] = $title_position[$key+1];
            foreach ($position->players as $player) {
                $list_players[$position->title]['list_players'][] = (object) $player->toArray();
            }
        }

        $list_position = Positions::orderBy('id', 'asc')->get();

        return view('team',['list_position' => $list_position, 'list_players' => $list_players,'footer_contact'=>$footer_contact,'title'=>$title, 'description'=>$description, 'meta_keywords'=>$meta_keywords]);
    }

    public function add_player(Request $request) {
        $player = Team_Players::create([
            'name' => $request->name_player,
            'number' => $request->number_player,
            'id_position' => $request->position_player
        ]);

        if(isset($request->img_player)) {
            $path = 'public/uploads/team/';
            if(!is_dir($path))
                mkdir($path,0777,true);
//            $request->img_player->move($path, 'player'.$player->id.'.'.$request->img_player->getClientOriginalExtension());
//            $player->portrait = $path.'player'.$player->id.'.'.$request->img_player->getClientOriginalExtension();
//            $player->save();

            $src = $request->img_player;
            $img = imagecreatefromjpeg($src);

            $dest = imagecreatetruecolor($request->w * (imagesx($img) / $request->div_w), $request->h * (imagesy($img) / $request->div_h));
            imagecopy($dest, $img, 0, 0, $request->x1 * (imagesx($img) / $request->div_w), $request->y1 * (imagesy($img) / $request->div_h),
                $request->w * (imagesx($img) / $request->div_w), $request->h * (imagesy($img) / $request->div_h));
            $filename =  $path.'player'.$player->id.'.'.$request->img_player->getClientOriginalExtension();
            imagejpeg($dest, $filename, 90);
            if ($request->w * (imagesx($img) / $request->div_w) > 264 && $request->h * (imagesy($img) / $request->div_h) > 352) {
                $img = imagecreatefromjpeg($filename);
                $dest = imagecreatetruecolor(264, 352);
                imagecopy($dest, $img, 0, 0, 0, 0, 264, 352);
                imagejpeg($dest, $filename, 90);
            }
            $player->portrait = $path.'player'.$player->id.'.'.$request->img_player->getClientOriginalExtension();
            $player->save();
        }

        return back();
    }

}