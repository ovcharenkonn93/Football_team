<?php

namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PDO;
use Imagick;

class ResizeImage extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

public static function ResizeImage($image_from,$image_to, $fitwidth=1200,$fitheight=1200,$quality=75) {
    //echo ("<br>Функция уменьшения изображений подключена");
    global $php_inc;
    $os=$originalsize=getimagesize($image_from);
    // если фотка JPEG или размеры, до которых надо уменьшить - ничего не делать
    if($originalsize[2]!=1 && $originalsize[2]!=2 && $originalsize[2]!=3){
        return false;
    }
//    if($originalsize[2]!=2 && $originalsize[2]!=3 && $originalsize[2]!=6 && ($originalsize[2]<9
//            or $originalsize[2]>12)) {
//        return false;
//    }

    if($originalsize[0]>$fitwidth or $originalsize[1]>$fitheight) {
        $h=getimagesize($image_from);
        if(($h[0]/$fitwidth)>($h[1]/$fitheight))
        {
            $fitheight=$h[1]*$fitwidth/$h[0];
        }else{
            $fitwidth=$h[0]*$fitheight/$h[1];
        }

        $o = ImageCreateTrueColor($fitwidth, $fitheight);
        if($os[2]==1)$i = ImageCreateFromGif($image_from);
        if($os[2]==2)$i = ImageCreateFromJPEG($image_from);
        if($os[2]==3)
        {
            $i=ImageCreateFromPng($image_from);
            imagecolortransparent($i, imagecolorallocate($i, 0, 0, 0));
            imagealphablending($i, false);
            imagesavealpha($i, true);
        };

        imagecopyresampled($o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h[0], $h[1]);
        if($os[2]==1)imagegif($o, $image_to);
        if($os[2]==2)imagejpeg($o, $image_to, $quality);
        if($os[2]==3)imagepng($o, $image_to);
        chmod($image_to,0777);
        imagedestroy($o);
        imagedestroy($i);
        return 2;
    }
    if($originalsize[0]<=$fitwidth && $originalsize[1]<=$fitheight) {

        if($os[2]==1)$i = ImageCreateFromGif($image_from);
        if($os[2]==2)$i = ImageCreateFromJPEG($image_from);
        if($os[2]==3)
        {
            $i=ImageCreateFromPng($image_from);
            imagecolortransparent($i, imagecolorallocate($i, 0, 0, 0));
            imagealphablending($i, false);
            imagesavealpha($i, true);

        };
        if($os[2]==1)imagegif($i, $image_to);
        if($os[2]==2)imagejpeg($i, $image_to,$quality);
        if($os[2]==3)imagepng($i, $image_to);
        //imagejpeg($i, $image_to, $quality);
        chmod($image_to,0777);
        return 1;
    }
}
}