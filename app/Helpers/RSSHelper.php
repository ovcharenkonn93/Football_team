<?php
namespace App\Helpers;

class RSSHelper {

    public function __construct(Manager $manager) {
//        $this->managet = $manager;
    }

    public static function print_rss($news)
    {
        $rss = '<?xml version="1.0"?>
          <rss version="2.0">
          <channel>
          <title>ФК «Донецкая Республика»: Новости</title>
          <link>http://football.ru</link>
          <description>Новости, статьи</description>';
        foreach($news as $row) {
            $id=$row['id'];
            $title=$row['name'];
            $text=$row['description'];
            $date=$row['created_at'];
            $rss .= '<item>
                        <title>'.$title.'</title>
                        <link>http://football.ru/news/'.$id.'</link>
                        <description>'.$text.'</description>
                        <pubDate>'.$date.'</pubDate>
                        <guid>ссылка на статью</guid>
                    </item>';
        }
        $rss .= '</channel></rss>';
        return $rss;
    }
}


