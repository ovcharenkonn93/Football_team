<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup_database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup database /public/backup folder';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = 'public/backup';
        if(!is_dir($path))
            mkdir($path,0777,true);
        $backup_file = date("H:i:s_Y-m-d")."_".env('DB_DATABASE'). '.sql';
        $commanda = "mysqldump -uroot football> C:/Users/1/Desktop/1.sql";
        system($commanda);
    }
}
