<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offices extends Model
{
    protected $table = 'offices';
    public $timestamps = false;

    protected $fillable = ['name','legal_addr','actual_addr', 'email'];

    public function contacts() {
        return $this->hasMany('App\Models\Contacts','office','id');
    }

    public function vacancies() {
        return $this->hasMany('App\Models\Vacancies','office','id');
    }

    public function documents(){
        return $this->belongsToMany('App\Models\Documents','offices_documents','id_office','id_document');
    }

    public function documents_by_current_page(){
        return $this->documents()->whereHas('page',function($q){
            preg_match ("/\/[^\/]*$/", $_SERVER['REQUEST_URI'], $matches);
            $request_uri = $matches[0];
            $q->where('url','=',$request_uri);
        });
    }
}