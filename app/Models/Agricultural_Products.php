<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agricultural_Products extends Model
{
    protected $table = 'agricultural_products';
    public $timestamps = false;

    protected $fillable = ['name','description','cost','action'];

}