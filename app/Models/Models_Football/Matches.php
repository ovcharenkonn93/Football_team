<?php

namespace App\Models\Models_Football;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
    protected $table = 'matches';
    public $timestamps = false;

    protected $fillable = ['id','id_team_master','id_team_guest','id_type_event','date','goals_master','goals_guest', 'stadium'];

    public function team_master() {
        return $this->belongsToMany('App\Models\Models_Football\Football_Teams','id_team_master','id');
    }

    public function team_guest() {
        return $this->belongsToMany('App\Models\Models_Football\Football_Teams','id_team_guest','id');
    }

    public function type_event() {
        return $this->belongsToMany('App\Models\Models_Football\Type_Event','id_type_event','id');
    }

    public function goals() {
        return $this->hasMany('App\Models\Models_Football\Matches_Goals','id_matches','id');
    }

}