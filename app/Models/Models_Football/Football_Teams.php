<?php

namespace App\Models\Models_Football;

use Illuminate\Database\Eloquent\Model;

class Football_Teams extends Model
{
    protected $table = 'football_teams';
    public $timestamps = false;

    protected $fillable = ['id','title', 'logo', 'own'];

    public function master() {
        return $this->hasMany('App\Models\Models_Football\Matches','id_team_master','id');
    }

    public function guest() {
        return $this->hasMany('App\Models\Models_Football\Matches','id_team_guest','id');
    }
}