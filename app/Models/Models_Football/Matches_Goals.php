<?php

namespace App\Models\Models_Football;

use Illuminate\Database\Eloquent\Model;

class Matches_Goals extends Model
{
    protected $table = 'matches_goals';
    public $timestamps = false;

    protected $fillable = ['id','id_matches','number_goals','time_goals'];

    public function match() {
        return $this->belongsToMany('App\Models\Models_Football\Matches','id_matches','id');
    }

}