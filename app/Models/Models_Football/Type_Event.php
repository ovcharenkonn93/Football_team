<?php

namespace App\Models\Models_Football;

use Illuminate\Database\Eloquent\Model;

class Type_Event extends Model
{
    protected $table = 'type_event';
    public $timestamps = false;

    protected $fillable = ['id','title'];

    public function matches() {
        return $this->hasMany('App\Models\Models_Football\Matches','id_type_event','id');
    }
}