<?php

namespace App\Models\Voting;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $table = 'interview';
    public $timestamps = true;

    protected $fillable = ['title'];

    public function questions(){
        return $this->hasMany('App\Models\Voting\Interview_Question','id_interview','id');
    }

}