<?php

namespace App\Models\Voting;

use Illuminate\Database\Eloquent\Model;

class Interview_Answer extends Model
{
    protected $table = 'interview_answer';
    public $timestamps = false;

    protected $fillable = ['id_interview_question','ip'];

    public function interview(){
        return $this->belongsTo('App\Models\Voting\Interview_Question','id_interview_question','id');
    }

}