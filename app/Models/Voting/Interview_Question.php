<?php

namespace App\Models\Voting;

use Illuminate\Database\Eloquent\Model;

class Interview_Question extends Model
{
    protected $table = 'interview_question';
    public $timestamps = false;

    protected $fillable = ['id_interview','question'];

    public function interview(){
        return $this->belongsTo('App\Models\Voting\Interview','id_interview','id');
    }

    public function answers(){
        return $this->hasMany('App\Models\Voting\Interview_Answer','id_interview_question','id');
    }

}