<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class For_Plants extends Model
{
    protected $table = 'for_plants';
    public $timestamps = false;

    protected $fillable = ['name','description','cost'];

}