<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tariff_Trucking extends Model
{
    protected $table = 'tariff_trucking';
    public $timestamps = false;

    protected $fillable = ['id_trucking','tariff'];

    public function  trucking() {
        return $this->belongsToMany('App\Models\Trucking','id_trucking','id');
    }
}