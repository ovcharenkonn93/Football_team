<?php

namespace App\Models\Site_Pages;

use Illuminate\Database\Eloquent\Model;

class Site_Pages extends Model
{
    protected $table = 'site_pages';
    public $timestamps = false;

    protected $fillable = ['url','title','content','meta_keywords','meta_description','id_contact','picture'];

    public function contact() {
        return $this->belongsToMany('App\Models\Contacts','id_contact','id');
    }
}