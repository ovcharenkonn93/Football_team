<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';
    public $timestamps = false;

    protected $fillable = ['office','contact_type','position','name','phone_number','email'];

    public function office() {
        return $this->belongsToMany('App\Models\Offices','office','id');
    }

    public function site_pages() {
        return $this->hasMany('App\Models\Site_Pages\Site_Pages','id_contact','id');
    }
}