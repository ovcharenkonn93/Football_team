<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cultivation extends Model
{
    protected $table = 'cultivation';
    public $timestamps = false;

    protected $fillable = ['plowing','aggregates','cost_services','cost_services_max','units'];

}