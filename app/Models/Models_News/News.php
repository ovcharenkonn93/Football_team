<?php

namespace App\Models\Models_News;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $timestamps = false;

    protected $fillable = ['name','description','content','title_img_news','priority','meta_keywords','meta_description','created_at','updated_at'];
}