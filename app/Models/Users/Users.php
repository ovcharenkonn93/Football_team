<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    public $timestamps = false;

    protected $fillable = ['login','password','remember_token','created_at','updated_at'];
}