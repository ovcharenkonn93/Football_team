<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    protected $table = 'positions';
    public $timestamps = false;

    protected $fillable = ['title'];

    public function players() {
        return $this->hasMany('App\Models\Team\Team_Players','id_position','id');
    }
}