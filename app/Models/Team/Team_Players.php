<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;

class Team_Players extends Model
{
    protected $table = 'team_players';
    public $timestamps = false;

    protected $fillable = ['name', 'number', 'portrait', 'id_position','active'];

    public function positions() {
        return $this->belongsTo('App\Models\Team\Positions','id_position','id');
    }
}