<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancies extends Model
{
    protected $table = 'vacancies';
    public $timestamps = false;

    protected $fillable = ['office', 'vacancies', 'gender', 'education', 'experience', 'employment'];

    public function office() {
        return $this->belongsToMany('App\Models\Offices','office','id');
    }
}