<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//error 404


//Route::get('/registration_documents',['uses'=>'Controller_Documents\Controller_Documents@index']);
Route::get('/team',['uses' => 'Controller_Players@index']);

Route::post('/team/add_player',['uses' => 'Controller_Players@add_player']);

Route::get('/add_match',['uses'=>'Controller_Matches@form_add_matches']);

Route::get('calendar/edit_match',['uses'=>'Controller_Matches@edit_matches']);

Route::post('calendar/update_match',['uses'=>'Controller_Matches@update_match']);

Route::post('/add_new_match',['uses'=>'Controller_Matches@add_matches']);

Route::get('/add_video/get_date','AddVideoController@get_date');

Route::get('/add_video/get_matches/{date}','AddVideoController@get_matches');

Route::get('/add_video','AddVideoController@add_video');

Route::get('/calendar',['uses'=>'Controller_Calendar@calendar']);

Route::post('/cultivation_csv', ['uses'=>'LoadPriceCSV@LoadPriceCultivation']);

Route::get('/', ['uses'=>'Controller_News\Controller_News@index']);

Route::post('/edit_title_img_news', ['uses'=>'Controller_News\Controller_News@edit_title_img_news']);

Route::get('/news', ['uses'=>'Controller_News\Controller_News@news_page']);

Route::get('/contacts', 'Site_Pages@contacts');

Route::post('/contacts/edit_contacts', 'Site_Pages@edit_contacts');

Route::post('/contacts/add_contacts', 'Site_Pages@add_contacts');

Route::post('/contacts/update_office_contact', 'Site_Pages@update_office_contact');

Route::get('/contacts/delete_office/{id}', 'Site_Pages@delete_office');

Route::get('/contact/get_data', 'Site_Pages@contact_get_data_by_id');

Route::post('/contact/edit_page', 'Site_Pages@contact_edit_page');

Route::get('/vacancies', 'Site_Pages@vacancies');

Route::post('/vacancies/add', 'Site_Pages@add_vacancies');

Route::get('/vacancies/delete/{id}', 'Site_Pages@delete_vacancies');

Route::get('/vacancies/edit', 'Site_Pages@edit_vacancies');

Route::post('/vacancies/update', 'Site_Pages@update_vacancies');

Route::get('/cultivation', 'Site_Pages@cultivation');

//Route::get('/registration_documents', 'Site_Pages@registration_documents');

Route::post('/cultivation/add', 'Site_Pages@add_cultivation');

Route::get('/cultivation/delete/{id}', 'Site_Pages@delete_cultivation');

Route::get('/cultivation/edit', 'Site_Pages@edit_cultivation');

Route::post('/cultivation/update', 'Site_Pages@update_cultivation');

Route::get('/trucking', 'Site_Pages@trucking');

Route::post('/trucking/add', 'Site_Pages@add_trucking');

Route::get('/trucking/delete/{id}', 'Site_Pages@delete_trucking');

Route::get('/trucking/edit', 'Site_Pages@edit_trucking');

Route::post('/trucking/update', 'Site_Pages@update_trucking');

Route::get('/for_plants', 'Site_Pages@for_plants');

Route::post('/for_plants/add', 'Site_Pages@add_for_plants');

Route::get('/for_plants/delete/{id}', 'Site_Pages@delete_for_plants');

Route::get('/for_plants/edit', 'Site_Pages@edit_for_plants');

Route::post('/for_plants/update', 'Site_Pages@update_for_plants');

Route::get('/agricultural-products', 'Site_Pages@agricultural_products');

Route::post('/agricultural-products/add', 'Site_Pages@add_agricultural_products');

Route::get('/agricultural-products/delete/{id}', 'Site_Pages@delete_agricultural_products');

Route::get('/agricultural-products/edit', 'Site_Pages@edit_agricultural_products');

Route::post('/agricultural-products/update', 'Site_Pages@update_agricultural_products');

Route::get('/login',['uses'=>'Auth\LoginController@index']);

Route::get('/logout',['uses'=>'Auth\LoginController@logout']);

Route::post('/login', ['uses' => 'Auth\LoginController@auth']);

Route::get('/change_password',['uses'=>'Auth\LoginController@change_password']);

Route::post('/change_password',['uses'=>'Auth\LoginController@update_password']);

Route::post('/add_news', ['uses'=>'Controller_News\Controller_News@add_news']);

Route::get('news/delete/{id}', 'Controller_News\Controller_News@delete_news');

Route::post('news/edit_news', 'Controller_News\Controller_News@edit_news');

Route::get('news/sort', 'Controller_News\Controller_News@sort');

Route::get('news/rss', 'Controller_News\Controller_News@rss');

Route::get('news/{new_id}', 'Controller_News\Controller_News@new_page');

Route::get('/votings',['uses'=>'Controller_Voting\Controller_Interview@votings']);

Route::get('{page_name}', 'Site_Pages@index');

Route::post('/edit_page', 'Site_Pages@edit_page');

Route::post('/add_document', ['uses'=>'Site_Pages@add_document']);
Route::post('/update_document', ['uses'=>'Site_Pages@update_document']);
Route::post('/delete_document', ['uses'=>'Site_Pages@delete_document']);

Route::post('/edit_structure', ['uses'=>'Site_Pages@edit_structure']);
Route::post('/edit_page_picture', ['uses'=>'Site_Pages@edit_page_picture']);

Route::post('/edit_meta', ['uses'=>'Site_Pages@edit_meta']);

Route::post('/add_voting',['uses'=>'Controller_Voting\Controller_Interview@store']);
Route::post('/edit_voting',['uses'=>'Controller_Voting\Controller_Interview@update']);
Route::post('/last_voting',['uses'=>'Controller_Voting\Controller_Interview@index']);
Route::post('/add_vote',['uses'=>'Controller_Voting\Controller_Interview@vote']);
Route::post('/get_voting',['uses'=>'Controller_Voting\Controller_Interview@get_voting']);
Route::post('/voting_by_month',['uses'=>'Controller_Voting\Controller_Interview@voting_by_month']);





// Маршруты прописаны универсально. Чтобы страница начала обрабатываться её нужно добавить в базу данных
// Database: football  -   Table: site_pages